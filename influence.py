"""


Refs:
     [Glattfelder et al.19] “The Architecture of Power: Patterns of Disruption and Stability in the Global Ownership Network”, J.B. Glattfelder and S. Battiston, ssrn (2019)
"""

import numpy as np
#import pandas as pd
from network_tools.curveballs import edgelist_from_adjlist

import matplotlib.pyplot as plt

import graph_tool.all as gt
import igraph as ig

from scipy.sparse.linalg import spsolve
from scipy.sparse import  eye

#import os

import warnings

#from network_tools.trophic_layout import ForceAtlas
#from network_tools.measure import get_SCC, get_cycle_count

#from itertools import product

#from scipy.sparse import csr_matrix, eye

from network_tools.coherence_sampler import ppm

def test_tree_random():
    """
    https://igraph.org/python/doc/api/igraph._igraph.GraphBase.html#Tree_Game
    """
    pass

def test_ppm_indirect_gt():
    """
    si acycliq: toutes le vp sont à zero, W pas diagonalisable. =>tester DEDICOM
    si qqs loop: diagonalisable, => tester comparaison directe
    """
    # code new trophic for gt in trophic_mackay.py
    # generate graph
    N=10000
    Nb = 100
    L = 10000
    T=1.
    adjlist,s = ppm(N,L,Nb, T)
    e = edgelist_from_adjlist(adjlist)
    g =gt.Graph(directed=True)
    g.add_edge_list(e)
    vp_s = g.new_vp('float',val = -1000)
    vp_s.a =s
    # generate value for nodes
    
    # compute direct value
    # compute indirect value
    # compare
    
    # do the same, with T rising, as in network_tools.coherence_sampler.test_ppm_increase_T

def example_vitali11():
    """
    example case in [Vitali et al. 11], and [Glattfelder et al.19] SI §1.6.7
    """
    e = [(0,1,0.1), (1,2,0.5),(2,1,0.3), (1,3,0.5),(3,1,0.3), (1,4,0.2),(4,1,0.3), (3,4,0.6),(4,3,0.5),(2,4,0.2),(4,2,0.5),(4,5,1)]
    v = np.ones(6)
    #xi = (0.36, 2.6, 1.4, 2.520, 3.050, 0.0)
    chi0 = np.array([5,49,26,48,54,0])
    return e,v,chi0

def test_indirect_ig():
    """
    """
    e,v,chi0 = example_vitali11()
    g= ig.Graph(directed=True)
    g.add_vertices( N ) # 
    g.add_edges(e ) 
    
    g= ig.Graph.TupleList(e, directed=True,edge_attrs='weight')
    #
    W = g.get_adjacency_sparse(attribute='weight')
    Wv= direct_value(W,v)
    # compute indirect value
    chi  = indirect_value(W,v,Wv)
    #plt.scatter(chi,chi0)
    assert np.allclose(chi,chi0)
    """
    Comment: OK
    """

def test_ppm_indirect_ig():
    """
    si acycliq: toutes le vp sont à zero, W pas diagonalisable. =>tester DEDICOM
    si qqs loop: diagonalisable, => tester comparaison directe
    """
    # generate graph
    N=1000
    Nb = 10
    L = 1000
    T=0.1
    adjlist,s = ppm(N,L,Nb, T)
    e = edgelist_from_adjlist(adjlist)

    g= ig.Graph(directed=True)
    g.add_vertices( N ) # 
    g.add_edges(e ) 
    
    # generate edge weights
    g.es['weight'] = np.random.rand( g.ecount() )
    # normalize edge weights
    
    # generate value for nodes
    # "Note that the mean and standard deviation are not the values for the distribution itself, but of the underlying normal distribution it is derived from."
    mu, sigma = 3., 1. # mean and standard deviation
    v = np.random.lognormal(mu, sigma, 1000)
    # compute direct value
    W = g.get_adjacency_sparse(attribute='weight')
    Wv= direct_value(W,v)
    # compute indirect value
    chi  = indirect_value(W,v,Wv)
    plt.scatter(Wv,chi)
    #check: comput F0
    
    # do the same, with T rising, as in network_tools.coherence_sampler.test_ppm_increase_T


def direct_value(W,v):
    """
    compute chi=Wv

    https://docs.scipy.org/doc/scipy/reference/sparse.linalg.html
    
    refs: [Glattfelder et al.19]
    """
    return W@v

def indirect_value(W,v,Wv):
    """
    chi = W chi + Wv
    
    refs: [Glattfelder et al.19]
    """
    A = eye(W.shape[0]) -W
    b = Wv
    chi = spsolve(A,b  )
    return chi

"""
bipartite tools



1-mode projection implementations:
    igraph 
    https://networkx.org/documentation/latest/reference/algorithms/generated/networkx.algorithms.bipartite.projection.projected_graph.html#networkx.algorithms.bipartite.projection.projected_graph
    http://data.complexnetworks.fr/Bip/
"""

import graph_tool.all as gt
import numpy as np
import networkx as nx
from networkx.algorithms import bipartite

def get_sample_bipartite_dir_graph_nx(id_=0):
    """
    """
    D = nx.DiGraph()
    if id_==0:
        # Add nodes with the node attribute "bipartite"
        D.add_nodes_from([0,1, 2], bipartite=0)
        D.add_nodes_from([3,4,5], bipartite=1)
        # Add edges only between nodes of opposite node sets
        D.add_edges_from([(0, 3), (0, 4), (4,1), (2, 4), (2, 5)])
    elif id_==1:
        #https://networkx.org/documentation/stable/reference/algorithms/generated/networkx.algorithms.bipartite.generators.gnmk_random_graph.html#networkx.algorithms.bipartite.generators.gnmk_random_graph
        D = bipartite.gnmk_random_graph(100,100,1000,directed=True) 
        top = nx.bipartite.sets(D)[0]
        #reverse some edges ( they're all in the direction)
        p=0.5
        l=[]
        for u, v in D.edges():
            if np.random.random()<p:
                D.add_edge(v,u)
                l.append((u,v))
        for elem in l:
                u,v=elem
                D.remove_edge(u,v)
        """

        pos = nx.bipartite_layout(D, top)
        nx.draw(D,pos)
        """
    return D

def get_sample_bipartite_dir_graph():
    """
    test cases
    
    more: 
    g = gt.lattice([100,100]) 
    g.vp["type"] = g.new_vp("bool")
    is_biparitite, part = gt.is_bipartite(g, partition=True) 
    g.vp["type"] = part
    """
    l=[ {'edge_list':[[0,2], [2,1]],
        'vtype' : [0,0,1],
        'edge_list_proj' : [[0,1]]
        },
        {'edge_list':[[0,2], [1,2]],
        'vtype' : [0,0,1],
        'edge_list_proj' :[]
        },
         {'edge_list':[[2,0], [2,1]],
        'vtype' : [0,0,1],
        'edge_list_proj' : []
        }  ]
    return l    
    
def test_monopartite_projection_gt():
    """
    """
    for d in  get_sample_bipartite_dir_graph():
        #make gt graph
        g = gt.Graph(directed=True)
        g.vp["type"] = g.new_vp("int")
        g.add_edge_list( d['edge_list'] )
        g.vp["type"].a = d['vtype']
        #project
        g_proj = monopartite_projection_gt(g,mode=0)
        #compare to expected output
        g_proj_target = gt.Graph(directed=True)
        g_proj_target.add_edge_list( d['edge_list_proj'] )
        s = gt.similarity(g_proj, g_proj_target,norm=False)
        print(s)
  
def test_monopartite_projection_compare_nx():
    """
    
    From nx [documentation](https://networkx.org/documentation/stable/reference/algorithms/bipartite.html#module-networkx.algorithms.bipartite):
        "NetworkX does not have a custom bipartite graph class but the Graph() or DiGraph() classes can be used to represent bipartite graphs. However, you have to keep track of which set each node belongs to, and make sure that there is no edge between nodes of the same set. The convention used in NetworkX is to use a node attribute named bipartite with values 0 or 1 to identify the sets each node belongs to."
    """
    D= get_sample_bipartite_dir_graph_nx(id_=0)
    #convert to gt
    g = gt.Graph(directed=True)
    g.add_edge_list(     list(D.edges()) )
    vtype = g.vp["type"] = g.new_vp("int")
    g.vp["type"].a = np.array([D.nodes[n]['bipartite'] for n in D])
    #do monopartite proj
    mode = 0
    g_proj = monopartite_projection_gt(g,mode=mode)
    g_proj.get_edges()
    idx = np.where(np.array([D.nodes[n]['bipartite'] for n in D])==mode)[0]
    D_proj = bipartite.projected_graph(D, idx)
    list(D_proj.edges()) 
    #compare adjacency matrix
    #warning: nx and gt dont't have the same convention for adjacency matrix
    #   nx : "For directed graphs, entry i,j corresponds to an edge from i to j." from [doc](https://networkx.org/documentation/stable/reference/generated/networkx.linalg.graphmatrix.adjacency_matrix.html?highlight=adjacency#networkx.linalg.graphmatrix.adjacency_matrix)
    A_nx = nx.adjacency_matrix(D_proj) >0
    A_gt = gt.adjacency(g_proj).T >0
    assert np.all(A_nx.todense().flatten() ==A_gt.todense().flatten())
    
def monopartite_projection_gt(g,mode=1,remove_self_loop=True):
    """
    
    Q=???get adjacency as a linear operator (csr matrix not necessary, see doc gt.adjacency)
    #A = gt.adjacency(g, vindex =g.vp["idx"], operator=False)
    
    #undirected case: compute BTB
    #proj = A.transpose().dot(A) #undirected case, cf Newman18 p.??
    """
    A = gt.adjacency(g, operator=False)
    A=A.transpose()
    proj = A.dot(A)
    if remove_self_loop:
        proj.setdiag(0)
    #cutoff 
    proj = proj>0
    #extract submatrix
    g_projected = gt.Graph(directed=True)
    g_projected.add_edge_list(np.array(proj.nonzero()).T)
    """
    g_projected = g.copy()
    g_projected.add_edge_list(np.array(proj.nonzero()).T)
    g_projected = gt.Graph(gt.GraphView(g_projected, 
                                            vfilt=(g.vp["type"].a==mode)), prune=True)        
    """                                            
    g_projected = gt.GraphView(g_projected,   vfilt=(g.vp["type"].a==mode))
    g_projected.purge_vertices() 
    g_projected.purge_edges()
    return g_projected
    
def monopartite_projection_ig(g):
    """
    (undirected : https://igraph.org/python/api/latest/igraph.Graph.html#bipartite_projection)
          
    TODO: unit test: cf newman
    """
    #
    import igraph as ig
    g = gt.collection.ns["eu_procurements_alt/DE_2014"]
    is_bi, part = gt.is_bipartite(g, partition=True)
    g.vp["type"] = part
    g.vp.pop('label')
    g.vp.pop('_pos')
    g_ig = ig.Graph.from_graph_tool(g) #OK
    nv1,ne1,nv2,ne2 = ig.Graph.bipartite_projection_size(g_ig, types='type')#OK
    nv1,ne1,nv2,ne2
    #----------
    #convert bipartite to ig
    #https://igraph.org/python/api/latest/igraph.Graph.html#from_graph_tool
    g = load_gt_graph_filtered(filename_gt = "data/agribalyse.gt")
    g.vp.pop('flow_name') # IS IT NECESSARY ?vertex type must be kept:
    g.vp.pop('flow_type')
    g.vp.pop('id')
    g.vp.pop('category')
    g.vp.pop('category_agg')
    g.ep.pop('weight')
    g.purge_vertices() 
    g.purge_edges()
    g.reindex_edges() # necessary because doesn't start at 0
    g_ig = ig.Graph.from_graph_tool(g)  #OK
    g_ig.vs.attributes()
    nv1,ne1,nv2,ne2 = ig.Graph.bipartite_projection_size(g_ig, types='type')
    #PROBLEM:nv2=15655 (process) ; ne2=45 352 246 ???????
    g_ig.density()    #                                                    Out[65]: 0.002
    # ------------
    #convert tripartite to ig
    #run algo ig.bipartite_projection
    fname_exchange = "data/exchanges_export_agribalyse2.csv"
    fname_process = "data/processes_export_agribalyse2.csv"
    fname_flow = "data/flows_export_agribalyse2.csv"
    fname_category = "data/categories_export_agribalyse2.csv"
    df_exchange, df_process,df_flow, df_category = read_csv_ij_dump(fname_exchange,fname_process,fname_flow, fname_category)
    g = make_multilayer_tripartite_graph(df_exchange, df_process,df_flow, df_category)
    # project to 2 layer: filter category nodes, filter edges on layer 1 (category node to others))
    
    g_bipartite = gt.GraphView(g, vfilt= g.vp["type"].a <2 ,  efilt = g.ep["edgeType"].a ==0) 
    g_bipartite.ep.pop('edgeType')
    g_bipartite.ep.pop('weight')
    g_bipartite.vp.pop('name')
    g_bipartite.purge_vertices() 
    g_bipartite.purge_edges()
    g_ig = ig.Graph.from_graph_tool(g_bipartite)  #OK
    #check converted graph
    g_ig.is_directed()
    g_ig.vs.attributes()
    g_ig.vcount()
    g_ig.ecount()
    g_ig.is_simple()   # FALSE ??????????
    np.sum(1*g_ig.is_multiple())  # =204435 ?????????????
    g_ig = g_ig.simplify(multiple=True)
    nv1,ne1,nv2,ne2 = ig.Graph.bipartite_projection_size(g_ig, types='type') #ne2=39 704 514 ???
    g_ig_proj =  ig.Graph.bipartite_projection(g_ig, types='type', multiplicity=False,which=1)
    g_ig_proj.ecount()  # 39.10^6 !!!!!!!!!!!!!!
    g_ig_proj.is_simple() # TRUE
    #convert back to gt
    #https://igraph.org/python/api/latest/igraph.Graph.html#to_graph_tool
    #g_mono = g_ig.to_graph_tool(graph_attributes=None, vertex_attributes=None, edge_attributes=None)
    """
    Commentaires:
    * direction not taken into account
    * multiple edges ???
    * nb edge rising ???
    
    """
 

"""
    # NOT WHAT IS EXPECTED
    https://stackoverflow.com/questions/57911814/does-graph-tool-have-a-way-of-projecting-bipartite-graphs
    
    g = gt.lattice([500,500])
    is_biparitite, part = gt.is_bipartite(g, partition=True)
    
    for v, bipartite_label in enumerate(part):
        if bipartite_label == 0:
            neighbours = list(g.vertex(v).all_neighbours())

            for s, t in combinations(neighbours, 2):
                g_temp.add_edge(s, t)

    g_projected = gt.Graph(gt.GraphView(g_temp, vfilt=part.a==1), prune=True)
    
"""

"""  
@jit(nopython=False)
def mono_proj(g,part):
   

    # NOT TESTED
    for v in g.iter_vertices():
    cnt = 0
        for u in g.iter_all_neighbors(v):
           for u2 in g.iter_all_neighbors(u):
              cnt+=1 
        l.append(cnt) 


   # doesn't work in nopython mode => TOO SLOW
    for v, bipartite_label in enumerate(part):
        if bipartite_label == 0:
            neighbours = list(g.vertex(v).all_neighbours())
            
            for n1 in neighbours:
                neighbours2 = list(g.vertex(n1).all_neighbours())
                for n2 in neighbours2:    
                    g.add_edge(v, n2) 
                    
    g_projected = gt.Graph(gt.GraphView(g, vfilt=part.a==1), prune=True)        

    return g_projected
    """      

from scipy.sparse import csr_matrix, coo_matrix

def get_nonempty_row(indptr):
    """
    return the indices of nonempty rows in csr matrix
    
    Usage: see renormalize.renormaliz, network_lci.analyze
    """
    n = indptr.shape[0] - 1    
    l=[]
    for i in range(n):  
        k1 = indptr[i]            
        k2 = indptr[i+1]
        if ( k1< k2): l.append(i)
    return l


def get_nonempty_col(indices):
    """
    return the indices of nonempty cols in csr matrix
    
    Usage: see network_lci.analyze
    """
    return list(set(indices))


def dict_to_coo(d):
    """
    convert a count dict with tuple key (i,j) 
    to a coo sparse matrix
    
    Usage: see network_lci.analyze
    """
    data_l=[]
    i_l=[]
    j_l=[]    
    for (i,j),data in d.items(): 
        i_l.append(i)
        j_l.append(j)
        data_l.append(data)
    return coo_matrix((data_l, (i_l, j_l)))

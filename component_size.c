/*

Compute size of the components of a random graph with geometric degree distribution.
Replication of a numerical experiment in [Newman 18].

COMPILE:
$gcc component_size.c  `pkg-config --static --libs --cflags igraph` -o component_size

REF:
https://igraph.org/c/html/latest/igraph-Random.html#igraph_rng_get_geom

*/

#include <igraph.h>

#define NODES 10000000
#define GEOM(p) (igraph_rng_get_geom(igraph_rng_default(), p ))
#define DEBUG 0


void print_vector(igraph_vector_t *v, FILE *f) {
    long int i;
    for (i = 0; i < igraph_vector_size(v); i++) {
        fprintf(f, "%li ", (long int) VECTOR(*v)[i]);
    }
    fprintf(f, "\n");
}


int main() {

    igraph_t g ;
    int i;
    igraph_vector_t outdeg;
    igraph_vector_t component_sizes;

    igraph_rng_seed(igraph_rng_default(), 42);

    igraph_vector_init(&outdeg, NODES);
    for(i=0; i<NODES; i++)
	{VECTOR(outdeg)[i] = GEOM(1-0.3);} 
    
    if(DEBUG){
       printf("====DEGREE\n");
       igraph_vector_print(&outdeg); 	}
       printf("\n");

    // Generation
	
    igraph_degree_sequence_game(&g, &outdeg, 0, IGRAPH_DEGSEQ_SIMPLE);
    
    if (igraph_is_directed(&g) || igraph_vcount(&g) != NODES) {
	printf("graph generation error ");
        return 1;
    }
	
    // Components
    // https://igraph.org/c/html/latest/igraph-Structural.html#graph-components
    // example: igraph_erdos_renyi_game.c    

    igraph_vector_init(&component_sizes, 0);
    igraph_clusters(&g, NULL, &component_sizes, NULL, IGRAPH_STRONG);
    if(DEBUG){
       printf("====COMPONENT SIZE\n");
       igraph_vector_print(&component_sizes);   
       printf("\n");
      }

    //print
    print_vector(&component_sizes, stdout);

    // cleaning
	
    igraph_destroy(&g);
    igraph_vector_destroy(&outdeg);
    igraph_vector_destroy(&component_sizes);

    return 0;
}

/*
 * AIM: get a faster curveball_q 
 * IDEA: wrap cpp code using cython (same as networkit)
 *           see https://networkit.github.io/dev-docs/DevGuide.html
 * OPTION 1:    start from Fischer code (lightweight). has a vector(set) structure, similar to adjlist in curveballs.py
 *                    
 *                      PROBLEM: simple operations like set unions are not trivial : https://stackoverflow.com/questions/54763112/how-to-improve-stdset-intersection-performance-in-c
 * OPTION 2:    start from libnetworkit curveball implementation ; 
 *                      PROBLEM: how to pass "s" vector to the cpp code ?
 * 
 * g++ -std=c++11 coherence_ensemble_generator.cpp -o coherence_ensemble_generator
 * 
 * 
 * ------ Networkit::curveball in :
 * 
 *  node = https://networkit.github.io/dev-docs/cpp_api/typedef_namespaceNetworKit_1a2e1aa535e7aaeec9130eb3fc0ab32697.html?highlight=node
 * 
 *   computeCommonDisjointNeighbour()
 *   https://github.com/networkit/networkit/blob/master/networkit/cpp/randomization/GlobalCurveballImpl.hpp
 * 
 *  OR
 * 
 *  l.447 in https://github.com/networkit/networkit/blob/master/networkit/cpp/randomization/CurveballImpl.cpp
 *  common_neighbours.clear();
 * 
 * ----- Igraph adjacency 
 * adjlists: https://igraph.org/c/html/latest/igraph-Data-structures.html#igraph-Adjlists
 * set operations on sorted vectors: https://igraph.org/c/html/latest/igraph-Data-structures.html#set-operations-on-sorted-vectors
 * 
 * OTHER LIBS:
 * https://ae.cs.uni-frankfurt.de/index.html
 * https://github.com/manpen
 * https://github.com/massive-graphs/extmem-lfr
 * https://github.com/hthetran
 * https://github.com/Daniel-Allendorf/incpwl       power-law
 * 
 * */

#include <cmath>  // for `exp`
#include <vector>
#include <set>


void curveball( std::vector<std::set<unsigned int> > links  ){
    
    }

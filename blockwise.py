"""
Created on Aug 31, 2021
@author: Aurélien Hazan <aurelien.hazan@u-pec.fr>
"""
import numpy as np
from network_tools.blockwise_view import blockwise_view
from network_tools.blockwise import *

try:
    from network_datasets.synthetic import RavaszBarabasiGraphCorrected
except: 
    print('could not load from network_datasets.synthetic') 
        
import matplotlib.pyplot as plt

def test_block_approximation_uneven():
    """
    uneven block. fails 
    """
    #do the block transformation
    arr = np.arange(16).reshape(4, 4)  
    #arr_block_approx = arr.copy()
    blocksize = (2, 2)
    arr_block_approx = block_approximation(arr,blocksize )
    #inspect the obtained array
    blocked = blockwise_view(arr, blocksize)    
    blocked_approx = blockwise_view(arr_block_approx, blocksize)    
    
    shape_reduced = blocked.shape[0:2]
    for i in range(shape_reduced[0]):
        for j in range(shape_reduced[1]):
            assert blocked[i][j].sum() ==  blocked_approx[i][j].sum() 

def test_block_approximation_permutation_ravasz():
    """
    this is related to $HOME/renormalize/example_hierarchical_ravasz
    """
    hlevels=3;
    motif=5
    A = RavaszBarabasiGraphCorrected(motif,hlevels=hlevels)
    #permutation
    perm = np.random.permutation( A.shape[0])
    A_perm = A[perm,:]
    A_perm = A_perm[:,perm]
    
    blocksize = (5, 5)
    P_block=block_approximation(A,blocksize )
    if plot: plt.imshow(P_block)

    P_perm_block=block_approximation(A_perm,blocksize )
    if plot: 
        fig, axes = plt.subplots(1,3 ,figsize=(15, 15)); 
        axes[0].spy(A)
        axes[1].spy(A_perm)
        axes[2].imshow(P_perm_block)

def test_block_approximation_ravasz():
    """
    """
    hlevels=3;
    motif=5
    A = RavaszBarabasiGraphCorrected(motif,hlevels=hlevels)
    
    blocksize = (5, 5)
    P_block=block_approximation(A,blocksize )
    if plot: plt.imshow(P_block)

    if plot: 
        fig, axes = plt.subplots(1,2 ,figsize=(15, 15)); 
        axes[0].spy(A)
        axes[1].imshow(P_block)


def test_block_approximation():
    """
    """
    #do the block transformation
    arr = np.arange(24).reshape(6, 4)  
    #arr_block_approx = arr.copy()
    blocksize = (3, 2)
    arr_block_approx = block_approximation(arr,blocksize )
    #inspect the obtained array
    blocked = blockwise_view(arr, blocksize)    
    blocked_approx = blockwise_view(arr_block_approx, blocksize)    
    
    shape_reduced = blocked.shape[0:2]
    for i in range(shape_reduced[0]):
        for j in range(shape_reduced[1]):
            assert blocked[i][j].sum() ==  blocked_approx[i][j].sum() 



def block_approximation(A, blocksize, inplace=False):
    """
    spread the mass in each block on the whole block
    DENSE, UNDIRECTED case.
    
    TODO: optimize undir
    TODO: sparse case ???
    TODO: do the same with numba ?
    
    input:
    -------
    A: numpy.ndarray
    
    output
    --------
    """
    #TODO: check symmetric array
    if not A.ndim==2: raise ValueError('must be 2 dim array')
    if inplace: 
        raise NotImplementedError   # TODO: check array type. if is double, ok. ELse: error
    else:
        A_cp = A.copy().astype(float)
        #block view
        blocked = blockwise_view(A_cp, blocksize)
    shape_reduced = blocked.shape[0:2]
    blocklen = blocked.shape[2]*blocked.shape[3]
    #loop blocks
    for i in range(shape_reduced[0]):
        for j in range(shape_reduced[1]):
            s = blocked[i][j].sum()
            if i==j:
                #correction for the diagonal block
                blocked[i][j] = s / (blocklen -    blocked.shape[2])
                di = np.diag_indices( blocked.shape[2] )
                blocked[i][j][di]=0.
            else:    
                blocked[i][j] = s / blocklen
    return A_cp   

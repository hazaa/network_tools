import sys         
dir_TAT="/home/aurelien/local/git/extern/Trophic-Analysis-Toolbox/Python_files"                           
sys.path.append(dir_TAT) 

import os
from os.path import join, getsize 

import numpy as np
import igraph as ig

from network_casd.tools import trophic_incoherence, trophic_levels,get_largest_cc

#import networkx as nx
import pandas as pd 
from scipy.linalg import eigh
import matplotlib.pyplot as plt
from scipy.sparse import diags, lil_matrix

from scipy.sparse.linalg import spsolve


import trophic_tools as ta

def test_walk_network_files():
    """
    """
    path = '../../data/network_data_johnson_samuel/'
    walk_network_files(path)

def walk_network_files(path):
    """
    try to reproduce Fig.1 in [JJ17] using dataset from : https://www.samuel-johnson.org/data
    """
    list_files = []
    dirs_l = []
    label_l=[]
    #get names of directorys
    for root, dirs, files in os.walk(path): 
        dirs_l.append(dirs)
    dirs_l =dirs_l[0]
    #get all filenames
    for root, dirs, files in os.walk(path): 
        for name in files: 
            list_files.append(join(root, name))
   #count nb of files per dir         
    for root, dirs, files in os.walk(path): 
        l = len(files)
        if l>0:
            label_l.append(l)
    #get directory name of each file
    label_a = np.repeat(dirs_l,label_l)
    

    #TODO: save category in df
    size_l = []
    label_l = []
    F0_l = []
    success = np.zeros(len(list_files),dtype=int)
    n_success =0
    for i,f in enumerate(list_files):
        #create graph
        ok = 1
        try:
            g = ig.Graph.Read_Edgelist(f)
            
            #df_edges = pd.read_csv(f, sep='\t',header=None,names=['dest','src'])
            # REMIND: F0 is insensitive to edge direction reversal
            #Graphtype = nx.DiGraph()
            #G =  nx.from_pandas_edgelist(df_edges, source='src', target='dest', create_using=Graphtype)            

        except Exception as inst:
            ok = 0
            print('--failed creating g: {}--'.format(f))  
        #retry  with another separator
        if not ok:    
            try:
                df_edges = pd.read_csv(f, sep=' ',header=None)
                g.add_edge_list(df_edges.values.tolist())
                Graphtype = nx.DiGraph()
                G =  nx.from_pandas_edgelist(df_edges, source='src', target='dest', create_using=Graphtype)            
                ok=1
            except Exception as inst:
                ok = 0
                print('--failed creating g: {}--'.format(f))  
                print(type(inst))     
                print(inst.args)              
        #solve for trophic level             
        if ok:    
            try:   
                #F_0, h = ta.trophic_incoherence(G)
                g_sub = get_largest_cc(g,mode="weak")
                h_sub = trophic_levels(g_sub)
                g_sub.vs['trophic']=h_sub.tolist()
                F_0 = trophic_incoherence(g_sub,use_weight=False)
            except Exception as inst:
                ok = 0
                print('--failed solving: {}--'.format(f))  
                print(type(inst))     
                print(inst.args)    
                #print('t.N={}'.format(t.N))
        #compute eigenvalue           
        if ok:    
            try:  
                F0_l.append(F_0)
                size_l.append(g_sub.vcount() )
                #TODO: save all params in df
                print('--success:{}--'.format(f))  
                n_success+=1
                success[i]=1
            except Exception as inst:
                ok = 0
                print('--failed computing ??: {}--'.format(f))  
                print(type(inst))     
                print(inst.args)       

    print("n_success={}".format(n_success))       
    #save TODO    
    #plot   
    #TODO:   px.scatter()
    fig, ax = plt.subplots(1, 1, figsize=(10, 10))
    ax.scatter(size_l,F0_l,label=label_a[success>0])
    ax.set_xlabel('size')
    ax.set_ylabel('F_0')
    ax.legend()
    
    fig = pe.scatter(pd.DataFrame({'F0':F0_l,"size":size_l, "category":label_a[success>0]}),
                    x="size",y="F0",color="category" ) 
    fig.write_image('F0_johnson.pdf')    
    fig.show()

def test():
    url=dir_TAT+'/ythan_E.csv'
    df = pd.read_csv(url)
    #df.head(6)
    Graphtype = nx.DiGraph()
    G_ythan = nx.from_pandas_edgelist(df, source='source', target='target',edge_attr='weight', create_using=Graphtype)
    G = G_ythan
    ta.trophic_levels(G)
    ta.trophic_plot(G,k=1)
    F_0, h = ta.trophic_incoherence(G)

    print('Trophic incoherence =',round(F_0,3))

    # get eigvects
    Lambda = get_laplacian(G)
    w, v = eigh(Lambda.todense(),eigvals_only = False)
    #checks:
    assert np.isclose(w[0],0) # 0-th eigenvalue can be (very) slightly negative because of numerical.
    assert np.all(w[1:]>=0) 
    #plot
    plt.matshow(v)
    # project over modes: see [Koeth eq 3.2]

# 
def get_laplacian(G):
    """
    code below is taken from Trophic-Analysis-Toolbox, but Laplacian is slightly modified, see comment.

    #L[0,0 ]= 0         # MOD AH. this is necessary for solving (see mackay p..) but has bad side-effects: 1 eigenvalue of Lambda is <0

    Refs:
    
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.linalg.eigh.html#scipy.linalg.eigh
    """
    G2 = G.to_undirected(reciprocal=False,as_view=True)
    if nx.is_connected(G2):
        W = nx.adjacency_matrix(G)                  # weighted adjacency matrix as Compressed Sparse Row format sparse matrix of type '<class 'numpy.longlong'>'
        in_weight = W.sum(axis=0).A1          # Eq.2.1
        out_weight = W.sum(axis=1).A1         # Eq.2.1
        u = in_weight + out_weight            # (total) weight Eq.2.2
        #v = in_weight - out_weight            # imbalance Eq.2.3 (the difference between the flow into and out of the node)
        L = diags(u, 0) - (W + W.transpose()) # (weighted) graph-Laplacian operator Eq.2.5
        #L[0,0 ]= 0         # MOD AH. this is necessary for solving (see mackay p..) but has bad side-effects: 1 eigenvalue of Lambda is <0
    return L


def trophic_levels(g):
    """
    adapted from in Trophic Analysis Toolbox by B.Sansom (using networkx)
    https://github.com/BazilSansom/Trophic-Analysis-Toolbox
    
    parameter:
    ---------------
    g: graph-tool
    a directed graph (weighted or not)
    
    output:
    ----------
    h: np.array
    the trophic level
    
    see arxiv:2001.05173
    """
    l = gt.label_largest_component(g,directed=False)
    #if not np.all(l.a):  #THIS FAILS : WHY ??
    #    raise ValueError('graph must be weakly connected')
    W = gt.adjacency(g)
    in_weight = W.sum(axis=0).A1          # Eq.2.1
    out_weight = W.sum(axis=1).A1         # Eq.2.1
    u = in_weight + out_weight            # (total) weight Eq.2.2
    v = in_weight - out_weight            # imbalance Eq.2.3 (the difference between the flow into and out of the node)
    L = diags(u, 0) - (W + W.transpose()) # (weighted) graph-Laplacian operator Eq.2.5
    L[0,0 ]= 0
    h = spsolve(L, v)                     # solve Eq.2.6 for h using sparse solver spsolve
    h = np.round(h - h.min(),decimals=10) # put lowest node at zero and remove numerical error
    return h

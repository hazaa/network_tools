"""
TROPHIC COHERENCE

Related implementations:

* python dense: 
    https://networkx.org/documentation/stable/reference/algorithms/centrality.html#trophic
        (trophic_levels, trophic_differences, trophic_incoherence_parameter)
* python sparse: 
    https://github.com/BazilSansom/Trophic-Analysis-Toolbox
    
    https://github.com/jklaise/coherent-network-tools
* in c++:  https://www.samuel-johnson.org/

Refs: 
    [JJ17],[Johnson, Jones 17] Johnson, Jones "Looplessness in networks is linked to trophic coherence"  http://www.pnas.org/cgi/doi/10.1073/pnas.1613786114
    [Johnson et al. 2014]  "Trophic coherence determines food-web stability"  
                                        Johnson,Dom ́ınguez-Garc ́ıa Donetti, Mu ̃noz
                                        10.1073/pnas.1409077111 , arXiv:1404.7728
                                        
"""
import numpy as np
import pandas as pd
import networkx as nx
from scipy.sparse.linalg import spsolve,lsqr
from scipy.linalg import lu_factor, lu_solve
from scipy.sparse import diags
from scipy.sparse import diags, lil_matrix
from scipy.sparse.linalg import eigs
import matplotlib.pyplot as plt

import graph_tool.all as gt

from scipy.stats import dlaplace,zipf,planck

import os
from os.path import join, getsize 

import warnings

from network_tools.trophic_layout import ForceAtlas
from network_tools.measure import get_SCC, get_cycle_count

from itertools import product

from scipy.sparse import csr_matrix, eye


def test_motifs():
    """
    cf [MacKay et al.20] Figure 11 'feedback loop', etc...
    """ 
    plt.switch_backend("cairo")# see gt doc "Integration with matplotlib"
    
    edges=[ [(0,1),(1,2),(2,0)],   #[MacKay et al.20] Figure 11 'feedback loop'
                   [    (0,1),(1,2)],           # 'chain' 
                   [    (0,0),(0,1),(1,2),(2,0)], #'motif3', self-loop
                   [    (0,1),(0,2),(1,2)], #"feed forward loop"
                   [ (0,2),(1,2)],   # not in MacKay
                   [(0,1),(1,2),(2,1)],  
                   [(0,1),(0,2)],
                   [(0,1),(0,2),(1,2),(2,1)]
                        ] 
    fig, axes = plt.subplots(3, 3)
    axes_flat = axes.flatten()
    for i,e in enumerate(edges):
        g = gt.Graph(directed=True)
        g.add_edge_list(e)                                                   
        t = Trophic(g)
        try:
            t.run()        
            print("i={} q={:1.1f} tau={:1.1e}".format(i, t.q,t.tau))    
        except:               
            print("{} fail".format(i))   
        gt.graph_draw(g,mplfig=axes_flat[i])    
    fig.savefig("gt-mpl.pdf")
"""                        
    edges=[ (0,1),(1,2),(2,0),   #[MacKay et al.20] Figure 11 'feedback loop'
                       (3,4),(4,5),           # 'chain' 
                       (6,6),(6,7),(7,8),(8,6), #'motif3'
                       (9,10),(9,11),(10,11) #"feed forward loop"
                        ]                        
    g = gt.Graph(directed=True)
    g.add_edge_list(edges)
    comp,hist = gt.label_components(g, directed=False)
    #loop across components
    for c in np.unique(comp.a):
        idx = (c==comp.a)
        g_filt = gt.GraphView(g ,vfilt=idx)
        g_filt.purge_vertices() 
        g_filt.purge_edges()
        t = Trophic(g_filt)
        t.run()        
        print(t.q)
"""

def compute_npath_ncycle_bipartite_dicm():
    """
    compute nb of paths, nb of cycles in the bipartite directed configuration model.
    compare to theoretical formula
    """
    # make adjacency
    K=0.1
    x=K*np.random.rand(1000)
    y=K*np.random.rand(1000)
    u=K*np.random.rand(1001)
    v=K*np.random.rand(1001)
    Nx  = x.shape[0]
    Nu  = u.shape[0]
    B_R = np.outer(y, v)
    #L_R = B_R.sum()
    B_L = np.outer(u, x)
    #L_L = B_L.sum()
    #np.sum(1/L_L*B_L, axis=0)
    Pup = np.hstack( [ np.zeros((Nx,Nx)), B_R])
    L_R = Pup.sum()
    Pdown = np.hstack( [ B_L, np.zeros((Nu,Nu))])
    L_L = Pdown.sum()
    P = np.vstack([Pup,Pdown])
    A = 1*(np.random.rand(Nx+Nu,Nx+Nu)<P)
    A_sp = csr_matrix(A)
    # compute branching factor
    alpha_xy = np.dot(x,y)
    alpha_uv = np.dot(u,v)
    # compute successive powers
    n_nu_l = [] #nb of paths
    m_nu_l = []#nb of cycles
    n_nu_th_l = [] #nb of paths
    m_nu_th_l = []#nb of cycles
    #A_pow = np.eye(Nx+Nu)
    A_pow = eye(Nx+Nu)
    #A_sq = A @ A
    A_sq = A_sp@A_sp
    n=4
    for i in range(n) :
        A_pow = A_pow@A_sq
        n_nu_l.append( A_pow.sum()  )
        m_nu_l.append( A_pow.diagonal().sum() )
        dum = (alpha_xy * alpha_uv)**(i+1)
        n_nu_th_l.append( dum*( L_R/alpha_xy + L_L/alpha_uv ) )
        m_nu_th_l.append( 2* dum )
           
    fig,ax = plt.subplots(3,1)
    ax[0].scatter(n_nu_th_l , n_nu_l)
    ax[1].scatter(m_nu_th_l ,m_nu_l)
    x = list(range(n))
    ax[2].plot(x, np.array(m_nu_l)/ np.array(n_nu_l) )
    c_nu = 2/( L_R/alpha_xy + L_L/alpha_uv ) 
    ax[2].plot([0,n] , c_nu*np.array([1,1]))
    """
    Comment:
    * m_nu and n_nu are accurate, in the particular case.
    * c_nu is approximately constant, and is close to the theoretical value
    """
    # check alpha
    g = gt.Graph(directed=True)
    g.vp['type']=g.new_vp('int')
    g.add_edge_list(np.array(A_sp.nonzero()).T)
    g.vp['type'].a[0:1000]=1
    vp_deg_tot = g.degree_property_map("total")
    g_filt = gt.GraphView(g ,vfilt=vp_deg_tot.a>0 )
    g_filt.purge_edges()
    g_filt.purge_vertices()
    t = Trophic(g_filt,bipartite_vprop='type')
    t.run()      
    assert np.isclose(t.alpha ,  np.sqrt(alpha_xy*alpha_uv) )
    plt.spy(gt.adjacency(g),markersize=0.1)
    """
    Comment: failure !!!!!!!!!!!!!!!!!
    """

def test_random_basal_bipartite():
    """
    """
    NB= 100;  N=   4000 ; LB=  500 ; L= 10000
    #NB= 100;  N=   1000 ; LB=  500 ; L= 5000
    g= make_random_graph_basal(NB,N,LB,L,bipartite=True)
    t = Trophic(g)
    t.run()  
    print("q={}".format(t.q))
    print("q_basal={}".format(t.q_basal))
    print('mean 0nb {}  theor ={}'.format( t.trophic_level.a[ g.vp['b'].a==1].mean(),  t.L/t.LB+2 ))
    print('mean 1 {} theor ={}'.format( t.trophic_level.a[ g.vp['b'].a==2].mean(), t.L/t.LB+1 ))
        
    g_mono= make_random_graph_basal(NB,N,LB,L,bipartite=False)
    t_mono = Trophic(g_mono)
    t_mono.run()  
    print("q={}  theor ={}".format(t_mono.q, t_mono.q_basal ))
    
    fig, ax = plt.subplots(4, 1, figsize=(10, 10))
    ax[0].hist(t.trophic_level.a,50)
    ax[1].hist(t.trophic_level.a[ g.vp['b'].a==1 ],50)
    ax[2].hist(t.trophic_level.a[ g.vp['b'].a==2 ],50)
    ax[3].hist(t_mono.trophic_level.a,50)
    
    t.plot_graph()
    """
    Comments:
    * bipartite: snb is below L/Lb+1
    * bipartite: q_basal higher than measured. why ?
    * 
    """

def test_make_dataset_random_basal():    
    df = make_dataset_random_basal(beta=0.,bipartite=True)
    fig, ax = plt.subplots(2, 1, figsize=(10, 10))
    ax[0].scatter(df['q_basal_mono'],df['q'])
    ax[0].set_xlabel('q_basal_mono')
    ax[0].plot([0,4],[0,4], 'r--')
    ax[1].scatter(df['q_basal'],df['q'])
    ax[1].set_xlabel('q_basal_bi')
    ax[1].plot([0,4],[0,4], 'r--')
    """
    Comment: 
    * q/q_b fit is bad: q_basal higher than measured. why ?
    * beta influences the result !!!!!!!!!!!!!
    """

def make_dataset_random_basal(bipartite=False,beta=0.5):
    """
    directed bipartite random model: 0basal, 0nonbasal, 1
    aggregate edge array:    
    [0,0,LB ; 
     0,0, (L-LB)/2 ;
     0, (L-LB)/2, 0 ]
    
    """
    NB  = [100, 200]
    N  =[500, 1000]
    LB=  [200, 400,1000]
    L = [1100,2000,3000] 
    it = [0]
    
    n_comb = len(NB)*len(N)*len(LB)*len(L)*len(it)
    alpha= pd.Series( index = range(n_comb),dtype=int ) 
    q= pd.Series( index = range(n_comb) ,dtype=float) 
    q_basal= pd.Series( index = range(n_comb) ,dtype=float) 
    q_basal_mono= pd.Series( index = range(n_comb) ,dtype=float) 
    df = pd.DataFrame( index = range(n_comb) , columns = ['NB','N','LB', 'L', 'it'],
                                    dtype=float)
    """
    from gt documentation:
    b:  Group membership for each node.
    probs: "value probs[r,s] corresponds to the average number of edges between groups r and s "
    micro_ers: If true, the microcanonical version of the model will be evoked, where the numbers of edges between groups will be given exactly by the parameter probs, and this will not fluctuate between samples.
    """
    for i,(NB_,N_,LB_,L_,it_) in enumerate( product(NB,N,LB,L,it) ):
        df.iloc[i,:] = [NB_,N_,LB_,L_,it_]
        g=make_random_graph_basal(NB_,N_,LB_,L_, beta= beta,bipartite=bipartite)               
        t = Trophic(g)
        t.run()       
        q[i] = t.q
        q_basal_mono[i] = np.sqrt(L_/LB_-1)    
        q_basal[i] = np.sqrt(L_/LB_-LB_/L_)    
        #s_alpha[i] = t.alpha
        del t 
    df['q']=q     
    df['q_basal_mono']=q_basal_mono     
    df['q_basal']=q_basal
    return df#,s_alpha,df  

def q_basal_bi(beta,L,LB) :
    """
    """
    return np.sqrt( 4*beta**2 * L/LB - 8*beta**2+4*beta + 4*beta*(1-beta)*LB/L )
    
def s0nb_basal_bi(beta,L,LB):
    """
    """
    return ( 2 + LB/( LB+beta*(L-LB) ))/(1- beta*(L-LB)/(LB+beta*(L-LB)))
    

def test_make_random_graph_basal_beta():
    """
    """ 
    NB_,N_,LB_,L_ = 100,1000,150,3000
    bipartite=True
    q_l = []
    qb_l = []
    beta_l = np.linspace(0.1,0.9,9)
    s1_l=[]
    s1_theor_l=[]
    s0nb_l=[]
    s0nb_theor_l=[]
    for beta in beta_l:
        g=make_random_graph_basal(NB_,N_,LB_,L_, bipartite=bipartite,beta=beta)               
        t = Trophic(g)
        t.run()       
        #q[i] = t.q
        #q_basal_mono[i] = np.sqrt(L_/LB_-1)        
        #print("beta = {}   q={}    qb ={}".format(beta, t.q,  q_basal_bi(beta,L_,LB_)  ) )
        q_l.append( t.q)
        qb_l.append( q_basal_bi(beta,L_,LB_)  )
        s0nb_l.append(  t.trophic_level.a[ g.vp['b'].a==1].mean() ) 
        s1_l.append(  t.trophic_level.a[ g.vp['b'].a==2].mean()  ) 
        s0 = s0nb_basal_bi(beta,L_,LB_) 
        s0nb_theor_l.append( s0 ) 
        s1_theor_l.append(s0-1)
    print("q_b={}".format(np.sqrt(L_/LB_-1) )      )
    fig, ax = plt.subplots(3, 1, figsize=(10, 10))
    ax[0].plot(s0nb_theor_l, s0nb_l)
    ax[0].set_xlabel('s0nb_theor')
    ax[0].set_ylabel('s0nb')
    ax[1].plot(s1_theor_l, s1_l)
    ax[1].set_xlabel('s1_theor')
    ax[1].set_ylabel('s1')
    ax[2].plot(qb_l, q_l)
    ax[2].set_xlabel('qb')
    ax[2].set_ylabel('q')    
    """
    Comment:
    * s0nb and s1 computed theoretically FAIL !!!!!
    * q vs qb: FAIL!!!
    """

def make_random_graph_basal(NB,N,LB,L,beta = 0.5,bipartite=False):
    """
    generate poisson bipartite with a basal layer
    beta in 0,1
    """
    micro_ers = True
    if bipartite:
        b = np.hstack([np.zeros(NB), 1*np.ones(int((N-NB)/2)), 2*np.ones(int((N-NB)/2))  ])
        #probs = np.array([[0,0,LB],    [0,0,int((1-beta)/2*(L-LB)/2)],  [0,int((1+beta)/2*(L-LB)/2),0]])
        probs = np.array([[0,0,LB],    [0,0,int(beta*(L-LB))],  [0,int((1-beta)*(L-LB)),0]])
    else:
        b = np.hstack([np.zeros(NB), 1*np.ones(int((N-NB))) ])
        probs = np.array([[0,LB],[0, L-LB]])    
    g = gt.generate_sbm(b, probs, directed=True, micro_ers=micro_ers,
                                        micro_degs=False)
    if bipartite: assert gt.is_bipartite(g)    
    #TODO: add vp 
    vprop = g.vp["b"] = g.new_vp("int")
    vprop.a = b
    return g


def test_alpha_monopartite_poisson():
    """
    check difference between alpha when in-out degree is correlated, or not 
    """
    def deg_sample_uncor_poisson():
        n=20
        return np.random.poisson(n), np.random.poisson(n)
    
    def deg_sample_cor_poisson():
        n=20;a=0.8
        kin= np.random.poisson(n)
        return kin, kin + dlaplace.rvs(a)
    
    """def deg_sample_uncor_zipf():
        a=2
        return zipf.rvs(a),  zipf.rvs(a)
    
    def deg_sample_cor_zipf():
        a=1.5;b=1
        kin = zipf.rvs(a) 
        return kin, kin + planck.rvs(b)"""
    
    def sample_k(max):
        accept = False
        while not accept:
            k = np.random.randint(1,max+1)
            accept = np.random.random() < 1.0/k
        return k

    deg_sample_uncor = deg_sample_uncor_poisson
    deg_sample_cor = deg_sample_cor_poisson
    """deg_sample_uncor =  lambda: (sample_k(20),sample_k(20)) # SLOWWWWW
    def deg_sample_cor():
        b=1
        kin=sample_k(10)
        return  kin,kin + planck.rvs(b)"""
    
    alpha_mono_uncor=[]
    alpha_mono_cor=[]
    for i in range(1):
        #generate
        try:
            g_cor = gt.random_graph(1000, deg_sample_cor)
            g_uncor = gt.random_graph(1000, deg_sample_uncor)
            
            #check alpha
            alpha_mono_uncor.append( get_alpha_monopartite(g_uncor) )
            alpha_mono_cor.append(get_alpha_monopartite(g_cor))
        except: pass    
    print("uncor: E={}  std={}".format( np.mean(alpha_mono_uncor),np.std(alpha_mono_uncor) ) )     
    print("cor: E={}  std={}".format( np.mean(alpha_mono_cor),np.std(alpha_mono_cor) ) )   
    """
    Comment:
    * alpha difference is small comparing cor to uncor (but significant) 
    """
    # compare to direct sampling  
    np.mean([np.product(deg_sample_uncor()) for i in range(100)])/(2*n) 
    np.mean([np.product(deg_sample_cor()) for i in range(100)])/(2*n) 
    """
    Comment:
    * results are coherent
    """
    #check degree distrib
    fig, ax = plt.subplots(2, 2, figsize=(10, 10))
    for i,g in enumerate([g_uncor,g_cor]):
        vp_in_deg = g.degree_property_map('in') 
        count_in = np.bincount(vp_in_deg.a)
        vp_out_deg = g.degree_property_map('out') 
        count_out = np.bincount(vp_out_deg.a)
        ax[i,0].plot( range(count_in.shape[0]), count_in)    
        ax[i,1].plot( range(count_out.shape[0]), count_out)    
    """
    Comment:
    * nothing special
    """
    #check degree correlation 
    hist_cor = gt.combined_corr_hist(g_cor, "in", "out")    
    hist_uncor = gt.combined_corr_hist(g_uncor, "in", "out")    
    fig, ax = plt.subplots(1, 2, figsize=(10, 10))
    ax[0].imshow(hist_uncor[0].T, interpolation="nearest", origin="lower")
    #ax[0].colorbar()
    ax[0].set_xlabel("uncor in-degree")
    ax[0].set_ylabel("out-degree")
    ax[1].imshow(hist_cor[0].T, interpolation="nearest", origin="lower")
    #ax[1].colorbar()
    ax[1].set_xlabel("cor in-degree")
    ax[1].set_ylabel("out-degree")    
    """
    Comment:
    *corr plot is ok
    *poisson:alpha difference is small comparing cor to uncor 
    *zipf:
    *NB: analytic result for <kin kout> not straightforward AFAIK.
    """    

def alpha_mono_vs_bipartite():
    """
    compute alpha for a monopartite graph, then a bipartite graph, with the same kin,kout
    
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.dlaplace.html#scipy.stats.dlaplace
    """
    def deg_sample_uncor_poisson():
        return np.random.poisson(20), np.random.poisson(20)
    
    n=1000
    block = np.zeros(n)
    block[0:int(n/2)]=1
    def prob_bi(a, b):
        if a == b:
            return 0
        else:
            return 1
    g_mono = gt.random_graph(n, deg_sample_uncor_poisson)
    g_bi,vp = gt.random_graph(n, deg_sample_uncor_poisson, block_membership = block,edge_probs=prob_bi)
    g_bi.vp['type']=vp
    #check 
    print(get_alpha_monopartite(g_mono) )
    print(get_alpha_monopartite(g_bi) )
    print(get_alpha_bipartite(g_bi,'type') )
    """
    Comment:
    * get_alpha_monopartite(g_mono) = get_alpha_monopartite(g_bi)
    * get_alpha_bipartite returns ~ *2
    """

"""
def make_dataset_random_bipartite_3layer():
    
    #bipartite with 3 layers
    
    N1  = [100, 200]
    N2  =[500, 1000]
    N3 = [500, 1000]
    L12=  [200, 400,1000]
    L23 = [1000,2000,3000] 
    L32 = [1000,2000,3000] 
    it = [1,1,1]
    
    n_comb = len(N1)*len(N2)*len(N3)*len(L12)*len(L23)*len(L32)*len(it)
    s_alpha= pd.Series( index = range(n_comb),dtype=int ) 
    s_q= pd.Series( index = range(n_comb) ,dtype=int) 
    df = pd.DataFrame( index = range(n_comb) , columns = ['N1','N2','N3', 'L12', 'L23', 'L32'],
                                    dtype=float)
    
    for i,(n1,n2,n3,l12,l23,l32,_) in enumerate( product(N1,N2,N3,L12,L23,L32,it) ):
       df.iloc[i,:] = [n1,n2,n3,l12,l23,l32]
       g = make_random_bipartite_3layer(n1,n2,n3,l12,l23,l32) 
       t = Trophic(g)
       t.run()       
       s_q[i] = t.q
       s_alpha[i] = t.alpha
       del t  
    return s_q,s_alpha,df   

    

def make_random_bipartite_3layer(N1,N2,N3, L12, L23, L32):
    #
    #create a directed Erdos-Renyi 3 layer bipartite network. Number of nodes inside layers are N1,N2,N3.
    #Number of inter layer links are L12,L23,L32.
    #Basal nodes are in layer 1. There is no incoming link to layer 1
    
    g = gt.Graph(directed=True)
    g.add_vertex(N1+N2+N3)
    edgelist = zip( np.random.randint(0,N1, L12) , np.random.randint(N1,N1+N2, L12) )
    g.add_edge_list(edgelist)
    edgelist = zip( np.random.randint(N1,N1+N2, L23),np.random.randint(N1+N2,N1+N2+N3, L23) )
    g.add_edge_list(edgelist)
    edgelist = zip( np.random.randint(N1+N2,N1+N2+N3, L32),np.random.randint(N1,N1+N2, L32) )
    g.add_edge_list(edgelist)
    assert  gt.is_bipartite(g)
    return g
    #gt.random_rewire(g, model='erdos')
"""

def test_plot_graph():
    """
    """
    filename = '../network_datasets/data/ythan_E.csv'
    g = gt.load_graph_from_csv(filename,directed=True,hashed=True,skip_first=True)
    t = Trophic(g)
    t.run()
    t.plot_graph()    

def test_layout_forceatlas_ythan():
    """
    """
    filename = '../network_datasets/data/ythan_E.csv'
    g = gt.load_graph_from_csv(filename,directed=True,hashed=True,skip_first=True)
    t = Trophic(g)
    t.run()
    pos_init = np.zeros([ g.num_vertices(),2 ])
    pos_init[:,1]=t.trophic_level.a
    pos_init[:,0]=np.random.rand(g.num_vertices() )
    
    #convert to undirected
    gu = gt.GraphView(g, directed=False)
    adjacency = gt.adjacency(gu,operator=False)
    adjacency.setdiag(0) 
    force_atlas = ForceAtlas()
    embedding = force_atlas.fit_transform(adjacency,pos_init=pos_init)
    embedding.shape
    
    #rescale
    xmin,xmax = np.min(embedding[:,0]),np.max(embedding[:,0])
    ymin,ymax = np.min(embedding[:,1]),np.max(embedding[:,1])
    a = (xmax-xmin)/(ymax-ymin)
    embedding[:,1] *= a
    
    #plot
    pos = g.new_vertex_property("vector<double>")  
    for v in g.vertices():
        i = g.vertex_index[v]
        pos[v] = embedding[i,:]
    gt.graph_draw(g, pos=pos)


def test_synthetic_1():
    """
    test case taken from Trophic-Analysis-Toolbox/Python_files/demo_trophic_analysis.ipynb
    """
    edges = [(1,2,1),(3,2,1),(4,2,1),(2,5,1),(2,6,1),(2,7,1)] 
    g = gt.Graph(directed=True)
    g.add_edge_list(edges)
    g.remove_vertex(0)
    t = Trophic(g)
    #t.is_weakly_connected()
    #assert t.g_is_weakly_connected
    t.get_basal_nodes()
    t.in_component_with_basal()
    t.get_basal_properties()
    assert t.NB == 3
    assert t.LB ==3
    t.get_trophic_levels()
    assert np.all(t.trophic_level.a==(1+[0, 1, 0, 0, 2, 2, 2])) 
    t.get_trophic_differences()
    assert np.all( t.trophic_difference.a==[1,1,1,1,1,1])
    t.get_trophic_differences_mean()
    assert t.trophic_difference_mean==1.
    t.get_q()
    assert t.q ==0.
    t.get_alpha()
    #assert t.alpha = 
    t.get_q_basal()
    #assert q_basal=
    t.get_tau()
    #assert t.tau=
    t.get_largest_eig()
    t.lambda1
    t.sanity_check()
    print('lambda1 = {}  exp(tau) ={} '.format(t.lambda1,  np.exp(t.tau)))

def test_synthetic_1_vs_nx():
    """
    """
    edges = [(1,2,1),(3,2,1),(4,2,1),(2,5,1),(2,6,1),(2,7,1)] 
    #
    G=nx.DiGraph()                                               
    G.add_weighted_edges_from(edges)
    q_nx = nx.trophic_incoherence_parameter(G)
    #
    g = gt.Graph(directed=True)
    g.add_edge_list(edges)
    g.remove_vertex(0)
    t = Trophic(g)
    t.run()
    #
    assert t.q == q_nx   

def test_synthetic_2_vs_nx():
    """
    """
    edges = [(1,2,1),(2,3,1),(1,3,1)]
    #
    G=nx.DiGraph()                                               
    G.add_weighted_edges_from(edges)
    q_nx = nx.trophic_incoherence_parameter(G)
    #
    g = gt.Graph(directed=True)
    g.add_edge_list(edges)
    g.remove_vertex(0)
    t = Trophic(g)
    t.run()
    #
    assert t.q == q_nx    

def test_synthetic_2():
    """
    test case taken from Trophic-Analysis-Toolbox/Python_files/demo_trophic_analysis.ipynb
    """
    edges = [(0,1),(1,2),(0,2)]
    g = gt.Graph(directed=True)
    g.add_edge_list(edges)
    t = Trophic(g)
    t.run()

def test_trophic_food():
    """
    """
    g = gt.collection.data["serengeti-foodweb"]
    t = Trophic(g)
    t.run()
    t.get_stats()
    """
    Comment: lambda1 should be 0 instead of 1. But: look at JJ17 SI Fig S2: many food webs
    have low tau with lambda1 = 1
   
    """
def test_iterative_solver():
    """
    https://docs.python.org/3/library/os.html#os.walk
    """
    """
    filename = '../../data/network_data_johnson_samuel/FoodWebs/Ythan96.dat'
    filename = '../../data/network_data_johnson_samuel/FoodWebs/benguela.dat' #FAILS
    filename = '../../data/network_data_johnson_samuel/FoodWebs/Berwicktxt.dat'
    g = gt.load_graph_from_csv(filename,directed=True,hashed=True,skip_first=False,
                                                        csv_options={"delimiter": "\t"} )
    """
                                                        
    filename = '../network_datasets/data/ythan_E.csv'
    g = gt.load_graph_from_csv(filename,directed=True,hashed=True,skip_first=True)
    #get weakly connected component
    g_largest = gt.extract_largest_component(g, directed=False,prune=True)
    t = Trophic(g)
    t.run()
    t.run(s0=t.trophic_level.a )
    df = t.get_stats()
    print(df)
    assert np.isclose( t.tau == -1.32)
    """
    Comment: 
    * baf value: alpha_ratio, tau
    * good: q, q_ratio
    """


def test_trophic_food_ythan():
    """
    https://docs.python.org/3/library/os.html#os.walk
    """
    """
    filename = '../../data/network_data_johnson_samuel/FoodWebs/Ythan96.dat'
    filename = '../../data/network_data_johnson_samuel/FoodWebs/benguela.dat' #FAILS
    filename = '../../data/network_data_johnson_samuel/FoodWebs/Berwicktxt.dat'
    g = gt.load_graph_from_csv(filename,directed=True,hashed=True,skip_first=False,
                                                        csv_options={"delimiter": "\t"} )
    """
                                                        
    filename = '../network_datasets/data/ythan_E.csv'
    g = gt.load_graph_from_csv(filename,directed=True,hashed=True,skip_first=True)
    #get weakly connected component
    g_largest = gt.extract_largest_component(g, directed=False,prune=True)
    t = Trophic(g_largest)
    t.run()
    df = t.get_stats()
    print(df)
    assert np.isclose( t.tau == -1.32)
    """
    Comment: 
    * baf value: alpha_ratio, tau
    * good: q, q_ratio
    """

def test_walk_network_files():
    """
    """
    path = '../../data/network_data_johnson_samuel/'
    df=walk_network_files(path)

def walk_network_files(path):
    """
    try to reproduce Fig.1 in [JJ17] using dataset from : https://www.samuel-johnson.org/data
    """
    list_files = []
    for root, dirs, files in os.walk(path): 
        for name in files: list_files.append(join(root, name))
    #TODO: save category in df
    tau_l = []
    lambda1_l=[] 
    df_stats = None
    n_success =0
    for f in list_files:
        #create graph
        ok = 1
        try:
            df_edges = pd.read_csv(f, sep='\t',header=None)
            #df_edges = pd.read_csv(f, header=None,engine='python')
            g = gt.Graph(directed=True)
            # g.add_edge_list(df_edges.values.tolist())
            g.add_edge_list(df_edges[[1,0]] .values.tolist()) #REVERSE
        except Exception as inst:
            ok = 0
            """print('--failed creating g: {}--'.format(f))  
            print(type(inst))     
            print(inst.args)          """
        #retry  with another separator
        if not ok:    
            try:
                df_edges = pd.read_csv(f, sep=' ',header=None)
                #df_edges = pd.read_csv(f, header=None,engine='python')
                g = gt.Graph(directed=True)
                g.add_edge_list(df_edges.values.tolist())
                ok=1
            except Exception as inst:
                ok = 0
                print('--failed creating g: {}--'.format(f))  
                print(type(inst))     
                print(inst.args)              
        #solve for trophic level             
        if ok:    
            try:   
                #vp_total_deg = g.degree_property_map('total')            
                #g = gt.GraphView(g, vfilt=  vp_total_deg.a>0)
                t = Trophic(g)
                t.run(solver_args=dict(solver="spsolve"))    
            except Exception as inst:
                ok = 0
                print('--failed solving: {}--'.format(f))  
                print(type(inst))     
                print(inst.args)    
                #print('t.N={}'.format(t.N))
        #compute eigenvalue           
        if ok:    
            try:  
                t.get_largest_eig()
                tau_l.append( t.tau)
                lambda1_l.append( t.lambda1)
                #TODO: save all params in df
                print('--success:{}--'.format(f))  
                n_success+=1
                if df_stats is None:
                    df_stats= t.get_stats(index=f[-20:-4],lambda1=True,ncycle=False)
                else:    
                    df_stats = pd.concat([df_stats, t.get_stats(index=f[-20:-4],lambda1=True,ncycle=False) ])
            except Exception as inst:
                ok = 0
                print('--failed computing lambda1: {}--'.format(f))  
                print(type(inst))     
                print(inst.args)       

    print("n_success={}".format(n_success))       
    #save TODO    
    #plot   
    #TODO:   px.scatter()
    fig, ax = plt.subplots(1, 1, figsize=(10, 10))
    ax.scatter(tau_l,lambda1_l )
    x = np.linspace( np.nanmin(tau_l), np.nanmax(tau_l), 20)
    ax.plot(x,np.exp(x), 'r--')
    ax.set_xlabel('tau')
    ax.set_ylabel('lambda1')
    return df_stats

def compare_Trophic_Analysis_Toolbox():
    pass

def test_Trophic_in_component_with_basal():
    """
    """
    g = gt.Graph(directed=True)
    e_l = [(0,1),(1,2), (3,4),(4,5),(5,3)]
    g.add_edge_list(e_l)
    t = Trophic(g)
    t.get_basal_nodes()
    t.in_component_with_basal()
    assert( np.all(t.vp_in_component_with_basal.a == np.array([1,1,1,0,0,0])))


def get_alpha_monopartite_old(g):
    """
    get <k_in k_out>/<k> for a monopartite graph
    """
    vp_in_deg = g.degree_property_map('in') 
    vp_out_deg = g.degree_property_map('out') 
    vp_total_deg = g.degree_property_map('total') 
    product =  g.new_vertex_property("int",val=0)
    #gt.map_property_values( gprop, product,  lambda x: x[0]*x[1])#FAILS
    for v in g.vertices():
        product[v] = vp_in_deg[v]*vp_out_deg[v]
    alpha = (product.a.mean()/vp_total_deg.a.mean()).tolist()
    return alpha



def get_alpha_monopartite(g):
    """
    get <k_in k_out>/<k> for a monopartite graph
    """
    vp_in_deg = g.degree_property_map('in') 
    vp_out_deg = g.degree_property_map('out') 
    vp_total_deg = g.degree_property_map('total') 
    #product =  g.new_vertex_property("int",val=0)
    product=[]
    total = []
    #gt.map_property_values( gprop, product,  lambda x: x[0]*x[1])#FAILS
    for v in g.vertices():
        product.append( vp_in_deg[v]*vp_out_deg[v] )
        total.append(vp_total_deg[v]  )
    alpha = np.mean(product)/ np.mean(total)
    return alpha

def get_alpha_bipartite(g,label_mode):
    """
    compute alpha in bipartite network
    
    label_mode: str
    label of vprop that identifies the chosen mode in the bipartite network
    
    THIS IS SLOW
    """
    vp_in_deg = g.degree_property_map('in') 
    vp_out_deg = g.degree_property_map('out') 
    #vp_total_deg = g.degree_property_map('total') 
    vprop = g.vp[label_mode]
    #product =  g.new_vertex_property("int",val=0)
    #gt.map_property_values( gprop, product,  lambda x: x[0]*x[1])#FAILS
    """
    product0=[]
    product1=[]
    deg_total0=[]
    deg_total1=[]

    for v in g.vertices():
        if vprop[v]:
            product1.append( vp_in_deg[v]*vp_out_deg[v])
            deg_total1.append( vp_total_deg[v] )
        else:
            product0.append( vp_in_deg[v]*vp_out_deg[v])    
            deg_total0.append( vp_total_deg[v] )
            
    alpha0 = np.mean(product0)/np.mean(deg_total0)        
    alpha1 = np.mean(product1)/np.mean(deg_total1)        
    alpha = np.sqrt(alpha0*alpha1)
    """
    ku_l=[]; kv_l=[]; kx_l=[]; ky_l=[]
    for v in g.vertices():
        if vprop[v]:
            kx_l.append( vp_out_deg[v])
            ky_l.append( vp_in_deg[v])
        else:
            ku_l.append( vp_in_deg[v])
            kv_l.append( vp_out_deg[v])
    Lxy=np.sum(kx_l)  
    Luv = np.sum(ky_l) 
    #alpha = np.sqrt( np.array(x_l).dot(np.array(y_l)) *  np.array(u_l).dot(np.array(v_l)) /(np.sum(u_l)*np.sum(x_l) ))
    return np.array(kx_l).dot(np.array(ky_l)) /Lxy ,  np.array(ku_l).dot(np.array(kv_l))/ Luv,Lxy,Luv

class Trophic:
    """
    Compute relevant quantities for the coherence ensemble defined in [JJ17]
    
    Following notations in graph-tool:
    vp = vertex property
    ep = edge property
    """
    def __init__(self,g,bipartite_vprop=None ):
        self.g = g
        self.g_filt=None
        self.trophic_level = None #vp, node's trophic levels
        self.trophic_difference= None #ep, difference of trophic levels along edges
        self.trophic_difference_mean= None
        self.largest_component = None #vp, node belongs to largest compo
        self.vp_basal = None #vp, node is basal
        self.vp_in_component_with_basal =None#vp
        #self.g_is_weakly_connected=None
        self.N = None #number of nodes
        self.NB=None # number of basal nodes
        self.L=None #number od edges
        self.LB=None # number of basal edges
        self.lambda1=None # eigv with largest real part
        self.q = None
        self.q_basal = None
        self.k_mean  = None
        self.alpha = None
        self.alpha_bipartite =None
        self.alpha_basal = None
        self.tau = None
        self.s_nb_basal = None # cf JJ17 SI eq.7
        self.trophic_difference_negative = None
        self.bipartite_vprop = bipartite_vprop
        self.proba_acyclic =None #cf JJ17 eq.20
    def run(self,solver_args=dict(solver="spsolve", s0=None)):
        """
        run all important computations in adequate order.
        """    
        #self.is_weakly_connected()
        self.check_g()
        self.get_basal_nodes()
        self.in_component_with_basal()
        self.get_basal_properties()
        self.get_trophic_levels(**solver_args)
        self.get_trophic_differences()
        self.get_trophic_differences_mean()
        self.get_q()
        self.get_k_mean()
        self.get_alpha()
        self.get_q_basal()
        self.get_tau()
        self.get_proba_acyclic()
        #self.get_largest_eig()
        self.get_trophic_difference_negative()
        self.lambda1=None
        self.sanity_check()
        #print('lambda1 = {}  exp(tau) ={} '.format(self.lambda1,  np.exp(self.tau)))

    """def is_weakly_connected(self):
        
        #check g is weakly connected. if not, compute a filtered graph
        
        #comp, hist = gt.label_components(self.g,directed=False)
        l = gt.label_largest_component(self.g,directed=False)
        if np.all(l.a).tolist() :
            self.g_is_weakly_connected = True
        else:     
            raise NotImplementedError
            #get ep with 1 if s in largest connected component
            self.largest_component=l 
            #filter and copy to g_filt  
            g_filt = gt.GraphView(self.g, vfilt=l) """
    def in_component_with_basal(self):
        """
        find nodes belonging to a component without basal node
        """
        if self.vp_basal is None: raise RuntimeError('vp_basal  must be computed first')

        comp,hist = gt.label_components(self.g, directed=False)
        self.vp_in_component_with_basal  = self.g.new_vertex_property("bool",val=False)
        #loop across components
        for c in np.unique(comp.a):
            idx = (c==comp.a)
            #flag nodes that belong to basal component
            if np.any( idx * self.vp_basal.a) :
                   self.vp_in_component_with_basal.a[idx]=True
    def get_g(self):
        """
        get the relevant graph depending on connectivity g: if g is weakly connected, return g.
        Else, return a filtered version.
        """
        #check if all nodes are in undir component with basal node
        if np.all(self.vp_in_component_with_basal.a):
            return self.g
        else:
            #if not work with g_filt
            #raise NotImplementedError
            return gt.GraphView(self.g, vfilt = self.vp_in_component_with_basal)
    def check_g(self):
        """
        """
        vp_total_deg = self.g.degree_property_map('total')         
        nb_isolated = (1*(vp_total_deg.a==0)).sum().tolist()
        if nb_isolated>0:
            warnings.warn('isolated nodes')
            #raise RuntimeError('isolated nodes')
            
        if not self.g.is_directed:   raise RuntimeError('g must be directed')
    def get_basal_nodes(self):
        """
        make vp property map with basal (= indegree==0)
        """ 
        #vp_in_deg = self.g.degree_property_map('in') 
        #self.vp_basal  = self.g.new_vertex_property("bool")
        vp_in_deg = self.g.degree_property_map('in') 
        self.vp_basal  = self.g.new_vertex_property("bool")
        gt.map_property_values(vp_in_deg, self.vp_basal,  lambda x: x==0)
    def get_basal_properties(self):
        """
        compute number of basal nodes, number of basal edges, alpha basal. 
        see [JJ17] p.5619.
        """
        g= self.get_g()
        #compute NB  (sum of property map), 
        if self.vp_basal==None: raise RuntimeError('vp_basal  must be computed first')
        #self.NB = self.vp_basal.a.sum().tolist()
        self.NB=np.sum([1 for v in g.vertices() if v.in_degree()==0])
        #compute LB (for each node in this set, compute out-degree and get sum)            
        #TODO: make this more efficient
        #g_basal = gt.GraphView(g, vfilt = vp_basal)
        #self.LB = np.sum([v.out_degree() for v in g_basal.vertices()]) #FAILS ??????????,
        self.LB = np.sum([v.out_degree() for v in g.vertices() if v.in_degree()==0])
        #compute alpha basal cf [JJ17] eq.(7)
        self.L = g.num_edges()
        self.N = g.num_vertices()
        self.alpha_basal = (self.L -self.LB  )/(self.N - self.NB)#
        self.s_nb_basal = self.L /self.LB  +1 # JJ17 SI eq.(8)
    def get_trophic_levels(self, solver="spsolve",s0=None):    
        """
        compute trophic level from sparse adjacency matrix, see [Johnson et al.2014] eq.(??)
        
        REFS:
        https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.linalg.spsolve.html
        https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.linalg.lsqr.html#scipy.sparse.linalg.lsqr
        """
        g= self.get_g()    
        #get adj AND transpose (NB: convention for adj matrix in gt and networkx != )
        A = gt.adjacency(g, operator=False).transpose()
        vp_in_deg = g.degree_property_map('in') 
        v = np.maximum(vp_in_deg.a, 1)
        lam = diags(v, 0, dtype=int) - A
        #lam = diags(v, 0, dtype=float) - A
        # solve for trophic levels
        if solver=="spsolve":
            s = spsolve(lam.T, v)  
        elif solver=="lsqr":             
            s =  lsqr(lam.T, v,x0=s0)[0]   
        elif solver=="LU":
            lu, piv = lu_factor(lam.T.todense())
            s = lu_solve((lu, piv), v)
        #check nan
        if np.any(np.isnan(  s)): raise ValueError('nan trophic level') 
        #make new vp, copy h into it
        self.trophic_level =  g.new_vertex_property("double")
        self.trophic_level.a = s

    def get_trophic_differences(self):    
        """
        compute trophic_differences $x_{ij}=s_i-s_j$, see [JJ17] p.5619
        code below is an adaptation of github.com/jklaise/coherent-network-tools to graph-tool
        
        see also:
        
            * code in Trophic-Analysis-Toolbox (is dense, not usable for large graphs).
            * code in github.com/jklaise/coherent-network-tools uses edge list, is adapted below.
            (https://github.com/jklaise/coherent-network-tools in coherent_network.py:coherence_stats() )
        """ 
        if  self.trophic_level is None: raise RuntimeError('trophic levels must be computed first')
         
        g= self.get_g()    
        #create ep  
        self.trophic_difference  = g.new_edge_property("double",val=0.)
        for e in g.edges():
             self.trophic_difference[e] = self.trophic_level[ e.target()  ] -  self.trophic_level[  e.source() ]
    def get_trophic_difference_negative(self):
        """
        """
        self.trophic_difference_negative = np.sum( 1*(self.trophic_difference.a <0   ))
    def get_trophic_differences_mean(self):
        """
        compute $<x_{ij}>$ with $x_{ij}=s_i-s_j$, see [JJ17] p.5619
        """  
        self.trophic_difference_mean = self.trophic_difference.a.mean().tolist()          
    def get_q(self):
        """
        compute the trophic coherence $q$ in [JJ17] eq.(5)
        """  
        if self.trophic_difference is None: raise RuntimeError('trophic differences must be computed first')
        if self.q is None: 
            self.q = self.trophic_difference.a.std().tolist()             
    def get_largest_eig(self):
        """
        
        in [Johnson, Jones 17] "A is not in general, symmetric, its eigvs will be complex. (..) of particular
            interest is the eigv with the largest real part" p.5619
        """
        g= self.get_g()    
        #copied from graph-tool/doc/build/spectral.html
        A = gt.adjacency(g, operator=True)
        # https://docs.scipy.org/doc/scipy/reference/reference/generated/scipy.sparse.linalg.eigs.html#scipy.sparse.linalg.eigs
        # 'LR' = largest real
        try:
            e=eigs(A, k=1, which="LR", return_eigenvectors=False)[0] 
            self.lambda1 =np.real( e )
        except:
            print("could not find eigenvalue")    
    def get_k_mean(self):
        """
        get <ktot>
        """
        g= self.get_g()
        vp_total_deg = g.degree_property_map('total') 
        total = []
        for v in g.vertices():
            total.append(vp_total_deg[v]  )
        self.k_mean =  np.mean(total)
    def get_alpha(self):    
        """
        compute $\alpha$ in [JJ17] eq.(2)
        """
        g= self.get_g()
        if self.bipartite_vprop is None:
            self.alpha = get_alpha_monopartite(g)
        else:
            alpha_xy,alpha_uv,Lxy,Luv = get_alpha_bipartite(g,self.bipartite_vprop)
            self.alpha = np.sqrt(alpha_xy*alpha_uv ) 
            self.alpha_bipartite = [alpha_xy,alpha_uv,Lxy,Luv]
            #self.alpha = get_alpha_bipartite(g)
        """
        self.alpha = np.mean( [v.out_degree()*v.in_degree() for v in g.vertices() ])
        self.alpha /= np.mean( [(v.out_degree()+v.in_degree()) for v in g.vertices() ])
        """ 
        
    def get_q_basal(self):
        """        
        compute $\tilde{q}$ in [JJ17] eq.(6) 
        """
        if self.LB is None: raise RuntimeError('LB must be computed first')
        g= self.get_g()
        if self.bipartite_vprop is None:
            self.q_basal = np.sqrt( g.num_edges()/self.LB -1 )
        else:    
            self.q_basal = np.sqrt( g.num_edges()/self.LB - self.LB/g.num_edges())

    def get_tau(self):
        """
        compute $\tau$ in [JJ17] eq.(1)
        """
        if self.alpha is None: raise RuntimeError('alpha must be computed first')
        if self.q is None: raise RuntimeError('q must be computed first')
        if self.q_basal is None: raise RuntimeError('q_basal must be computed first')
        
        if self.q==0: 
            self.tau = -np.inf
        else:
            self.tau = np.log(self.alpha)+0.5*(1/self.q_basal**2 - 1/self.q**2)
    def path_cycle_length(self):
        """
        compute nb of paths and nb of cycles, compare to expectation in the coherence ensemble
        WARNING: do not try it in the loopy regime
        """
        g= self.get_g()
        c= gt.all_circuits(g)
        len_c=[]
        n=0
        #this is slow
        for c_ in c: 
            len_c.append(len(c_))
            n+=1
            if n>9999:break
        len_circuit_l = np.bincount(len_c)    
        A=gt.adjacency(g,operator=False)  # change this ???
        if self.bipartite_vprop is None:
            A_mult = A
        else:
            A_mult = A@A
        nu_l = [] #nb of paths
        m_nu_l = []#nb of cycles    
        m_nu_theor_l = []#nb of cycles    
        n=6
        A_pow = eye(A.shape[0] )
        for i in range(1,n) :
            A_pow = A_pow@A_mult
            #n_nu_l.append( A_pow.sum()  )
            m_nu_l.append( A_pow.diagonal().sum())    
            nu_l.append(2*i)
            m_nu_theor_l.append( 2*self.q_basal/self.q * np.exp(2*i*self.tau))
        fig,ax = plt.subplots(1,1)
        #ax[0].semilogy( nu_l , n_nu_l, label=r'$\sum (A^\nu)_{ij}$' )
        if False:
            ax.plot(  range(len(len_circuit_l)) , len_circuit_l, 'g--', label='gt count' )
            ax.plot(nu_l , m_nu_l,'r--',label=r'$m_\nu$' )    
            ax.plot(nu_l , m_nu_theor_l,'r--',label=r'$m_\nu^{th}$' )    
        else: 
            ax.semilogy(  range(len(len_circuit_l)) , len_circuit_l, 'g--', label='gt count' )
            ax.semilogy(nu_l , m_nu_l,'r--',label=r'$m_\nu$' )    
            ax.semilogy(nu_l , m_nu_theor_l,'r--',label=r'$m_\nu^{th}$' )    
   
            ax.legend()
        """ BELOW: has to be checked
        # compute powers
        nu_l = range(2,13)
        nu_a = np.array(nu_l)
        A=gt.adjacency(g,operator=False)  # change this ???
        A_pow = A.copy()
        for _ in nu_l :
            A_pow = A@ A_pow
            n_nu_l.append( A_pow.sum()  )
            m_nu_l.append( A_pow.diagonal().sum() )
        # other method: compute <\lambda^n>    
        A_op = gt.adjacency(g,operator=True)
        e = eigs(A_op,k=100, which="LR",return_eigenvectors=False)
        e_pow = e.copy()
        m_nu_lambda = [ ]
        for _ in nu_l:
            e_pow = np.multiply(e_pow,e) # element-wise multiplication
            m_nu_lambda.append( np.real( e_pow.mean())) 
        # other method: theoretical expectation in basal ensemble
        m_nu_basal = self.alpha**(nu_a) # cf JJ17 SI eq.(2)
        # other method: theoretical expectation in coherence ensemble
        n_nu_basal = self.L * self.alpha**(nu_a-1) # cf SI §1.1-2 and main text p.5620
        m_nu_coherence = (self.alpha_basal * self.q_basal)/(self.alpha * self.q)* np.exp(self.tau * nu_a) #cf JJ17 eq(11)
        # plot    
        fig,ax = plt.subplots(2,1)
        ax[0].semilogy( nu_l , n_nu_l, label=r'$\sum (A^\nu)_{ij}$' )
        ax[0].semilogy( nu_l , n_nu_basal,'r--',label='basal' )
        ax[0].set_xlabel(r'$\nu$')
        ax[0].set_ylabel(r'$n_\nu$')
        ax[0].legend()
        ax[1].plot( nu_l , m_nu_l, label=r'$\sum (A^\nu)_{ij}$' )
        ax[1].plot( range(len(len_circuit_l)) , len_circuit_l, 'g--', label='circuit' )
        ax[1].plot( nu_l , m_nu_coherence,'r--',label='coherence' )
        ax[1].plot( nu_l , m_nu_basal,'k--',label='basal' )
        ax[1].plot( nu_l , m_nu_lambda,'y--',label='lambda' )
        ax[1].set_xlabel(r'$\nu$')
        ax[1].set_ylabel(r'$m_\nu$')   
        ax[1].legend()
         """   
        return len_circuit_l
    def get_proba_acyclic(self):
        """
        cf JJ17 eq.20 
        """       
        x =  -(self.alpha_basal*self.q_basal)/(self.q *self.alpha) * 1./( np.exp(-self.tau) -1) 
        self.proba_acyclic = np.exp(x)
    def sanity_check(self):   
        """
        """
        # this is a consequence of the definition. see [Johnson, Jones 17] p.5619, second footnote.
        if not self.trophic_difference_mean==1.:
            msg = 'self.trophic_difference_mean!=1.'
            #raise ValueError(msg)
            warnings.warn(msg)
            #print(self.trophic_difference_mean)
         # si=0 for all basal nodes
    def get_stats(self, index=None,lambda1=False,ncycle=False) :
        """
        """
        if index is None: index='g'

        df = pd.DataFrame(index = [index],columns=['N','N_B', 'q'],dtype=float) 
        df.loc[(index,'N')]  = self.N
        df.loc[(index,'N_B')]  = self.NB
        df.loc[(index,'k_mean')]  = self.k_mean
        df.loc[(index,'q')]  = self.q
        df.loc[(index,'q_basal')]  = self.q_basal
        df.loc[(index,'q_ratio')]  = self.q/self.q_basal
        df.loc[(index,'alpha')]  = self.alpha
        if not self.bipartite_vprop is None:
            df.loc[(index,'alpha_bi_xy')]  = self.alpha_bipartite[0]
            df.loc[(index,'alpha_bi_uv')]  = self.alpha_bipartite[1]
            df.loc[(index,'alpha_bi_Lxy')]  = self.alpha_bipartite[2]
            df.loc[(index,'alpha_bi_Luv')]  = self.alpha_bipartite[3]
        df.loc[(index,'alpha_ratio')]  = self.alpha/self.alpha_basal
        df.loc[(index,'tau')]  = self.tau
        #df.loc[(index,'tau_basal')]  = np.log(self.alpha_basal)
        df.loc[(index,'exp(tau)')]  = np.exp(self.tau)
        df.loc[(index,'SCC')]  = get_SCC(self.g)
        df.loc[(index,'proba_acyclic')]  = self.proba_acyclic
        if ncycle:
            df.loc[(index,'ncycle')] = get_cycle_count(self.g)
        if self.lambda1 is None:
            if lambda1:
                self.get_largest_eig()
                df.loc[(index,'lambda1')]  = self.lambda1
        return df
        
    def plot_pdf(self,axes=None):
        """
        
        in the DiCM ensemble: 
                pdf(s_i)  is the  ?
                pdf(x_ij) is the  ?
        in the basal ensemble:
                pdf(s_i)  is the sum of two dirac distribution
                pdf(x_ij) is the sum of two dirac distribution
        """
        nb_bins = 50
        #
        if axes is None:
            fig, axes = plt.subplots(2, 1, figsize=(10,10))
        # si: histogram
        axes[0].hist(self.trophic_level.a,nb_bins)
        axes[0].set_xlabel(r'$s_i$')
        # xij: histogram
        axes[1].hist(self.trophic_difference.a,nb_bins)
        axes[1].set_xlabel(r'$x_{ij}$')
        # plot tau vs lambda1 ; background = all empirical networks in Johnson's db.
        # plot : against basal ensemble sample. Q: how many iters ??
        # plot: against  trophic coherence ensemble sample (GPPM). Q: how many iters ??
    def plot_pdf_basal(self,axes=None):
        """
        plot histogram of s_i and x_ij in the basal ensemble (see JJ17 SM p.3)
        """
        if axes is None:
            fig, axes = plt.subplots(2, 1, figsize=(10,10))        
        # si: histogram    
        #  basal
        x = [1,self.s_nb_basal ]
        y = [self.NB, self.N-self.NB]
        axes[0].stem(x,y,'r')            
        # xij: histogram        
        x = [0,self.L/self.LB ]
        y = [ 1-self.LB/self.L, self.LB/self.L ]
        axes[1].stem(x,y,'r')
    def plot_graph(self,n_iter=50,output=None, layout = 'force_atlas', layout_args={}):
        """
        plot graph with ForceAtlas layout
        NB: layout in Trophic Analysis Toolbox is not suitable for large graph
        """
        g = self.g
        if layout == 'force_atlas':
            pos_init = np.zeros([ g.num_vertices(),2 ])
            pos_init[:,1]=self.trophic_level.a
            #pos_init[:,0]=gt.contiguous_map(g.vp['category']).a   
            pos_init[:,0]=np.random.rand(g.num_vertices() )
            
            #convert to undirected
            gu = gt.GraphView(g, directed=False)
            adjacency = gt.adjacency(gu,operator=False)
            adjacency.setdiag(0) 
            #run modified forceatlas
            force_atlas = ForceAtlas(n_iter=n_iter)
            embedding = force_atlas.fit_transform(adjacency,pos_init=pos_init)
            embedding.shape
            
            #rescale
            xmin,xmax = np.min(embedding[:,0]),np.max(embedding[:,0])
            ymin,ymax = np.min(embedding[:,1]),np.max(embedding[:,1])
            a = (xmax-xmin)/(ymax-ymin)
            embedding[:,1] *= a
            
            #plot
            pos = g.new_vertex_property("vector<double>")  
            for v in g.vertices():
                i = g.vertex_index[v]
                pos[v] = embedding[i,:]
        elif layout=="radial":
            v_root = g.add_vertex()
            for v_end in g.vertices():
                if  self.trophic_level[v_end] ==1:
                    g.add_edge(v_root,v_end )      
            pos = gt.radial_tree_layout(g, v_root, rel_order=self.trophic_level, **layout_args)
            g.remove_vertex(v_root)
        gt.graph_draw(g, pos=pos,output=output)


from scipy.sparse import csr_matrix
#import igraph as ig
import pandas as pd
import numpy as np

import json
    
import graph_tool.all as gt    
    
from warnings import warn

def get_adj(df,lab_src,lab_dest,weighted=False):
    """
    see: network_dataset/population.py
    """
    node_name_1 = pd.concat(  [df[lab_src], df[lab_dest] ]).unique()
    node_name_1.sort()
    n1 = node_name_1.shape[0] 
    lookup1 = pd.Series(range(n1),index = node_name_1)
    row= lookup1.loc[ df[lab_src] ].values
    col= lookup1.loc[ df[lab_dest] ].values
    data = np.ones_like(row)
    if weighted:
        return csr_matrix((data, (row, col)), shape=(n1,n1)), lookup1.index.values.tolist()
    else:
        return csr_matrix((data, (row, col)), shape=(n1,n1), dtype=bool), lookup1.index.values.tolist()   

def test_add_contiguous_index_to_named_edgelist():
    """
    """
    df = pd.DataFrame({'ID_UL_MERE': ['123456','123456'], 'ID_UL_FILLE': ['234567','345678'], 
                    'TXCONTRA': [0.9,0.5], 'TYPE_LIAISON':['D','D']  })
    src_col_label='ID_UL_MERE'
    tgt_col_label='ID_UL_FILLE'
    df_extended = add_contiguous_index_to_named_edgelist(df,
                                            src_col_label,tgt_col_label, index=None)
    df_extended['idx_src'].values
    assert np.allclose(df_extended['idx_src'].values ,np.array([0, 0]) )
    assert np.allclose(df_extended['idx_tgt'].values ,np.array([1, 2]) )

def add_contiguous_index_to_named_edgelist(df,src_col_label,tgt_col_label, index=None,
                                                                        src_col_idx_label='idx_src',tgt_col_idx_label='idx_tgt'):
    """
    Add a contiguous index to names in edgelist stored in pd.DataFrame
    
    Parameters:
    ----------------
    df: pd.DataFrame
    datarfame where edge info is stored 
    
    index: pd.DataFrame
    pd.DataFrame with two columns: 'name' and 'index'
    'name' corresponds to the content of df[src_col_label] and df[tgt_col_label] 
    'index' is a unique integer
    
    src_label: str
    column of df that contains names of source nodes
    
    tgt_label: str
    column of df that contains names of target nodes
    
    """
    # get a unique integer for each name
    if index is None:
        name = pd.concat([df[src_col_label].rename({src_col_label:'name'}),
                                 df[tgt_col_label].rename({tgt_col_label:'name'})
                                ]).unique()
        name.sort()
        index = pd.DataFrame({ 'name': name, 'idx':range(name.shape[0])})
    # join src index
    df=pd.merge(df, index.rename(columns={'name':src_col_label},inplace=False), on=src_col_label ).rename(columns={'idx':src_col_idx_label},inplace=False)
    # join tgt index
    df=pd.merge(df, index.rename(columns={'name':tgt_col_label},inplace=False), on=tgt_col_label ).rename(columns={'idx':tgt_col_idx_label},inplace=False)
    return df


def test_graph_from_json():
    """
    
    Usage:
    -----------
    from network_tools.io import *
    import cobra
    from os.path import join    
    #https://cobrapy.readthedocs.io/en/latest/loopless.html
    from cobra.flux_analysis.loopless import  loopless_solution
    """

    data_dir = "data/"
    #filename = 'iAB_RBC_283.json'
    filename ="iJR904.json" 

    model = cobra.io.load_json_model(join(data_dir, filename))
    solution = model.optimize()
    #print(model.summary())
    #print(solution)
    
    g,info = graph_from_json(join(data_dir, filename),solution_flux=solution.fluxes)
    #g, info = graph_from_json(join(data_dir, filename))
    """
    from network_tools.trophic import *
    t = Trophic(g,bipartite_vprop='type') 
    t.run()
    t.get_stats(lambda1=True,ncycle=True) 
    """
    loopless = loopless_solution(model)
    g,info = graph_from_json(join(data_dir, filename),solution_flux=loopless.fluxes)

    
    
    
def graph_from_json(filename,solution_flux=None):
    """
    create gt graph from json metabolic file.
    
    Parameters:
    ----------------
    filename: str
    name ot the file
    
    Outputs:
    ------------
    g: gt.Graph
    g has a vertex proeprty 'type', with value 0 for metabolites and 1 for reactions
    
    Usage:
    ----------
    http://bigg.ucsd.edu/models/iAB_RBC_283
    
    
    see also: https://github.com/opencobra/cobrapy/blob/devel/src/cobra/io/json.py
    """
    if isinstance(filename, str):
        with open(filename, "r") as file_handle:
            info={}
            obj = json.load(file_handle)
            info['nb_reversible_reaction'] = 0
            #make look-up tables            
            look_up_metabolite={}
            n=0
            for d in obj['metabolites']:
                look_up_metabolite[d['id']]=n
                n+=1   
            look_up_reaction={}
            m=n
            for d in obj['reactions']:
                look_up_reaction[d['id']]=m
                m+=1    
            # create graph and vertices
            g = gt.Graph(directed=True)
            g.add_vertex(m)
            vtype = g.vp["type"] = g.new_vp("int",val=0)
            vname = g.vp["name"] = g.new_vp("string")
            vtype.a[n:]=1
            #iterate through reactions to add edges
            for d in obj['reactions']:
                rate_reaction = None
                idx_reaction =  look_up_reaction[ d['id'] ]
                lower_bound = d['lower_bound']
                upper_bound = d['upper_bound']
                g.vp['name'][idx_reaction]= d['id'] 
                    
                if solution_flux is None:
                    if lower_bound<0: info['nb_reversible_reaction'] +=1
                else:
                    rate_reaction = solution_flux[ d['id']  ]
                    
                #iterate through metabolites involved in this reaction
                for metabolite, stoech_coef in d['metabolites'].items():
                    idx_metabolite =  look_up_metabolite[ metabolite ]    
                    g.vp['name'][idx_metabolite]= metabolite 
                    
                    if rate_reaction is None:
                        #solution flux is not provided, relying on rate bounds to get direction
                        if lower_bound>=0:
                            #add only 1 direction
                            if stoech_coef<0:
                                g.add_edge(idx_metabolite,idx_reaction)
                            else:    
                                g.add_edge(idx_reaction,idx_metabolite)
                        else:
                            #reaction is reversible : both directions are added
                            g.add_edge(idx_metabolite,idx_reaction)       
                            g.add_edge(idx_reaction,idx_metabolite)
                    else:
                    # the rate of the particular solution is used to choose the preferred direction
                    # of reversible reactions    
                        if rate_reaction == 0:
                            pass
                        elif rate_reaction>0:
                            if stoech_coef<0:
                                g.add_edge(idx_metabolite,idx_reaction)
                            else:    
                                g.add_edge(idx_reaction,idx_metabolite)                        
                        elif rate_reaction<0:
                            if stoech_coef>0:
                                g.add_edge(idx_metabolite,idx_reaction)
                            else:    
                                g.add_edge(idx_reaction,idx_metabolite)                                                    
                        else: raise ValueError('reaction rate is invalid')        
            #post-process g
            vp_total_deg = g.degree_property_map('total')    
            g_filt = gt.GraphView(g, vfilt=vp_total_deg.a>0)
            g_filt.purge_edges()
            g_filt.purge_vertices()
            return g_filt, info     
    else:
        raise ValueError('filename must be string')            

#---------------------------------
#DEPRECATED 

def test_get_sparse_csr():
    """
    """
    df_interactions = pd.DataFrame({'acheteur_id':['Z','X'] , 'titulaire_id':['b','a']} )
    node_name_1 =df_interactions['titulaire_id'].unique()
    node_name_1.sort()
    n1 = node_name_1.shape[0] 
    lookup1 = pd.Series(range(n1),index = node_name_1)
    #lookup1 = lookup1.sort_index(inplace=False)
    assert lookup1.is_monotonic 
    node_name_2 = df_interactions['acheteur_id'].unique() #astype(str) 
    node_name_2.sort()
    n2 = node_name_2.shape[0] 
    lookup2 = pd.Series(range(n2),index = node_name_2)
    #lookup2 = lookup2.sort_index(inplace=False)
    assert lookup2.is_monotonic 
    
    biadj = get_sparse_csr(df_interactions[['titulaire_id','acheteur_id']],lookup1,lookup2)

    assert np.all(biadj.todense()==np.matrix([[1,0 ],[0,1]])) 



def get_sparse_csr_new(df,lookup1,lookup2):
    """
    see: decp-net/dynamic_net.py
    see: network_dataset/population.py
    
    node_name_1 =df_merge['COM_source'].unique()
    node_name_1.sort()
    n1 = node_name_1.shape[0] 
    lookup1 = pd.Series(range(n1),index = node_name_1)
    df_merge['COM_source_idx']= lookup1.loc[ df_merge['COM_source'] ].values
    df_merge['COM_destination_idx']= lookup1.loc[ df_merge['COM_destination'] ].values
    #then create
    """
    pass

def get_sparse_csr(df,lookup1,lookup2):
    """
    DEPRECATED - TOO SLOW
    
    make sparse csr matrix from transaction dataframe
    df has two columns. first (resp 2nd) column correponds to lookup1 (resp. lookup2)
    
    Refs: 
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.csr_matrix.html#scipy-sparse-csr-matrix:
        "csr_matrix((data, indices, indptr), [shape=(M, N)])
        is the standard CSR representation where the column indices for row i are stored in indices[indptr[i]:indptr[i+1]] 
        and their corresponding values are stored in data[indptr[i]:indptr[i+1]]."
        
    algo:
            indptr[0]=0
            for i in range(layer 1): 
                query linked elements in layer 2. 
                get their index idx from lookup2
                c= count()
                indptr[i+1]=indptr[i] +c
                data[indptr[i]:indptr[i+1]]=np.ones(c)
                indices[indptr[i]:indptr[i+1]]=idx
    
    input:
    --------
    df: pd.DataFrame
    
    lookup1: pd.Series  
                
    lookup2: pd.Series              
          
    output:
    ---------
    mat : sparse.csr_matrix            
    """
    #check dtype
    indptr=[0]
    data=[]
    indices=[]
    if df.shape[1] != 2: raise ValueError('df must have 2 columns')
    for i,val in enumerate(lookup1.index):
        new_indices = lookup2[ df[ df.iloc[:,0]==val ].iloc[:,1] ].values
        count = new_indices.shape[0]
        indptr.append(indptr[-1] +count)
        data = data + count*[1]
        indices = indices+ new_indices.tolist()
    return csr_matrix((data, indices, indptr)) #csr_matrix((data, indices, indptr), [shape=(M, N)])

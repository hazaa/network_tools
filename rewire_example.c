/*
 * TEST FUNCTIONS FOR rewire.c 
 * 
 * ------------
 *  COMPILE:
 * 
 * $gcc rewire.c  `pkg-config --static --libs --cflags igraph` -c
 * $gcc rewire.o rewire_example.c  `pkg-config --static --libs --cflags igraph` -lgsl -lgslcblas  -o  rewire_example
 * 
 *  RUN:
 * $./rewire_example
 * 
*/

#include <igraph.h>
#include <math.h>
#include "rewire.h"


void print_vector(igraph_vector_t *v) {
    long int i, l = igraph_vector_size(v);
    for (i = 0; i < l; i++) {
        printf(" %li", (long int) VECTOR(*v)[i]);
    }
    printf("\n");
}

int test_igraph_curveball(){

    
 return 0;   
}

/*
 * copied and adapted from "Calculating various properties of graphs" https://igraph.org/c/html/latest/igraph-Tutorial.html#tut-lesson-2
 * */
int test_igraph_vector_difference_sorted_first_nodes(){


  igraph_t graph;
  //https://igraph.org/c/html/latest/igraph-Generators.html#igraph_small
  igraph_small(&graph, 0, IGRAPH_DIRECTED, 0, 2, 0,3, 1, 3, 1, 4, -1);
  //igraph_small(&graph, 0, IGRAPH_DIRECTED, 0, 2, 0,3,0,4, 1, 4, 1, 5,1,6, -1);
  igraph_write_graph_edgelist(&graph, stdout);
  /*
  igraph_vector_t v;
  //igraph_vector_t result;
  igraph_real_t edges[] = { 0, 1, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8,
                            0,10, 0,11, 0,12, 0,13, 0,17, 0,19, 0,21, 0,31,
                            1, 2, 1, 3, 1, 7, 1,13, 1,17, 1,19, 1,21, 1,30,
                            2, 3, 2, 7, 2,27, 2,28, 2,32, 2, 9, 2, 8, 2,13,
                            3, 7, 3,12, 3,13, 4, 6, 4,10, 5, 6, 5,10, 5,16,
                            6,16, 8,30, 8,32, 8,33, 9,33,13,33,14,32,14,33,
                           15,32,15,33,18,32,18,33,19,33,20,32,20,33,
                           22,32,22,33,23,25,23,27,23,32,23,33,23,29,
                           24,25,24,27,24,31,25,31,26,29,26,33,27,33,
                           28,31,28,33,29,32,29,33,30,32,30,33,31,32,31,33,
                           32,33
  };

  igraph_vector_view(&v, edges, sizeof(edges) / sizeof(double));
  igraph_create(&graph, &v, 0, IGRAPH_DIRECTED);*/

  igraph_vector_difference_sorted_first_nodes(&graph);

  //igraph_vector_init(&result, 0);
  //igraph_vector_destroy(&result);
  igraph_destroy(&graph);
   
 return 0;   
}

int test_igraph_i_rewire_q() {
    igraph_t g,g_cp;
    igraph_adjlist_t al,al_cp;
    igraph_vector_t outdeg, indeg, vec;
    igraph_bool_t is_simple;
    igraph_real_t mean, std;
    int len,i,n_rewire = 1000;
    igraph_bool_t use_adjlist=1;
    igraph_integer_t num_successful_swaps,cum_num_successful_swaps=0;
    igraph_real_t T = 0.001, value;
    igraph_vector_t s,x;
    igraph_vector_init_real(&s, 0.);
    igraph_vector_init_real(&x, 0.);
    
    // Set random seed for reproducibility 
    igraph_rng_seed(igraph_rng_default(), 42);

    // gen graph
    igraph_barabasi_game( &g,
                                          20000,
                                         1.0,
                                          2,
                                         0,
                                          0,
                                          1.0,
                                         IGRAPH_DIRECTED,
                                          IGRAPH_BARABASI_PSUMTREE,
                                         0);
    long int no_of_edges = igraph_ecount(&g);                   
    IGRAPH_CHECK(igraph_vector_resize(&x, no_of_edges));	
                    
                                         
    // copy graph. see https://igraph.org/c/doc/igraph-Basic.html#igraph_copy
    igraph_copy(&g_cp, &g);
    
    /*
         // Get PageRank
    igraph_arpack_options_t o;       
    igraph_arpack_options_init(&o);   
     
  igraph_pagerank(&g, IGRAPH_PAGERANK_ALGO_PRPACK,
                    &s, &value,
                    igraph_vss_all(), IGRAPH_DIRECTED,
                    0.85,NULL,
                    NULL );*/
/*          
    igraph_pagerank(&g, IGRAPH_PAGERANK_ALGO_ARPACK,
                    &s, &value,
                    igraph_vss_all(), IGRAPH_DIRECTED,
                    0.85,NULL,
                    &o);*/

/*     trophic_level(&g,
                    &s, &value,
                    igraph_vss_all(), IGRAPH_DIRECTED,
                    1.,NULL,
                    &o );
    printf("value= %f\n",&value);                                    
                    */
                    
    /*get trophic */                
    trophic_level(&g,   &s);
    printf("trophic_level done\n");
    //igraph_vector_print(&s);                    
    trophic_level_difference(&g, &s, &x);


    moments(no_of_edges, &x, &mean, &std);
    printf("E[xij] =%f   q=STD[xij] =%f \n",mean,std);
    
    //main loop
    IGRAPH_FINALLY(igraph_adjlist_destroy, &al);
    IGRAPH_FINALLY(igraph_adjlist_destroy, &al_cp);

    printf("is directed: %d\n",  igraph_is_directed(&g));
    printf("vcount: %d\n", igraph_vcount(&g)) ;
    printf("ecount: %d\n", igraph_ecount(&g)) ;
    
    
    IGRAPH_CHECK(igraph_adjlist_init(&g_cp, &al_cp, IGRAPH_OUT, IGRAPH_LOOPS_ONCE, IGRAPH_MULTIPLE));
    for (i = 0; i < 10000; i++) {
            // rewire
            num_successful_swaps = igraph_i_rewire_q(&g, &s, T, n_rewire,  IGRAPH_REWIRING_SIMPLE, use_adjlist); 
            cum_num_successful_swaps += num_successful_swaps;
            // get distance wrt initial graph
            if( i%5000==0){
                //populate adjlist
                IGRAPH_CHECK(igraph_adjlist_init(&g, &al, IGRAPH_OUT, IGRAPH_LOOPS_ONCE, IGRAPH_MULTIPLE));        
            
                len = adjlist_dist( &al,&al_cp, IGRAPH_ADJ_DIRECTED);
                igraph_adjlist_destroy(&al);
                printf("len = %d  cum_num_successful_swaps: %d   / %d ",len,cum_num_successful_swaps, n_rewire);

                trophic_level(&g,   &s);
                trophic_level_difference(&g, &s, &x);
                moments(no_of_edges, &x, &mean, &std);
                printf("  E[xij] =%f   q=STD[xij] =%f \n",mean,std);
            }
    }
    //cleanup
    igraph_adjlist_destroy(&al_cp);

    IGRAPH_FINALLY_CLEAN( 2);

    igraph_destroy(&g);
    igraph_destroy(&g_cp);
    
    return 0;
}

int test_igraph_i_rewire_q_loop() {
    igraph_t g;
    //igraph_adjlist_t al,al_cp;
    //igraph_vector_t outdeg, indeg, vec;
    igraph_vector_t out_q, out_swap, out_dist;
    //igraph_bool_t is_simple;
    //igraph_real_t mean, std;
    int len,i;
    //igraph_bool_t use_adjlist=1;
    //igraph_integer_t num_successful_swaps;
    igraph_real_t T = 0.001, value;
    //igraph_vector_t s,x;
    igraph_vector_init_real(&out_q, 0.);
    igraph_vector_init_real(&out_swap, 0.);
    igraph_vector_init_real(&out_dist, 0.);
    //igraph_vector_init_real(&x, 0.);
    
    // Set random seed for reproducibility 
    igraph_rng_seed(igraph_rng_default(), 42);

    // gen graph
    igraph_barabasi_game( &g,
                                          20000,
                                         1.0,
                                          2,
                                         0,
                                          0,
                                          1.0,
                                         IGRAPH_DIRECTED,
                                          IGRAPH_BARABASI_PSUMTREE,
                                         0);
    int n_iter=1000;
    int n_rewire = 10000 ;                                         
    int n_update_s=100;
    int max_iter_gsl = 1000;
    int seed =42;                                    
    igraph_i_rewire_q_loop(&g, n_iter, n_rewire, n_update_s, max_iter_gsl, T, seed,&out_q,&out_swap,&out_dist );
    igraph_vector_print(&out_q);
    igraph_vector_print(&out_swap);
    igraph_vector_print(&out_dist);
}


int main() {
     //test_igraph_vector_difference_sorted_first_nodes();
    test_igraph_i_rewire_q();
    //test_igraph_i_rewire_q_loop();
    
}

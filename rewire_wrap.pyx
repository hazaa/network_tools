"""

!! TENTATIVE WRAPPER FOR FUNCTIONS IN rewire.c using CYTHON
!! FAILS !!!!!!!!!! DO NOT USE

------
REFS:

https://cython.readthedocs.io/en/stable/src/userguide/external_C_code.html
https://barclayii.github.io/building-your-own-graph-library-with-c-as-a-python-exntension-introduction-step-1.html

TODO: Other method: 
"igraph.Graph objects in Python have a hidden method called __graph_as_cobject__"
https://lists.nongnu.org/archive/html/igraph-help/2011-11/msg00067.html

-----
BUILD IGRAPH SHARED LIBS (required for shared libs):

build shared libs for igraph : https://igraph.org/c/html/latest/igraph-Installation.html
BUILD_SHARED_LIBS = ON

-----
BUILD MANUAL
$cython rewire_wrap.pyx

*IGRAPH IS STATIC
$gcc -fpic -c -o rewire_wrap.o `pkg-config --static --cflags python3 ` rewire_wrap.c `pkg-config --static --libs --cflags igraph`
$gcc -fpic -o rewire_wrap`python3-config --extension-suffix` rewire_wrap.o  `pkg-config --libs python3` `pkg-config --static --libs --cflags igraph`

Comment: compilation is ok ; but import in python FAILS

*IGRAPH IS SHARED
$gcc -fpic -c -o rewire_wrap.o `pkg-config  --cflags python3 ` rewire_wrap.c `pkg-config --cflags igraph`
$gcc -fpic -o rewire_wrap`python3-config --extension-suffix` rewire_wrap.o  -ligraph  -L/home/aurelien/local/git/extern/igraph/build/src `python3-config --ldflags` -lpython3.8

--------
BUILD WITH DISTUTIL
python3 setup.py build_ext 
cp build/lib.linux-x86_64-3.8/rewire_wrap.cpython-38-x86_64-linux-gnu.so ./

STATIC: OK
$gcc -pthread -Wl,-O1 -Wl,-Bsymbolic-functions -Wl,-Bsymbolic-functions -Wl,-z,relro -g -fwrapv -O2 -Wl,-Bsymbolic-functions -Wl,-z,relro -g -fwrapv -O2 -g -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 build/temp.linux-x86_64-3.8/rewire_wrap.o build/temp.linux-x86_64-3.8/rewire.o -ligraph -o build/lib.linux-x86_64-3.8/rewire_wrap.cpython-38-x86_64-linux-gnu.so -I/usr/local/include/igraph -L/usr/local/lib -ligraph -lm -lstdc++ -lxml2 -lz -lblas -llapack -larpack -lgomp -lpthread -fPIC `pkg-config --static --libs --cflags igraph`  `python3-config --ldflags` -lpython3.8
BUT import fails

SHARED: OK
see setup.py  :      library_dirs = ['/home/aurelien/local/git/extern/igraph/build/src']

----------
USAGE:

* SHARED
export LD_LIBRARY_PATH=/home/aurelien/local/git/extern/igraph/build/src
python3
>>>import rewire_wrap
>>>g = rewire_wrap.Graph(0,1)
segfault

* STATIC: import fails

"""

cdef extern from "<igraph.h>":
    ctypedef int    igraph_integer_t
    ctypedef double igraph_real_t
    ctypedef int    igraph_bool_t
    
    # cf IGRAPH_HOME/include/igraph_datatype.h
    cdef struct igraph_t:
        pass
    # cf https://igraph.org/c/html/latest/igraph-Basic.html#igraph_empty, https://igraph.org/c/html/latest/igraph-Basic.html#igraph_destroy
    int igraph_empty(igraph_t *graph, igraph_integer_t n, igraph_bool_t directed)
    void igraph_destroy(igraph_t *graph)

    cdef inline int create_ig():
        igraph_t *g
        igraph_empty(g, 10, 1)
        return 0

# adapted from https://barclayii.github.io/building-your-own-graph-library-with-c-as-a-python-exntension-introduction-step-1.html
cdef class Graph(object):
    # (2) You need to declare the attributes you are going to use beforehand.
    # By default, these attributes are "private", meaning that external
    # Python code is not available to see them.  Of course, you can make
    # them public.
    cdef igraph_t *_handle

    # (3) Any initializations involving C code is done in __cinit__() rather
    # than __init__().  This is the best place to, e.g., create a C graph
    # object (using malloc(3)) and store the pointer as a handle.
    def __cinit__(self, n,directed):
        print('1')
        #igraph_empty(self._handle , n, directed)
        igraph_empty(self._handle , 0,1)
        print('2')
        if self._handle is NULL:
            raise MemoryError()
        print('3')    
        #self._handle = _handle

    # (4) Any C-level finalization involving C code is done in __dealloc__(),
    # and there is no __del__() method.  Code destroying C objects (using
    # free(3)) should be here.
    def __dealloc__(self):
        if self._handle is not NULL:
            igraph_destroy(self._handle)
            self._handle = NULL

    #adding/deleting vertices
    #https://igraph.org/c/html/latest/igraph-Basic.html#adding-and-deleting-vertices-and-edges

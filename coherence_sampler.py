"""
CONSTANT TROPHIC-COHERENCE  SAMPLER

AIM: rewire while preserving kin kout and q
    
REFS:
    [Carstens et al.18] https://doi.org/10.1016/j.mex.2018.06.018 
    [Sansom et al. 21] Sansom Johnson MacKay "Trophic Incoherence Drives Systemic Risk in Financial Exposure Networks"  https://www.rebuildingmacroeconomics.ac.uk/publications
    [Klaise et al.16] arxiv:1505.07332
    
UNTESTED IDEAS:
        
    ISO-s MOVES (don't recompute s)
    0) compute s0 
    1) for each node n1, find the (connected) node n_nn with closest value s
    2) rewire
    
    
    MCMC algo
    0) compute s0 (record nb of iter), set s_prev= s0
    1) sample 2 edges; get abcd (as in igraph_rewire)
    2) rewire
    3) compute new solution s 
         with approximate page-rank cf Fortunato  arXiv:cs/0511016; 
         with iterative solver, taking s_prev as initial condition
    4) compute xij, q,...
    5) decide wether to reject or accept 
    6) if reject, rewire back
    
    Q: 
    * py:
       * iterative solver: https://docs.scipy.org/doc/scipy/reference/reference/sparse.linalg.html#module-scipy.sparse.linalg
    * ig: 
       * rewire https://igraph.org/c/html/latest/igraph-Generators.html#igraph_rewire   $HOME/operators/rewire.c 
       * pagerank : https://igraph.org/c/html/latest/igraph-Structural.html#igraph_pagerank_algo_t
       * trophic coherence ???
       * io?
"""    
from random import random,sample,shuffle,randint,randrange
from network_tools.curveballs import edgelist_2_adjlist,  edgelist_from_adjlist, net_from_adjlist, perturbation_score_adj_list

import numpy as np
import pandas as pd
#import networkx as nx

from network_tools.rewire_wrap import *
import igraph as ig
    

import matplotlib.pyplot as plt

import graph_tool.all as gt

import warnings

#from numba import jit

from network_tools.trophic import Trophic
from network_tools.measure import get_cycle_count,get_S,get_nb_component

from copy import deepcopy

from tqdm import tqdm
# ----------------------------------------
# BASAL SAMPLER
"""
TODO

#sample kin/kout for each vertex. NB: basal nodes are sampled from another distrib (since ki=0)
#2 step CM sampling: each non basal node set of in-stubs in divided into 2 subsets: from basal; not from basal
# is_graphical must be checked
"""


#-----------------------------------------
# GT Q BINNING  SAMPLER
def test_rewire_q_bin():
    filename = '../network_datasets/data/ythan_E.csv'
    g = gt.load_graph_from_csv(filename,directed=True,hashed=True,skip_first=True)
    #params
    n_run = 100
    n_iter = 10
    n_bins =  5
    #compute stats
    t0 = Trophic(g)
    t0.run()
    s = t0.trophic_level
    g.vertex_properties["s"] = s
    df0 = t0.get_stats()
    df0 
    edgelist0 = g.get_edges()
    r_mode = 'd'  #directed
    al = edgelist_2_adjlist(edgelist0,mode=r_mode)
    #storage
    score_l=[]
    q_l = []
    s_l=[]
    tau_l=[]
    n_cycle=[]
    lgst_component_fraction_l = []
    nb_component_l = []
    num_successful_swaps = []
    #rewire
    for i in range(n_run):
        rej =rewire_q_bin(g,n_iter=n_iter,bins=n_bins)
        t = Trophic(g)
        t.run()
        #score
        edgelist = g.get_edges()
        al_r = edgelist_2_adjlist(edgelist,mode=r_mode)
        score_l.append(perturbation_score_adj_list(al_r, al) )
        #get measures
        s = t.trophic_level
        q_l.append(t.q)
        s_l.append(s.a)
        tau_l.append(t.tau)
        nb_component_l.append(get_nb_component(g))
        lgst_component_fraction_l.append(get_S(g)) 
        num_successful_swaps.append( g.num_vertices() - rej) 
    fig,ax = plt.subplots(4,2,figsize=(15,10))    
    ax[0,1].plot(q_l)    
    ax[0,1].set_ylabel('q')
    ax[0,0].plot(score_l)    
    ax[0,0].set_ylabel('dist')
    ax[1,0].plot(tau_l)    
    ax[1,0].set_ylabel(r'$\tau$')
    ax[1,1].plot(n_cycle)    
    ax[1,1].set_ylabel('n_cycle')
    ax[2,0].plot(nb_component_l)    
    ax[2,0].set_ylabel('nb_component')
    ax[2,1].plot(lgst_component_fraction_l)    
    ax[2,1].set_ylabel('lgst_component_fraction')
    ax[3,0].plot(np.cumsum(num_successful_swaps))
    ax[3,0].set_ylabel('cum nswaps')


def rewire_q_bin(g,n_iter=1,bins=10, edge_sweep=True):
    """
    """
    s = g.vertex_properties["s"] 
    
    """
    if isinstance(bins,int):
         m = np.min(s.a); M=np.max(s.a)
         bins =  np.linspace(m,M,bins+1)   
    """
    if isinstance(bins,int):
         q =  np.linspace(0,100,bins+1)
         bins  = np.percentile(s.a , q )

    vfilt = g.new_graph_property("bool")
    rej_sum=0 
    for i in range(bins.shape[0]-1):
        vfilt = (s.a > bins[i] ) &(s.a < bins[i+1] )
        u = gt.GraphView(g, vfilt=vfilt)
        rej = gt.random_rewire(u, model='configuration', n_iter=n_iter, edge_sweep=edge_sweep)
        rej_sum+=rej
    return rej_sum
#------------------------------------------
# IGRAPH REWIRING SAMPLER

def test_rewire_q_long_run():
    """
    check effect of rep on distance to initial graphs
    """
   
    #reps_n = 100
    #load net
    filename = '../network_datasets/data/ythan_E.csv'
    g = gt.load_graph_from_csv(filename,directed=True,hashed=True,skip_first=True)
    #compute stats
    t0 = Trophic(g)
    t0.run()
    s = t0.trophic_level
    #prepare for rewire_q
    edgelist0 = g.get_edges()
    g_ig = ig.Graph(directed=True)
    g_ig.add_vertices( g.num_vertices() ) # 
    g_ig.add_edges(edgelist0 ) 
    g_ctypes = convert_to_ctypes_ig(g_ig)
    assert g_ctypes.ecount()==g.num_edges()
    assert g_ctypes.vcount()==g.num_vertices()
    r_mode = 'd'  #directed
    al = edgelist_2_adjlist(edgelist0,mode=r_mode)
    #params
    T = 0.01
    n_rewire=50000
    #storage
    sc_trade=[]
    q_l = []
    s_l=[]
    tau_l=[]
    n_cycle=[]
    lgst_component_fraction_l = []
    nb_component_l = []
    num_successful_swaps = []
    for step in tqdm(range(1,10)):
        nswaps = rewire_q_ctypes_ig(g_ctypes,n_rewire,T,s)
        num_successful_swaps.append(nswaps)
        edgelist= edgelist_from_ctypes_ig(g_ctypes)   
        # perturbation
        al_r = edgelist_2_adjlist(edgelist,mode=r_mode)
        sc_trade.append( perturbation_score_adj_list(al_r, al) )
        #
        #update s
        # make graph
        g_samp = gt.Graph(directed=True)
        g_samp.add_edge_list(edgelist) 
        t = Trophic(g_samp)
        t.run()
        #update 
        s = t.trophic_level
        #get measures
        q_l.append(t.q)
        s_l.append(s.a)
        tau_l.append(t.tau)
        nb_component_l.append(get_nb_component(g_samp))
        lgst_component_fraction_l.append(get_S(g_samp)) 
        #if t.tau<0:
        #    n_cycle.append(get_cycle_count(g_samp))
    #TODO : plot initial values also    
    fig,ax = plt.subplots(7,1)    
    ax[1].plot(q_l)    
    ax[1].set_ylabel('q')
    ax[0].plot(sc_trade)    
    ax[0].set_ylabel('dist')
    ax[2].plot(tau_l)    
    ax[2].set_ylabel(r'$\tau$')
    ax[3].plot(n_cycle)    
    ax[3].set_ylabel('n_cycle')
    ax[4].plot(nb_component_l)    
    ax[4].set_ylabel('nb_component')
    ax[5].plot(lgst_component_fraction_l)    
    ax[5].set_ylabel('lgst_component_fraction')
    ax[6].plot(np.cumsum(num_successful_swaps))
    ax[6].set_ylabel('cum nswaps')
    plt.plot(np.array(s_l))
    # check final value 
    edge_list = edgelist_from_adjlist(al_r,mode=r_mode)    
    # make graph
    g_samp = gt.Graph(directed=True)
    g_samp.add_edge_list(edge_list) 
    #compute stats
    #t = Trophic(g_samp_largest)
    t = Trophic(g_samp)
    t.run()
    t.q

# ---------------------------------------
# CURVEBALL SAMPLER and modifications

def test_curveballs_q_long_run():
    """
    check effect of rep on distance to initial graphs
    """
    reps_n = 100
    #load net
    filename = '../network_datasets/data/ythan_E.csv'
    g = gt.load_graph_from_csv(filename,directed=True,hashed=True,skip_first=True)
    #compute stats
    t0 = Trophic(g)
    t0.run()
    s = t0.trophic_level
    #prepare for curveball
    ee_net = g.get_edges()
    r_mode = 'd'  #directed
    al = edgelist_2_adjlist(ee_net,mode=r_mode)
    net_0 = net_from_adjlist(al,mode=r_mode) # needed for debug only
    #check al has same size as g
    if len(al) != g.num_vertices():
        warnings.warn('al has less vertices than g')
    T = 0.05
    al_r = deepcopy(al)
    sc_trade=[]
    q_l = []
    s_l=[]
    tau_l=[]
    n_cycle=[]
    lgst_component_fraction_l = []
    nb_component_l = []
    for step in tqdm(range(1,10000)):
        rej = curveballs_q(al_r,s.a, T, reps_n = reps_n, mode=r_mode,swaplike='no')
        sc_trade.append( perturbation_score_adj_list(al_r, al) )
        #
        #update s
        edge_list = edgelist_from_adjlist(al_r,mode=r_mode)    
        # make graph
        g_samp = gt.Graph(directed=True)
        g_samp.add_edge_list(edge_list) 
        t = Trophic(g_samp)
        t.run()
        #update 
        s = t.trophic_level
        #get measures
        q_l.append(t.q)
        s_l.append(s.a)
        tau_l.append(t.tau)
        nb_component_l.append(get_nb_component(g_samp))
        lgst_component_fraction_l.append(get_S(g_samp)) 
        if t.tau<0:
            n_cycle.append(get_cycle_count(g_samp))
    #TODO : plot initial values also    
    fig,ax = plt.subplots(6,1)    
    ax[1].plot(q_l)    
    ax[1].set_ylabel('q')
    ax[0].plot(sc_trade)    
    ax[0].set_ylabel('dist')
    ax[2].plot(tau_l)    
    ax[2].set_ylabel(r'$\tau$')
    ax[3].plot(n_cycle)    
    ax[3].set_ylabel('n_cycle')
    ax[4].plot(nb_component_l)    
    ax[4].set_ylabel('nb_component')
    ax[5].plot(lgst_component_fraction_l)    
    ax[5].set_ylabel('lgst_component_fraction')
    plt.plot(np.array(s_l))
    # check final value 
    edge_list = edgelist_from_adjlist(al_r,mode=r_mode)    
    # make graph
    g_samp = gt.Graph(directed=True)
    g_samp.add_edge_list(edge_list) 
    #compute stats
    #t = Trophic(g_samp_largest)
    t = Trophic(g_samp)
    t.run()
    t.q
    """
    Comment:  
    * q fluctuates around an equilibrium value. dist departs from 0 and converges to some value. 
    * s value of nodes is indeed fluctuating; except bottom nodes (= basal nodes+direct neighbors?), don't move. 
    * This shows that, in first approximation, the algorithm does the job.
    * Q not sure if recomputing s is necessary when noise is low and s_i don't change much.
    """
    

def test_curveballs_q():
    """
    check q in increasing with T
    """
    T_l = np.logspace(-3, 1, 10)
    n_samp = 3
    reps_n = 1000
    #load net
    filename = '../network_datasets/data/ythan_E.csv'
    g = gt.load_graph_from_csv(filename,directed=True,hashed=True,skip_first=True)
    #compute stats
    t0 = Trophic(g)
    t0.run()
    s = t0.trophic_level
    #prepare for curveball
    ee_net = g.get_edges()
    r_mode = 'd'  #directed
    al = edgelist_2_adjlist(ee_net,mode=r_mode)
    net_0 = net_from_adjlist(al,mode=r_mode) # needed for debug only
    #check al has same size as g
    if len(al) != g.num_vertices():
        warnings.warn('al has less vertices than g')
    #while T increases
    #q_l = []
    #rejection_rate_l=[]
    #NB_l = []
    df = pd.DataFrame(columns=['idx_samp', 'T', 'q', 'q_basal','alpha','alpha_basal','rejection_rate','NB' ])
    k=0
    for idx_t,T in enumerate(T_l):
        for idx_samp in range(n_samp):     
            #call sampler 
            al_r = deepcopy(al)
            rej = curveballs_q(al_r,s.a, T, reps_n = reps_n, mode=r_mode,swaplike='no')
            #rejection_rate_l.append(rej)
            edge_list = edgelist_from_adjlist(al_r,mode=r_mode)    
            # make graph
            g_samp = gt.Graph(directed=True)
            g_samp.add_edge_list(edge_list)
            #sanity check: in/out degree is preserved
            net_r = net_from_adjlist(al_r,mode=r_mode)
            assert sorted(net_0.degree(mode = 'IN')) == sorted(net_r.degree(mode = 'IN')) and sorted(net_0.degree(mode = 'OUT')) == sorted(net_r.degree(mode = 'OUT'))
            """#the same with gt; but may fail if 'al' has less nodes than g
            if not np.all(g_largest.degree_property_map('in').a == g_samp.degree_property_map('in').a):
            """    
            #compute stats
            #t = Trophic(g_samp_largest)
            t = Trophic(g_samp)
            t.run()
            #
            df.loc[k,'idx_samp']=idx_samp
            df.loc[k,'idx_t']=idx_t
            df.loc[k,'T']=T
            df.loc[k,'q'] = t.q
            df.loc[k,'q_basal'] = t.q_basal
            df.loc[k,'alpha'] = t.alpha
            df.loc[k,'alpha_basal'] = t.alpha_basal
            df.loc[k,'rejection_rate'] = rej
            df.loc[k,'NB'] = t.NB
            k+=1
            del t
            #q_l.append(t.q)
            #NB_l.append(t.NB)
    #--
    # sanity check: compare q of curveball generated samples with q of CM samples
    n_samp = 30
    n_iter= 10
    q_CM = []
    for i in range(n_samp):
        gt.random_rewire(g, "configuration",n_iter=n_iter, edge_sweep=True)
        #g_CM_largest = gt.extract_largest_component(g, directed=False,prune=True)
        #compute stats
        #t = Trophic(g_CM_largest)
        t = Trophic(g)
        t.run()
        q_CM.append(t.q)
    q_CM = np.array(q_CM)
    
    # --
    #compute average over samples
    df=df.astype({'idx_t':int,      'idx_samp':int, 'T':float, 'q':float, 'q_basal':float,'alpha':float,'alpha_basal':float,
                            'rejection_rate':float, 'NB':int})
    df_group_mean= df.groupby(['idx_t']).mean() 
    df_group_std= df.groupby(['idx_t']).std() 
    #plot    
    fig,ax = plt.subplots(2,3)
    ax[0,0].semilogx(T_l, df_group_mean['q'])   
    ax[0,0].semilogx(T_l, df_group_mean['q'] + 3*df_group_std['q'], 'r--' )   
    ax[0,0].semilogx(T_l, df_group_mean['q'] - 3*df_group_std['q'], 'r--' )   
    ax[0,0].set_xlabel("T")
    ax[0,0].set_ylabel("q")
    ax[0,0].set_ylim([0,5])
    ax[0,1].semilogx(T_l, df_group_mean['rejection_rate'])   
    ax[0,1].set_xlabel("T")
    ax[0,1].set_ylabel("rejection rate")
    ax[0,2].plot(T_l, df_group_mean['NB'])
    ax[0,2].set_xlabel('T')
    ax[0,2].set_ylabel(r'$N_B$')
    ax[1,0].plot(T_l, df_group_mean['alpha'])
    ax[1,0].set_xlabel('T')
    ax[1,0].set_ylabel(r'$\alpha$')    
    ax[1,2].hist(q_CM[q_CM<1000] ,30)
    ax[1,2].set_xlabel('q_CM')
    """
    Comment:
    * algorithm is usable for this small net: as T comes close to 0, rejection rate  is acceptable (30%) and
        q is preserved (does not decrease though)
    """ 

def get_curveballs_q_sample(g,s,T,reps_n,swaplike = 'no'):
    """
    get curveball_q rewired graph
    """
    r_mode = 'd'  #directed
    al = edgelist_2_adjlist( g.get_edges() ,mode=r_mode)
    al_r = deepcopy(al)
    #net_0 = net_from_adjlist(al,mode=r_mode) # needed for debug only
    #check al has same size as g
    if len(al) != g.num_vertices():
       warnings.warn('al has less vertices than g')

    rej = curveballs_q(al_r,s, T, reps_n = reps_n, mode=r_mode,swaplike=swaplike)
    edge_list = edgelist_from_adjlist(al_r,mode=r_mode)    
    #compute score
    score  = perturbation_score_adj_list(al_r, al)
    # make graph
    g_samp = gt.Graph(directed=True)
    g_samp.add_edge_list(edge_list) 

    return g_samp,rej,score

def curveballs_q(adjlist,  s, T,reps_n = 1000, mode='b',swaplike='no'):		#alternative modes are 'u': undirected; 'd': directed
    """
    Modification of the original algorithm in [Carstens et al.18] in order 
    to include conservation of trophic coherence q.
    Rejection is inspired by "preferential prey" model in [Sansom et al.21], [Klaise et al.16]
    
    COMMENT: too slow. see rewire.c
    
    ln pij ~ exp [ -1/2 (s(b) -s(a) )**2/ T**2 ]
    because : (xij^a-x_ij^b)_actual -  (xij^a-x_ij^b)_proposed = 2 (s(b) -s(a) 
    
    """
    rejection = 0
    for rep in range(reps_n):
        id_a,id_b=sample(list(range(len(adjlist))),2)
        a, b = adjlist[id_a],adjlist[id_b]
        if mode=='b':
            ua = a - b		# unique elements of a
        else:
            ua = a - (b | {id_b})  # unique elements of a
        if len(ua) != 0:		# first check if trade is possible
            if mode == 'b':
                ub = b - a		#unique elements of b
            else:
                ub = b - (a | {id_a})  # unique elements of b
            if len(ub) != 0:		# second check if trade is possible
                if swaplike != 'no':
                    trade_size = 1
                else:
                    trade_size = randrange(min(len(ua),len(ub))+1)
                if trade_size>0:
                    ra = set(sample(ua,trade_size))
                    rb = set(sample(ub, trade_size))                    
                    # rejection, addition to original algorithm
                    ra_cp =ra.copy()
                    rb_cp =rb.copy()
                    xij_a=s[ra_cp.pop()] - s[id_a]
                    xij_b=s[rb_cp.pop()] - s[id_b]
                    #pij = np.exp( -((xij_a-xij_b)**2)/( 2*T**2))
                    """
                    (xij^a-x_ij^b)_actual -  (xij^a-x_ij^b)_proposed = 2 (s(b) -s(a) 
                    """
                    pij = np.exp( -((s[id_a]-s[id_b])**2)/( 2*T**2))
                    if(  np.random.rand() >pij):
                        rejection +=1
                    else:
                        adjlist[id_a] = (a | rb) - ra
                        adjlist[id_b] = (b | ra) - rb
                        if mode == 'u':
                            ra, rb = list(map(array,list(map(list,[ra,rb]))))
                            adjlist[rb] |= {id_a}
                            adjlist[rb] -= {id_b}
                            adjlist[ra] |= {id_b}
                            adjlist[ra] -= {id_a}
    return rejection/reps_n
# ----------------------------------------
# PPM sampler

def test_ppm_vs_theoretical():
    """
    compare ppm samples to available ensemble values
    (e.g. basal) 
    $\tilde{alpha}, pdf(x_{ij}), \tilde{q}$
    """
    #cycle_count = False #slow
    N=100
    Nb = 10
    L = 500
    n_samp = 10
    T_l = np.logspace(-1,1,10)
    df = pd.DataFrame(columns=['idx_samp', 'T', 'q', 'q_basal','alpha','alpha_basal','tau' ])
    k=0
    for idx_t,T in tqdm(enumerate(T_l)):
        for idx_samp in range(n_samp): 
            adjlist,s = ppm(N,L,Nb, T)
            e = edgelist_from_adjlist(adjlist)
            g =gt.Graph(directed=True)
            g.add_edge_list(e)
            #
            t = Trophic(g)
            t.run()
            #            
            df.loc[k,'idx_samp']=idx_samp
            df.loc[k,'idx_t']=idx_t
            df.loc[k,'T']=T
            df.loc[k,'q'] = t.q
            df.loc[k,'q_basal'] = t.q_basal
            df.loc[k,'alpha'] = t.alpha
            df.loc[k,'alpha_basal'] = t.alpha_basal
            df.loc[k,'tau'] = t.tau
            if t.tau<0:
                df.loc[k,'n_cycle'] =     get_cycle_count(g)
            k+=1
            g.clear()
    #compute averages
    df=df.astype({'idx_t':int,      'idx_samp':int, 'T':float, 'q':float, 'q_basal':float,'alpha':float,'alpha_basal':float,
                            'tau':float })
    df_group_mean= df.groupby(['idx_t']).mean() 
    df_group_std= df.groupby(['idx_t']).std() 
    #        
    plt.semilogx(T_l, df_group_mean['q'], label='mean')
    plt.semilogx(T_l, df_group_mean['q_basal'], label='basal')
    plt.legend()
    plt.xlabel('T')
    plt.ylabel('q')    
    plt.semilogx(T_l, df_group_mean['n_cycle'], label='mean')
    """
    Comment: 
    * basal ensemble does not model ppm samples. (this is expected since the degree sequence is not constrained)
    * 
    """
    

def test_ppm_increase_T():
    """
    """
    N=1000
    Nb = 100
    L = 5000
    T_l = np.logspace(-1,1,10)
    q_l = []
    for T in T_l:
        adjlist,s = ppm(N,L,Nb, T)
        e = edgelist_from_adjlist(adjlist)
        g =gt.Graph(directed=True)
        g.add_edge_list(e)
        t = Trophic(g)
        t.run()
        q_l.append(t.q)
    plt.semilogx(T_l, q_l)
    plt.xlabel('T')
    plt.ylabel('q')
    """
    Comment: OK, behavior in [Klaise et al.16] is recovered.
    """

def test_ppm():
    """
    """
    N=10000
    Nb = 100
    L = 10000
    T=1.
    adjlist,s = ppm(N,L,Nb, T)
    e = edgelist_from_adjlist(adjlist)
    g =gt.Graph(directed=True)
    g.add_edge_list(e)
    vp_s = g.new_vp('float',val = -1000)
    vp_s.a =s 
    #TODO: compare initial s and computed s

"""
#@jit(nopython=True)
def test_list():
    adjlist = [set(0)] 
    return adjlist
"""

#@jit(nopython=True)  #FAILS because list of sets seems not to be supported
def ppm(N,L,Nb, T):
    """
    port of function red_JPPM() in digraphs.cpp available on www.samuel-johnson.org
    """
    #adjlist = [set(0)] 
    adjlist = [set() for i in range(N)]
    links=0; safe=0; # indices
    SAFE=L*L;  # to avoid getting stuck
    l=0;ll=0;ii=0;jj=0;  # indices
    fx=0.;  # kernel function (eg Gaussian)
    x=0. ;   # variable for si-sj-1
    beta=0.;  # factor in fx
    suma=0. ; suma_tot=0.  # for summations
    found=0  # index
    i=0 ; j=0
    
    s=np.zeros(N)
    
    beta=0.5/(T*T)       # using Gaussian for fx kernal (below), where T is standard dev. 
    if (T<0): beta=-beta;     # in case we want to use negative T

    #Introduce initial forest:
    for i in range(0, Nb): s[i]=1; # basal nodes
    for i in range(Nb, N):#  non-basal nodes
        xr=np.random.rand()  #//   xr=1.*rand()/(1.*RAND_MAX);     // xr uniform [0:1]
        j=np.random.randint(0, i)              # j<i random extant node for i to consume
        #adjlist[i] |= {j}   #         A[i][j]=1;     BEWARE: they use the convention A[i][j]=1== i->j
        adjlist[j] |= {i}   #        i->j
        s[i]=s[j]+1.
    
        # Introduce rest of edges:
        links=L-N+Nb  # remaining number of edges to introduce
        l=0#
        safe=0  # safe is to make sure 'while' can't get stuck for eternity
    while ((l<links)  and (safe<SAFE)):
        ii = np.random.randint(Nb,N)  #ii=1.0*rand()/(1.*RAND_MAX)*0.9999*(N-Nb)+Nb;  // random non-basal node candidate for predator
        jj = np.random.randint(N)  #jj=1.0*rand()/(1.*RAND_MAX)*0.9999*N;          // random node as candidate for prey
        
        #if jj not in adjlist[ii] :              #if (A[ii][jj]==0){  // potential pair to be connected.  BEWARE: they use the convention A[i][j]=1== i->j
        if ii not in adjlist[jj] :              #if (A[ii][jj]==0){  // potential pair to be connected
            x=s[ii]-s[jj]-1.
            fx=np.exp(-beta*x*x)
            xr=np.random.rand()    #// xr=1.*rand()/(1.*RAND_MAX);
            if (xr<=fx):
                #adjlist[ii] |= {jj}     #A[ii][jj]=1;  // make new edge . BEWARE: they use the convention A[i][j]=1== i->j
                adjlist[jj] |= {ii}     #A[ii][jj]=1;  // make new edge . BEWARE: they use the convention A[i][j]=1== i->j
                l+=1	
            
        safe+=1
    return adjlist,s    
    




# ----------------------------------------
# Configuration Model and tentative to sample with constant q (fails)


def sample_k(max):
    accept = False
    while not accept:
        k = np.random.randint(1,max+1)
        accept = np.random.random() < 1.0/k
    return k


def test_degree_sequence_constant_q():
    """
    """
    T_l = np.linspace(4, 10, 10)
    #load net
    filename = '../network_datasets/data/ythan_E.csv'
    g = gt.load_graph_from_csv(filename,directed=True,hashed=True,skip_first=True)
    #get weakly connected component
    g_largest = gt.extract_largest_component(g, directed=False,prune=True)
    in_deg_gt = g_largest.degree_property_map('in')
    out_deg_gt = g_largest.degree_property_map('out')
    #compute stats
    t0 = Trophic(g_largest)
    t0.run()
    s = t0.trophic_level
    #while T increases
    q_l = []
    for T in T_l:
        #call sampler 
        edges = degree_sequence_constant_q(in_deg_gt.a,out_deg_gt.a,s.a, T)
        # make graph
        g_samp = gt.Graph(directed=True)
        g_samp.add_edge_list(edges)
        #
        g_samp_largest = gt.extract_largest_component(g_samp, directed=False,prune=True)
        #compute stats
        t = Trophic(g_samp_largest)
        t.run()
        q_l.append(t.q)
    #plot    
    plt.plot(T_l, q_l)   
    """
    Comment:
    * algorithm is not usable: as T comes close to 0, rejection rate rises and computational
        time is prohibitive.
    """ 


def test_compute_edge_sampling_probability():
    """
    """
    T_l = np.linspace(1, 10, 3)
    #load net
    filename = '../network_datasets/data/ythan_E.csv'
    g = gt.load_graph_from_csv(filename,directed=True,hashed=True,skip_first=True)
    g_largest = gt.extract_largest_component(g, directed=False,prune=True)
    #compute stats
    t0 = Trophic(g_largest)
    t0.run()
    s = t0.trophic_level
    compute_edge_sampling_probability( g_largest.get_edges()  ,  s.a,T_l)

def compute_edge_sampling_probability(edge_list,s,T_list,plot=True,bins=30):
    """
    diagnosis tool to compute the histogram of edge_sampling_probability in 
    degree_sequence_constant_q sampler
    
    """
    n_T = len(T_list)
    if plot:   fig,ax = plt.subplots(n_T,1)
    for i,T in enumerate(T_list):
        pij_l = []
        for (src,tgt) in edge_list:
                xij=s[tgt] - s[src]
                pij = np.exp( -((xij-1.)**2)/( 2*T**2))
                pij_l.append(pij)
        #plot
        if plot:    ax[i].hist(pij_l, bins)        
                

def test_degree_sequence_game_foodweb():
    """
    """
    #load net
    filename = '../network_datasets/data/ythan_E.csv'
    g = gt.load_graph_from_csv(filename,directed=True,hashed=True,skip_first=True)
    in_deg = g.degree_property_map('in')
    out_deg = g.degree_property_map('out')
    
    edges = degree_sequence_game_no_multiple_directed_uniform(in_deg.a,out_deg.a)
    g_samp = gt.Graph(directed=True)
    g_samp.add_edge_list(edges)
    
    in_deg_samp = g_samp.degree_property_map('in')
    out_deg_samp = g_samp.degree_property_map('out')
    
    assert np.all(in_deg.a==in_deg_samp.a)
    assert np.all(out_deg.a==out_deg_samp.a)


def test_degree_sequence_game_random():
    """
    """
    N = 20
    in_deg = np.array([sample_k(5) for i in range(N)] , dtype=int)
    out_deg = np.array([sample_k(5) for i in range(N)], dtype=int)
    
    edges = degree_sequence_game_no_multiple_directed_uniform(in_deg,out_deg)
    g = gt.Graph(directed=True)
    g.add_edge_list(edges)
    
    in_deg_gt = g.degree_property_map('in')
    out_deg_gt = g.degree_property_map('out')
    
    assert np.all(in_deg==in_deg_gt.a)
    assert np.all(out_deg==out_deg_gt.a)




#@jit(nopython=True)
def degree_sequence_constant_q(in_deg,out_deg,s, T):
    """
    modification of igraph degree sequence sampler igraph_i_degree_sequence_game_no_multiple_directed_uniform 
    (in file HOME_IGRAPH/src/games/degree_sequence.c)
    
    NB: multiple links check has been disabled
    
    """
    #cf      igraph/src/games/degree_sequence.c: igraph_i_degree_sequence_game_no_multiple_directed_uniform 
    #check is graphical: 
    
    ecount = out_deg.sum()
    vcount = out_deg.shape[0]
    
    if ecount != in_deg.sum(): raise ValueError('kin, kout not graphical: sum mismatch')
    
    # Fill in- and out-stubs vectors. 
    out_stubs =  np.zeros(ecount, np.int_)
    in_stubs =np.zeros( ecount, np.int_)
    edges = np.zeros( 2 * ecount, np.int_)
    k = 0
    l = 0
    for i in range(0,  vcount):
        dout = out_deg[i]
        for j in range(0, dout):
            out_stubs[k] = i
            k +=1            
        din  = in_deg[i]
        for j in range(0,din):
            in_stubs[l] = i
            l +=1
            
    # DISCARDED: /* Build an adjacency list in terms of sets; used to check for multi-edges. */        
    xij = 0.
    pij=0.
                
    while True:
        success = 1
        # Shuffle out-stubs vector with Fisher-Yates and check for self-loops and multi-edges as we go. 
        for i in range(0,ecount):
            #k = RNG_INTEGER(i, ecount-1)
            k=np.random.randint(i,ecount)
            #swap
            temp=out_stubs[i]
            out_stubs[i]=out_stubs[k]
            out_stubs[k]=temp

            from_ = out_stubs[i]
            to   = in_stubs[i]

            # self-loop, fail 
            if (to == from_): 
                success = 0
                break
    
            """#DISCARDED /* multi-edge, fail */
            set = (igraph_set_t *) VECTOR(adjlist)[from];
            if (igraph_set_contains(set, to)) {
                success = 0;
                break;
            }"""

            #/* sets are already reserved */
            #igraph_set_add(set, to);

            # rejection
            xij=s[to] - s[from_]
            pij = np.exp( -((xij-1.)**2)/( 2*T**2))
            if(  np.random.rand() >pij):
                success = 0
                break

            # register edge 
            edges[2 * i]   = from_
            edges[2 * i + 1] = to
        
        if (success):
            break

    return edges.reshape((ecount,2))    


#@jit(nopython=True)
def degree_sequence_game_no_multiple_directed_uniform(in_deg,out_deg):
    """
    modification of igraph degree sequence sampler igraph_i_degree_sequence_game_no_multiple_directed_uniform 
    (in file HOME_IGRAPH/src/games/degree_sequence.c)
    
    NB: multiple links check has been disabled
    
    """
    #cf      igraph/src/games/degree_sequence.c: igraph_i_degree_sequence_game_no_multiple_directed_uniform 
    #check is graphical: 
    
    ecount = out_deg.sum()
    vcount = out_deg.shape[0]
    
    if ecount != in_deg.sum(): raise ValueError('kin, kout not graphical: sum mismatch')
    
    # Fill in- and out-stubs vectors. 
    out_stubs =  np.zeros(ecount, np.int_)
    in_stubs =np.zeros( ecount, np.int_)
    edges = np.zeros( 2 * ecount, np.int_)
    k = 0
    l = 0
    for i in range(0,  vcount):
        dout = out_deg[i]
        for j in range(0, dout):
            out_stubs[k] = i
            k +=1            
        din  = in_deg[i]
        for j in range(0,din):
            in_stubs[l] = i
            l +=1
            
    # DISCARDED: /* Build an adjacency list in terms of sets; used to check for multi-edges. */        
                
    while True:
        success = 1
        # Shuffle out-stubs vector with Fisher-Yates and check for self-loops and multi-edges as we go. 
        for i in range(0,ecount):
            #k = RNG_INTEGER(i, ecount-1)
            k=np.random.randint(i,ecount)
            #swap
            temp=out_stubs[i]
            out_stubs[i]=out_stubs[k]
            out_stubs[k]=temp

            from_ = out_stubs[i]
            to   = in_stubs[i]

            # self-loop, fail 
            if (to == from_): 
                success = 0
                break
    
            """#DISCARDED /* multi-edge, fail */
            set = (igraph_set_t *) VECTOR(adjlist)[from];
            if (igraph_set_contains(set, to)) {
                success = 0;
                break;
            }"""

            #/* sets are already reserved */
            #igraph_set_add(set, to);

            # register edge 
            edges[2 * i]   = from_
            edges[2 * i + 1] = to
        
        if (success):
            break

    return edges.reshape((ecount,2))    



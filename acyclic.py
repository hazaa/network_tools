"""


REFS:
[Carstens et al. 2014] "A uniform random graph model for directed acyclic networks and its effect on motif-finding"   doi:10.1093/comnet/cnu028
                    https://github.com/queenBNE/DirectedAcyclicNetworks
[Goni et al. 2010]

"""
from numba import jit
import numpy as np
import pandas as pd

"""
# This function returns the edgelist of a random directed acyclic network generated 
# using the first random graph model introduced in "Random graph models for directed 
# acyclic networks, Phys. Rev. E 80, Brian Karrer and M. E. J. Newman (2009)"

# The function takes as inputs the in-degree and out-degree sequence of a directed 
# acyclic network G=(V,E). 

# These sequences need to correspond to a topological ordering (1, ..., n) of V
# such that for every edge (i, j), i > j. 
# (i.e. for such an ordering d_in[i] = d_in(v_i) and d_out[i] = d_out(i))

# For example, the directed acyclic networks below are identical, but the vertices 
# of the first one are not labelled with a topological ordering

# V = {1,2,3}
# E = {(3,2), (3,1), (1,2)}

# V = {1,2,3}
# E = {(3,2), (2,1), (3,1)}

# For the second network the input to the Karrer Newman algorithm should be 
# d_in <- c(2,1,0)
# d_out <- c(0,1,2)

# A more interesting example is: 
# d_in <-  c(1,1,2,2,0,0)
# d_out <- c(0,0,0,1,3,2)


karrerNewmanEdgelist <- function(d_in, d_out){
   
  # Empty matrix to save edges and index
  edgelist <- matrix(nrow=sum(d_in), ncol=2)
  j <- 1
  
  # The number of 'in stubs' at each vertex (initially 0)
  in_stub_count <- rep(0, length(d_out))
  
  # For each vertex - starting with the 'oldest' vertex
  for(v in 1:length(d_in)){
    
    # If it should have outgoing edges
    if(d_out[v] > 0){
      # Create a list of vertices with in-stubs
      stubs <- c(rep(1:length(d_out), in_stub_count))
      # And pick the in-stubs to connect to
      if(length(stubs) == 1){
        out_neighbours <- stubs
      }else{
        out_neighbours <- sample(stubs, d_out[v], replace=F)
      }
      
      # Add the chosen edges to the edgelist
      for(t in out_neighbours){
        edgelist[j,] <- c(v, t)
        j <- j + 1
      }
      
      # Remove the chosen in-stubs from the in-stub counts
      nbrs <- table(out_neighbours)
      rownames <- row.names(nbrs)
      in_stub_count[as.numeric(rownames)] <- in_stub_count[as.numeric(rownames)] - nbrs[rownames]
    }
    # Add in-stubs for the current vertex
    in_stub_count[v] <- d_in[v]
  }
  # Create the
  return(edgelist)
}
""" 

def test_orderedSwitching():
    """
    """
    el = np.array( [[5,3], [5,2], [4,3], [4,2],[4,0],[3,1] ])
    #el = np.array( [[2,1], [1,0], [2,0]])
    el_rand,count = orderedSwitching(el, swaps = 100)
    print(count)

def test_orderedSwitching():
    """
    reproduce some figures in Goni
    """
    import graph_tool.all as gt
    import matplotlib.pyplot as plt
    
    g = gt.collection.data["pgp-strong-2009"] # |V|~30000 too large for 
    g = gt.collection.data["celegansneural"] #|V|=297
    g = gt.collection.data["polblogs"] #|V|=1490
    nedge = g.num_edges()#g.get_edges().shape[0]
    nvertex = g.num_vertices()#g.get_vertices().shape[0]
    n=5 #nb of 
    log_nswap_max = 3
    swaps_range = np.logspace(1,log_nswap_max,n)
    similarity=[]
    niter = 10 # nb of iterations for each swap number
    result = np.zeros((niter,n))
    count = np.zeros((niter,n))
    # do topological sorting
    if not gt.is_DAG(g):
        print("g not DAG")
        raise ValueError
        #tree = gt.min_spanning_tree(g)
        #g.set_edge_filter(tree) 
    sort = gt.topological_sort(g)
    LUT = pd.Series( range(nvertex), index = sort[::-1] ) #???
    # relabel nodes such that 
    # NB: "labelled such that for every edge (x,y) we have x > y. " (cf DirectedAcyclicNetworks/orderedSwitching.R)
    edge_list = g.get_edges()
    edge_list[:,0] = LUT[edge_list[:,0]]
    edge_list[:,1] = LUT[edge_list[:,1]]
    g_relabeled=gt.Graph()
    g_relabeled.add_edge_list(edge_list)
    assert(np.all(edge_list[:,0] >edge_list[:,1]))
    # repeat
    for j,s in enumerate(swaps_range):
        for i in range(niter) : 
            #randomize
            el_rand,count_ = orderedSwitching(edge_list, swaps = s)
            #create gt
            g_rand = gt.Graph(directed=True)
            g_rand.add_edge_list(el_rand.tolist())
            #indicators
            simi= gt.similarity(g_relabeled,g_rand,norm=True )
            #record
            result[i,j] = simi
            count[i,j]= count_
    #plot log(effective rewiring/|E|)
    # NB: Goni et al plot the dissimilarity
    logcount =  np.mean( np.log(count/nedge),axis=0)
    mu = np.mean(result,axis=0) 
    std = np.std(result,axis=0) 
    #plot
    #https://matplotlib.org/stable/plot_types/stats/errorbar_plot.html
    fig, ax = plt.subplots()
    ax.errorbar(logcount, mu, std, fmt='o', linewidth=2, capsize=6)
    ax.set_xlabel('log(nswap/nedge)')
    ax.set_ylabel('similarity')
    """
    Commentaire:
    * behavior from Goni is reproduced.
    * NB: Goni et al plot the dissimilarity
    * required nb of iter is rising quickly as nedge rises. (compare celegans, polblogs)
    * computation time is ok for |V|
    * (Q/ what is max dissimilarity for a given net ?)
    """

@jit(nopython=True)
def orderedSwitching(el, swaps = 100):
    """
    port to python of https://github.com/queenBNE/DirectedAcyclicNetworks
    
    TODO: profiling
    
    # Implementation of the ordered switching method. 
    # Takes as an input the edgelist of a directed acyclic network where vertices are 
    # labelled such that for every edge (x,y) we have x > y. 
    # Pairs of edges (x,y) and (u,v) are selected at random. 
    # If these satisfy the following conditions: 
    # 1. x != u and y != v    since a switch should change the network
    # 2. x > v and u > y      since a switch should respect the topological ordering
    # 3. (x,v) and (y,u) are not in the edgelist
    #                         since a switch should not introduce multiple edges
    # then the edges will swap heads
    
    el <- cbind(c(6,6,5,5,5,4), c(4,3,4,3,1,2))  
    
    orderedSwitching <- function(el, swaps = 100){
      if(!is.matrix(el))
        return("Please provide a matrix")
      m <- dim(el)[1]
      count <- 0
      for(i in 1:swaps){
        toswap <- sample(1:m, 2, replace=F)
        
        e1 <- el[toswap[1],]
        x <- e1[1]
        y <- e1[2]
        
        e2 <- el[toswap[2],]
        u <- e2[1]
        v <- e2[2] 
        
        if(x != u)
          if(y != v)
            if(x > v)
              if(u > y)
                if(sum(el[,1]==x & el[,2]==v) == 0)
                  if(sum(el[,1]==u & el[,2]==y) == 0){
                    el[toswap[1], 2] <- v
                    el[toswap[2], 2] <- y
                    count <- count + 1 
                  }
      }
        
      print(paste("Requested: ", swaps, " swaps. Made: ", count, " swaps", sep=""))
      return(el)
    }
    """
    m = el.shape[0]
    count = 0
    for i in range(swaps):
        toswap = np.random.choice( m, size=2, replace=False)
    
        e1  = el[toswap[0],:]
        x = e1[0]
        y = e1[1]
    
        e2 = el[toswap[1],:]
        u = e2[0]
        v = e2[1] 
        
        if(x != u):
          if(y != v):
            if(x > v):
              if(u > y):
                if(np.sum( (el[:,0]==x) & (el[:,1]==v)) == 0):
                  if(np.sum((el[:,0]==u) & (el[:,1]==y)) == 0):
                    el[toswap[0], 1] = v
                    el[toswap[1], 1] = y
                    count = count + 1 
    return el,count    

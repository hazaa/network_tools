/* -*- mode: C -*-  */
/* vim:set ts=4 sw=4 sts=4 et: */
/*
   IGraph library.
   Copyright (C) 2005-2021 The igraph development team

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
   02110-1301 USA

*/

/*
 *  REF
 * 
 * sparse solver: https://www.gnu.org/software/gsl/doc/html/splinalg.html
 * 
 * */

#include <igraph.h>
#include <math.h>

#include <gsl/gsl_math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_spmatrix.h>
#include <gsl/gsl_splinalg.h>

/* Threshold that defines when to switch over to using adjacency lists during
 * rewiring */
#define REWIRE_ADJLIST_THRESHOLD 10

/*
 *  utility function: read raw edge vector element by element
 * 
 *
igraph_integer_t read_edge_vector(igraph_t *graph , igraph_integer_t i){
    igraph_vector_t alledges;
    long int no_of_edges = igraph_ecount(graph); 
    
    IGRAPH_VECTOR_INIT_FINALLY(&alledges, no_of_edges * 2);
    igraph_get_edgelist(graph, &alledges,  0);
    igraph_integer_t  a = VECTOR(alledges)[ i ];
    //b = VECTOR(alledges)[(((igraph_integer_t)VECTOR(eids)[0]) * 2) + 1];
    igraph_vector_destroy(&alledges);
    IGRAPH_FINALLY_CLEAN(1);
    return a;
}
 */

/*OK*/
igraph_real_t read_vector_real(igraph_vector_t *vec, igraph_integer_t i){

    return VECTOR(*vec)[ i ];

}
/*OK*/
igraph_integer_t read_vector(igraph_vector_t *vec, igraph_integer_t i){

    return VECTOR(*vec)[ i ];

}
/*FAILS
igraph_real_t read_vector_real(igraph_vector_float_t *vec, igraph_integer_t i, igraph_real_t *out){

    *out= VECTOR(*vec)[ i ];

}*/

int copy_edge(igraph_t *graph, igraph_vector_t *alledges){
    
    //igraph_vector_t alledges;
    long int no_of_edges = igraph_ecount(graph); 
    
    IGRAPH_VECTOR_INIT_FINALLY(alledges, no_of_edges * 2);
    igraph_get_edgelist(graph, alledges, /*bycol=*/ 0);

}

/*
int copy_edge3(igraph_t *graph, igraph_vector_t *vector){
    
    igraph_vector_t alledges;
    long int no_of_edges = igraph_ecount(graph); 
    
    IGRAPH_VECTOR_INIT_FINALLY(&alledges, no_of_edges * 2);
    igraph_get_edgelist(graph, &alledges,  0);
    for( int i=0;i<2*no_of_edges;i++){ 
        //VECTOR(*vector)[ i ]= VECTOR(alledges)[ i ];
       // igraph_vector_push_back(vector,   VECTOR(alledges)[ i ] );
       igraph_vector_push_back(vector,  i);
    }
    //b = VECTOR(alledges)[(((igraph_integer_t)VECTOR(eids)[0]) * 2) + 1];
    igraph_vector_destroy(&alledges);
    IGRAPH_FINALLY_CLEAN(1);
    
}
* */

/* utility function to allow copying edges to python wrapper
 * 
 *  FAILS !!!!!! DO NOT USE
 * */
/*
int copy_edge2(igraph_t *graph, igraph_vector_t *from, igraph_vector_t *to){
    igraph_integer_t i,j, k=0;
    long int no_of_edges = igraph_ecount(graph); 
    IGRAPH_CHECK(igraph_vector_resize(from, no_of_edges));
    IGRAPH_CHECK(igraph_vector_resize(to, no_of_edges));    
    
    igraph_eit_t eit; //edge iterator
    igraph_eit_create(graph, igraph_ess_all(IGRAPH_EDGEORDER_FROM), &eit);
    // see igraph/examples/simple/igraph_similarity.c ; https://igraph.org/c/doc/igraph-Iterators.html#edge-iterators
    while (!IGRAPH_EIT_END(eit)) {
        long int eid = IGRAPH_EIT_GET(eit);
        i = IGRAPH_FROM(graph, eid);
        j = IGRAPH_TO(graph, eid);

        VECTOR(*from)[k] =i ; 
        VECTOR(*to)[k] =j; 
        printf("from %d  to %d\n", VECTOR(*from)[k], VECTOR(*to)[k]);
        IGRAPH_EIT_NEXT(eit);
        k++;
    }
    igraph_eit_destroy(&eit);
    
    return 0;
}
*/
/* utility function to allow copying edges to python wrapper
 * 
 *  FAILS !!!!!!   DO NOT USE
 * */
/*
int copy_edge_FAIL(igraph_t *graph, igraph_vector_t *from, igraph_vector_t *to){
    long int i,j;
    igraph_integer_t  k=0;
    long int no_of_edges = igraph_ecount(graph); 
    //IGRAPH_CHECK(igraph_vector_resize(from, no_of_edges));
    //IGRAPH_CHECK(igraph_vector_resize(to, no_of_edges));    
    
    igraph_eit_t eit; //edge iterator
    igraph_eit_create(graph, igraph_ess_all(IGRAPH_EDGEORDER_FROM), &eit);
    // see igraph/examples/simple/igraph_similarity.c ; https://igraph.org/c/doc/igraph-Iterators.html#edge-iterators
    while (!IGRAPH_EIT_END(eit)) {
        long int eid = IGRAPH_EIT_GET(eit);
        i =(long int) IGRAPH_FROM(graph, eid);
        j =(long int) IGRAPH_TO(graph, eid);

        //VECTOR(*from)[k] =i ; 
        //VECTOR(*to)[k] =j; 
        igraph_vector_push_back(from,i);
        igraph_vector_push_back(to,j);
        
        printf("from %d  to %d\n", i, j);
        IGRAPH_EIT_NEXT(eit);
        //k++;
    }
    igraph_eit_destroy(&eit);
    
    return 0;
}*/

/* AIM: test  igraph_vector_difference_sorted
 * 
 * COMMENT: implementation of  igraph_vector_difference_sorted returns STRANGE result!!!!!!!!!!!
 *                   implementation of igraph_vector_contains  returns strange results ???
*                  there is a unit test, cf tests/unit/vector2.c
 * 
 * TODO: compare to computeCommonDisjointNeighbour, https://github.com/networkit/networkit/blob/master/networkit/cpp/randomization/GlobalCurveballImpl.hpp
 * */
int igraph_vector_difference_sorted_first_nodes(igraph_t *graph){

    igraph_adjlist_t al;
    igraph_integer_t id_a, id_b;
    igraph_vector_t ua,ub;
    igraph_vector_t *neis_a, *neis_b;
    igraph_vector_init(&ua, 0);
    igraph_vector_init(&ub, 0);
    
    
    IGRAPH_CHECK(igraph_adjlist_init(graph, &al, IGRAPH_OUT, IGRAPH_NO_LOOPS, IGRAPH_NO_MULTIPLE));
    IGRAPH_FINALLY(igraph_adjlist_destroy, &al);
    
    id_a= 0;
    id_b= 1;
    //ua = a - (b | {id_b})  # unique elements of a
    //"The elements that are contained in only the first vector but not the second are stored in the result vector." https://igraph.org/c/html/latest/igraph-Data-structures.html#igraph_vector_difference_sorted
    neis_a =  igraph_adjlist_get(&al,  id_a)  ; 
    neis_b =  igraph_adjlist_get(&al,  id_b)  ; 
    
    printf("=====\nBefore sorting: neis_a,neis_b\n");
    igraph_vector_int_print(neis_a); 
    igraph_vector_int_print(neis_b); 
    printf("vector size neis_a: %d \n",  igraph_vector_size(neis_a) )    ;  
    printf("vector size neis_b: %d \n",  igraph_vector_size(neis_b) )   ;
    printf("=====\nAfter Sorting: neis_a,neis_b\n");
    igraph_vector_sort(neis_a);
    igraph_vector_sort(neis_b);
    
    igraph_integer_t len_neis_a =  igraph_vector_size(neis_a)  ;
    igraph_integer_t len_neis_b =  igraph_vector_size(neis_b)  ;
    
    printf("vector size neis_a: %d \n", len_neis_a)   ;   
    printf("vector size neis_b: %d \n", len_neis_b)   ;
    igraph_vector_int_print(neis_a); 
    igraph_vector_int_print(neis_b); 
    printf("=====\nDifference...\n");
    //
    igraph_vector_difference_sorted( neis_a, neis_b , &ua);
    igraph_vector_difference_sorted( neis_b, neis_a, &ub);
    int i;
    /* TRYING TO DO IT MANUALLY
    for (i = 0; i < len_neis_a ; i++) {
        //igraph_integer_t nei = VECTOR(*neis_a)[i];  
        long int nei = (long int)VECTOR(*neis_a)[i];  
          if (! igraph_vector_contains(neis_b, nei ) )                 
            { 
                printf("%d is not in neis_b, pushing to ua\n",(long int)nei);
               igraph_vector_push_back(&ua,  (long int)nei) ;
            }
        }
  
    for (i = 0; i <  igraph_vector_int_size(neis_b)  ; i++) {
         long int nei = (long int)VECTOR(*neis_b)[i];  
          if (! igraph_vector_contains(neis_a,  nei  ) )                 
            { 
                    printf("%d is not in neis_a, pushing to ub\n",(long int)nei );
               igraph_vector_push_back(&ub,  (long int)nei) ; 
            }
        }
      */
    if (igraph_vector_contains(&ua, id_b) )
        {  IGRAPH_ERROR("ua contains id_b", 1);}
    if (igraph_vector_contains(&ub, id_a) ){
          IGRAPH_ERROR("ub contains id_a", 1);}    
    
    printf("=====\nAfter Difference...\n");
    printf("vector size ua: %d \n",  igraph_vector_int_size(&ua) )      ;
    printf("vector size ub: %d \n",  igraph_vector_int_size(&ub) )      ;
    printf("ua,ub:\n");

    igraph_vector_int_print(&ua); 
    igraph_vector_int_print(&ub); 
    
    
    igraph_adjlist_destroy(&al);
    IGRAPH_FINALLY_CLEAN(1);
    
    igraph_vector_destroy(&ua);
    igraph_vector_destroy(&ub);
    
    }

/*
 * set operations on sorted vectors:
 *  https://igraph.org/c/html/latest/igraph-Data-structures.html#set-operations-on-sorted-vectors
    //https://igraph.org/c/html/latest/igraph-Data-structures.html#igraph_adjlist_get
 https://igraph.org/c/html/latest/igraph-Data-structures.html#igraph_vector_sort
 * 
 *  COMMENT:  unfinished ; unable to implement set operations with igraph/c.
 *  
 * 
 * 
 def curveballs(adjlist, reps_n = 1000, mode='b',swaplike='no'):		#alternative modes are 'u': undirected; 'd': directed
	for rep in range(reps_n):
		id_a,id_b=sample(list(range(len(adjlist))),2)
		a, b = adjlist[id_a],adjlist[id_b]
		if mode=='b':
			ua = a - b		# unique elements of a
		else:
			ua = a - (b | {id_b})  # unique elements of a
		if len(ua) != 0:		# first check if trade is possible
			if mode == 'b':
				ub = b - a		#unique elements of b
			else:
				ub = b - (a | {id_a})  # unique elements of b
			if len(ub) != 0:		# second check if trade is possible
				if swaplike != 'no':
					trade_size = 1
				else:
					trade_size = randrange(min(len(ua),len(ub))+1)
				if trade_size>0:
					ra = set(sample(ua,trade_size))
					rb = set(sample(ub, trade_size))
					adjlist[id_a] = (a | rb) - ra
					adjlist[id_b] = (b | ra) - rb
					if mode == 'u':
						ra, rb = list(map(array,list(map(list,[ra,rb]))))
						adjlist[rb] |= {id_a}
						adjlist[rb] -= {id_b}
						adjlist[ra] |= {id_b}
						adjlist[ra] -= {id_a}
	return adjlist


 * */
int igraph_curveball(igraph_t *graph, igraph_integer_t n_iter){
    long int no_of_nodes = igraph_vcount(graph);
    long int no_of_edges = igraph_ecount(graph);    
    igraph_adjlist_t al;
    igraph_integer_t i, id_a, id_b,num_swaps, num_successful_swaps;
    igraph_vector_t ua,ub;
    igraph_vector_int_t *neis_a, *neis_b;
    igraph_vector_init_int(&ua, 0);
    igraph_vector_init_int(&ub, 0);
    
    if (no_of_nodes < 4) {
        IGRAPH_ERROR("graph unsuitable for rewiring", IGRAPH_EINVAL);
    }
    

    /* As well as the sorted adjacency list, we maintain an unordered
     * list of edges for picking a random edge in constant time.
     */
    IGRAPH_CHECK(igraph_adjlist_init(graph, &al, IGRAPH_OUT, IGRAPH_LOOPS_ONCE, IGRAPH_MULTIPLE));
    IGRAPH_FINALLY(igraph_adjlist_destroy, &al);

    
    RNG_BEGIN();
    
    num_swaps = num_successful_swaps = 0;
    while (num_swaps < n_iter) {
    
    //id_a,id_b=sample(list(range(len(adjlist))),2)
    id_a= RNG_INTEGER(0, no_of_nodes - 1);
    id_b= RNG_INTEGER(0, no_of_nodes - 1);
    //ua = a - (b | {id_b})  # unique elements of a
    //"The elements that are contained in only the first vector but not the second are stored in the result vector." https://igraph.org/c/html/latest/igraph-Data-structures.html#igraph_vector_difference_sorted
    neis_a =  igraph_adjlist_get(&al,id_a)  ; 
    neis_b =  igraph_adjlist_get(&al,id_b)  ; 

    //sort neis_a , neis_b     
    igraph_vector_sort(neis_a);
    igraph_vector_sort(neis_b);
    // difference
    //igraph_vector_difference_sorted( neis_a, neis_b , &ua);
    for (i = 0; i <  igraph_vector_int_size(neis_a)  ; i++) {
          if (! igraph_vector_contains(neis_b,  VECTOR(*neis_a)[i]  ) )                 
            { 
                igraph_vector_push_back(&ua,  VECTOR(*neis_a)[i]) ; 
            }
        }
    if (igraph_vector_contains(&ua, id_b) )
        {  IGRAPH_ERROR("ua contains id_b", 1);}
    if( igraph_vector_size(&ua)>0){
        
            //igraph_vector_difference_sorted( neis_b, neis_a, &ub);
           for (i = 0; i <  igraph_vector_int_size(neis_b)  ; i++) {
              if (! igraph_vector_contains(neis_a,  VECTOR(*neis_b)[i]  ) )                 
                { 
                    igraph_vector_push_back(&ub,  VECTOR(*neis_b)[i]) ; 
                }
            }
            if (igraph_vector_contains(&ub, id_a) )
                {  IGRAPH_ERROR("ub contains id_a", 1);}
            if( igraph_vector_size(&ub)>0){  
                /*
                 * MUST CONTINUE HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                 * */
                
            }      
     }   
    num_swaps++;
    }
    
    RNG_END();
    
    igraph_adjlist_destroy(&al);
    IGRAPH_FINALLY_CLEAN(1);
    
    igraph_vector_destroy(&ua);
    igraph_vector_destroy(&ub);
    
    return 0;}

/*    copied from IGRAPH_HOME/src/operators/rewire.c  
 *    https://igraph.org/c/html/latest/igraph-Generators.html#igraph_rewire
 * 
 *   COMMENT: this function is just a copy of the original function, to test
 *                     compilation outside of igraph file structure.
 * 
 *     */
int igraph_i_rewire(igraph_t *graph, igraph_integer_t n, igraph_rewiring_t mode, igraph_bool_t use_adjlist) {
    long int no_of_nodes = igraph_vcount(graph);
    long int no_of_edges = igraph_ecount(graph);
    char message[256];
    igraph_integer_t a, b, c, d, dummy, num_swaps, num_successful_swaps;
    igraph_vector_t eids, edgevec, alledges;
    igraph_bool_t directed, loops, ok;
    igraph_es_t es;
    igraph_adjlist_t al;

    if (no_of_nodes < 4) {
        IGRAPH_ERROR("graph unsuitable for rewiring", IGRAPH_EINVAL);
    }

    directed = igraph_is_directed(graph);
    loops = (mode & IGRAPH_REWIRING_SIMPLE_LOOPS);

    RNG_BEGIN();

    IGRAPH_VECTOR_INIT_FINALLY(&eids, 2);

    if (use_adjlist) {
        /* As well as the sorted adjacency list, we maintain an unordered
         * list of edges for picking a random edge in constant time.
         */
        IGRAPH_CHECK(igraph_adjlist_init(graph, &al, IGRAPH_OUT, IGRAPH_LOOPS_ONCE, IGRAPH_MULTIPLE));
        IGRAPH_FINALLY(igraph_adjlist_destroy, &al);
        IGRAPH_VECTOR_INIT_FINALLY(&alledges, no_of_edges * 2);
        igraph_get_edgelist(graph, &alledges, /*bycol=*/ 0);
    } else {
        IGRAPH_VECTOR_INIT_FINALLY(&edgevec, 4);
        es = igraph_ess_vector(&eids);
    }

    /* We don't want the algorithm to get stuck in an infinite loop when
     * it can't choose two edges satisfying the conditions. Instead of
     * this, we choose two arbitrary edges and if they have endpoints
     * in common, we just decrease the number of trials left and continue
     * (so unsuccessful rewirings still count as a trial)
     */

    num_swaps = num_successful_swaps = 0;
    while (num_swaps < n) {

   //     IGRAPH_ALLOW_INTERRUPTION(); //AH MOD
        if (num_swaps % 1000 == 0) {
            snprintf(message, sizeof(message),
                     "Random rewiring (%.2f%% of the trials were successful)",
                     num_swaps > 0 ? ((100.0 * num_successful_swaps) / num_swaps) : 0.0);
            IGRAPH_PROGRESS(message, (100.0 * num_swaps) / n, 0);
        }

        switch (mode) {
        case IGRAPH_REWIRING_SIMPLE:
        case IGRAPH_REWIRING_SIMPLE_LOOPS:
            ok = 1;

            /* Choose two edges randomly */
            VECTOR(eids)[0] = RNG_INTEGER(0, no_of_edges - 1);
            do {
                VECTOR(eids)[1] = RNG_INTEGER(0, no_of_edges - 1);
            } while (VECTOR(eids)[0] == VECTOR(eids)[1]);

            /* Get the endpoints */
            if (use_adjlist) {
                a = VECTOR(alledges)[((igraph_integer_t)VECTOR(eids)[0]) * 2];
                b = VECTOR(alledges)[(((igraph_integer_t)VECTOR(eids)[0]) * 2) + 1];
                c = VECTOR(alledges)[((igraph_integer_t)VECTOR(eids)[1]) * 2];
                d = VECTOR(alledges)[(((igraph_integer_t)VECTOR(eids)[1]) * 2) + 1];
            } else {
                IGRAPH_CHECK(igraph_edge(graph, (igraph_integer_t) VECTOR(eids)[0],
                                         &a, &b));
                IGRAPH_CHECK(igraph_edge(graph, (igraph_integer_t) VECTOR(eids)[1],
                                         &c, &d));
            }

            /* For an undirected graph, we have two "variants" of each edge, i.e.
             * a -- b and b -- a. Since some rewirings can be performed only when we
             * "swap" the endpoints, we do it now with probability 0.5 */
            if (!directed && RNG_UNIF01() < 0.5) {
                dummy = c; c = d; d = dummy;
                if (use_adjlist) {
                    /* Flip the edge in the unordered edge-list, so the update later on
                     * hits the correct end. */
                    VECTOR(alledges)[((igraph_integer_t)VECTOR(eids)[1]) * 2] = c;
                    VECTOR(alledges)[(((igraph_integer_t)VECTOR(eids)[1]) * 2) + 1] = d;
                }
            }

            /* If we do not touch loops, check whether a == b or c == d and disallow
             * the swap if needed */
            if (!loops && (a == b || c == d)) {
                ok = 0;
            } else {
                /* Check whether they are suitable for rewiring */
                if (a == c || b == d) {
                    /* Swapping would have no effect */
                    ok = 0;
                } else {
                    /* a != c && b != d */
                    /* If a == d or b == c, the swap would generate at least one loop, so
                     * we disallow them unless we want to have loops */
                    ok = loops || (a != d && b != c);
                    /* Also, if a == b and c == d and we allow loops, doing the swap
                     * would result in a multiple edge if the graph is undirected */
                    ok = ok && (directed || a != b || c != d);
                }
            }

            /* All good so far. Now check for the existence of a --> d and c --> b to
             * disallow the creation of multiple edges */
            if (ok) {
                if (use_adjlist) {
                    if (igraph_adjlist_has_edge(&al, a, d, directed)) {
                        ok = 0;
                    }
                } else {
                    IGRAPH_CHECK(igraph_are_connected(graph, a, d, &ok));
                    ok = !ok;
                }
            }
            if (ok) {
                if (use_adjlist) {
                    if (igraph_adjlist_has_edge(&al, c, b, directed)) {
                        ok = 0;
                    }
                } else {
                    IGRAPH_CHECK(igraph_are_connected(graph, c, b, &ok));
                    ok = !ok;
                }
            }

            /* If we are still okay, we can perform the rewiring */
            if (ok) {
                /* printf("Deleting: %ld -> %ld, %ld -> %ld\n",
                              (long)a, (long)b, (long)c, (long)d); */
                if (use_adjlist) {
                    /* Replace entry in sorted adjlist: */
                    IGRAPH_CHECK(igraph_adjlist_replace_edge(&al, a, b, d, directed));
                    IGRAPH_CHECK(igraph_adjlist_replace_edge(&al, c, d, b, directed));
                    /* Also replace in unsorted edgelist: */
                    VECTOR(alledges)[(((igraph_integer_t)VECTOR(eids)[0]) * 2) + 1] = d;
                    VECTOR(alledges)[(((igraph_integer_t)VECTOR(eids)[1]) * 2) + 1] = b;
                } else {
                    IGRAPH_CHECK(igraph_delete_edges(graph, es));
                    VECTOR(edgevec)[0] = a; VECTOR(edgevec)[1] = d;
                    VECTOR(edgevec)[2] = c; VECTOR(edgevec)[3] = b;
                    /* printf("Adding: %ld -> %ld, %ld -> %ld\n",
                                (long)a, (long)d, (long)c, (long)b); */
                    igraph_add_edges(graph, &edgevec, 0);
                }
                num_successful_swaps++;
            }
            break;
        default:
            RNG_END();
            IGRAPH_ERROR("unknown rewiring mode", IGRAPH_EINVMODE);
        }
        num_swaps++;
    }

    if (use_adjlist) {
        /* Replace graph edges with the adjlist current state */
        IGRAPH_CHECK(igraph_delete_edges(graph, igraph_ess_all(IGRAPH_EDGEORDER_ID)));
        IGRAPH_CHECK(igraph_add_edges(graph, &alledges, 0));
    }

    IGRAPH_PROGRESS("Random rewiring: ", 100.0, 0);

    if (use_adjlist) {
        igraph_vector_destroy(&alledges);
        igraph_adjlist_destroy(&al);
    } else {
        igraph_vector_destroy(&edgevec);
    }

    igraph_vector_destroy(&eids);
    IGRAPH_FINALLY_CLEAN(use_adjlist ? 3 : 2);

    RNG_END();

    return 0;
}

/* Modified edge-rewiring function, from igraph_i_rewire, with an additional rejection step,
 *  to limit distance between trophic levels.
 *  
 * see python prototype in coherence_sample.py:curveballs_q(), (which uses
 * curveballs while classic edge rewiring is used in the present function)
 * */

int igraph_i_rewire_q(igraph_t *graph, igraph_vector_t *s , igraph_real_t T, igraph_integer_t n, igraph_rewiring_t mode) {
    
    long int no_of_nodes = igraph_vcount(graph);
    long int no_of_edges = igraph_ecount(graph);
    char message[256];
    igraph_integer_t a, b, c, d, dummy, num_swaps, num_successful_swaps;
    igraph_real_t s_a,s_b,s_c,s_d,x,delta;
    igraph_vector_t eids, edgevec, alledges;
    igraph_bool_t directed, loops, ok;
    igraph_es_t es;
    igraph_adjlist_t al;

    if (no_of_nodes < 4) {
        IGRAPH_ERROR("graph unsuitable for rewiring", IGRAPH_EINVAL);
    }

    directed = igraph_is_directed(graph);
    loops = (mode & IGRAPH_REWIRING_SIMPLE_LOOPS);

    RNG_BEGIN();

    IGRAPH_VECTOR_INIT_FINALLY(&eids, 2);

    if (1) {
        /* As well as the sorted adjacency list, we maintain an unordered
         * list of edges for picking a random edge in constant time.
         */
        IGRAPH_CHECK(igraph_adjlist_init(graph, &al, IGRAPH_OUT, IGRAPH_LOOPS_ONCE, IGRAPH_MULTIPLE));
        IGRAPH_FINALLY(igraph_adjlist_destroy, &al);
        IGRAPH_VECTOR_INIT_FINALLY(&alledges, no_of_edges * 2);
        igraph_get_edgelist(graph, &alledges, /*bycol=*/ 0);
    } else {
        IGRAPH_VECTOR_INIT_FINALLY(&edgevec, 4);
        es = igraph_ess_vector(&eids);
    }

    /* We don't want the algorithm to get stuck in an infinite loop when
     * it can't choose two edges satisfying the conditions. Instead of
     * this, we choose two arbitrary edges and if they have endpoints
     * in common, we just decrease the number of trials left and continue
     * (so unsuccessful rewirings still count as a trial)
     */

    num_swaps = num_successful_swaps = 0;
    while (num_swaps < n) {

   //     IGRAPH_ALLOW_INTERRUPTION(); //AH MOD
        if (num_swaps % 1000 == 0) {
            snprintf(message, sizeof(message),
                     "Random rewiring (%.2f%% of the trials were successful)",
                     num_swaps > 0 ? ((100.0 * num_successful_swaps) / num_swaps) : 0.0);
            IGRAPH_PROGRESS(message, (100.0 * num_swaps) / n, 0);
        }

        switch (mode) {
        case IGRAPH_REWIRING_SIMPLE:
        case IGRAPH_REWIRING_SIMPLE_LOOPS:
            ok = 1;

            /* Choose two edges randomly */
            VECTOR(eids)[0] = RNG_INTEGER(0, no_of_edges - 1);
            do {
                VECTOR(eids)[1] = RNG_INTEGER(0, no_of_edges - 1);
            } while (VECTOR(eids)[0] == VECTOR(eids)[1]);

            /* Get the endpoints */
            if (1) {
                a = VECTOR(alledges)[((igraph_integer_t)VECTOR(eids)[0]) * 2];
                b = VECTOR(alledges)[(((igraph_integer_t)VECTOR(eids)[0]) * 2) + 1];
                c = VECTOR(alledges)[((igraph_integer_t)VECTOR(eids)[1]) * 2];
                d = VECTOR(alledges)[(((igraph_integer_t)VECTOR(eids)[1]) * 2) + 1];
            } 

            /* For an undirected graph, we have two "variants" of each edge, i.e.
             * a -- b and b -- a. Since some rewirings can be performed only when we
             * "swap" the endpoints, we do it now with probability 0.5 */
            if (!directed && RNG_UNIF01() < 0.5) {
                dummy = c; c = d; d = dummy;
                if (1) {
                    /* Flip the edge in the unordered edge-list, so the update later on
                     * hits the correct end. */
                    VECTOR(alledges)[((igraph_integer_t)VECTOR(eids)[1]) * 2] = c;
                    VECTOR(alledges)[(((igraph_integer_t)VECTOR(eids)[1]) * 2) + 1] = d;
                }
            }

            /* If we do not touch loops, check whether a == b or c == d and disallow
             * the swap if needed */
            if (!loops && (a == b || c == d)) {
                ok = 0;
            } else {
                /* Check whether they are suitable for rewiring */
                if (a == c || b == d) {
                    /* Swapping would have no effect */
                    ok = 0;
                } else {
                    /* a != c && b != d */
                    /* If a == d or b == c, the swap would generate at least one loop, so
                     * we disallow them unless we want to have loops */
                    ok = loops || (a != d && b != c);
                    /* Also, if a == b and c == d and we allow loops, doing the swap
                     * would result in a multiple edge if the graph is undirected */
                    ok = ok && (directed || a != b || c != d);
                }
            }

            /* All good so far. Now check for the existence of a --> d and c --> b to
             * disallow the creation of multiple edges */
            if (ok) {
                if (1) {
                    if (igraph_adjlist_has_edge(&al, a, d, directed)) {
                        ok = 0;
                    }
                } else {
                    IGRAPH_CHECK(igraph_are_connected(graph, a, d, &ok));
                    ok = !ok;
                }
            }
            if (ok) {
                if (1) {
                    if (igraph_adjlist_has_edge(&al, c, b, directed)) {
                        ok = 0;
                    }
                } else {
                    IGRAPH_CHECK(igraph_are_connected(graph, c, b, &ok));
                    ok = !ok;
                }
            }

            /* MOD AH : trophic levels should not be too far apart */ 
            if (ok) {
                s_a = igraph_vector_e(s,a);   
                s_b = igraph_vector_e(s,b); 
                s_c = igraph_vector_e(s,c); 
                s_d = igraph_vector_e(s,d); 
                /*x = abs( (s_b-s_d) *(s_a-s_c) );
                delta = exp(-x/T);*/
                x = s_b-s_d;
                delta = exp(  - (x*x)/(2*T*T));
                if (RNG_UNIF01() > delta){
                    ok=0;
                }
            }    
            

            /* If we are still okay, we can perform the rewiring */
            if (ok) {
                /* printf("Deleting: %ld -> %ld, %ld -> %ld\n",
                              (long)a, (long)b, (long)c, (long)d); */
                if (1) {
                    /* Replace entry in sorted adjlist: */
                    IGRAPH_CHECK(igraph_adjlist_replace_edge(&al, a, b, d, directed));
                    IGRAPH_CHECK(igraph_adjlist_replace_edge(&al, c, d, b, directed));
                    /* Also replace in unsorted edgelist: */
                    VECTOR(alledges)[(((igraph_integer_t)VECTOR(eids)[0]) * 2) + 1] = d;
                    VECTOR(alledges)[(((igraph_integer_t)VECTOR(eids)[1]) * 2) + 1] = b;
                } else {
                    IGRAPH_CHECK(igraph_delete_edges(graph, es));
                    VECTOR(edgevec)[0] = a; VECTOR(edgevec)[1] = d;
                    VECTOR(edgevec)[2] = c; VECTOR(edgevec)[3] = b;
                    /* printf("Adding: %ld -> %ld, %ld -> %ld\n",
                                (long)a, (long)d, (long)c, (long)b); */
                    igraph_add_edges(graph, &edgevec, 0);
                }
                num_successful_swaps++;
            }
            break;
        default:
            RNG_END();
            IGRAPH_ERROR("unknown rewiring mode", IGRAPH_EINVMODE);
        }
        num_swaps++;
    }

    if (1) {
        /* Replace graph edges with the adjlist current state */
        IGRAPH_CHECK(igraph_delete_edges(graph, igraph_ess_all(IGRAPH_EDGEORDER_ID)));
        IGRAPH_CHECK(igraph_add_edges(graph, &alledges, 0));
    }

    IGRAPH_PROGRESS("Random rewiring: ", 100.0, 0);

    if (1) {
        igraph_vector_destroy(&alledges);
        igraph_adjlist_destroy(&al);
    } else {
        igraph_vector_destroy(&edgevec);
    }

    igraph_vector_destroy(&eids);
    //IGRAPH_FINALLY_CLEAN(use_adjlist ? 3 : 2);
    IGRAPH_FINALLY_CLEAN(3);

    RNG_END();

    //return 0;
    return num_successful_swaps;
}



/* utility function that calls igraph_i_rewire_q and updates s
 * 
 * 
 * */
int igraph_i_rewire_q_loop(igraph_t *graph, int n_iter, int n_rewire, int n_update_s, int max_iter_gsl, float Temp, int seed,igraph_vector_t *out_q,igraph_vector_t *out_swap,igraph_vector_t *out_dist  ) {
    // NB : in this version, trophic_level is included in the function body

    // igraph stuff
    long int no_of_edges = igraph_ecount(graph);   
    long int no_of_nodes = igraph_vcount(graph);  
      
    igraph_t g_cp;
    igraph_adjlist_t al,al_cp;
    igraph_vector_t outdeg, indeg, vec;
    igraph_bool_t is_simple;
    igraph_real_t mean=0., std=0.;
    int len,i, ii,j,k;
    //igraph_bool_t use_adjlist=1;
    igraph_integer_t num_successful_swaps,cum_num_successful_swaps=0;
    igraph_real_t value;
    igraph_vector_t s,x;
    igraph_vector_init_real(&s, 0.);
    igraph_vector_init_real(&x, 0.);
    IGRAPH_CHECK(igraph_vector_resize(&s, no_of_nodes));	
    IGRAPH_CHECK(igraph_vector_resize(&x, no_of_edges));	

	igraph_bool_t directed    = 1;
    
    // Set random seed for reproducibility 
    igraph_rng_seed(igraph_rng_default(), seed);     
                                         
    // copy graph. see https://igraph.org/c/doc/igraph-Basic.html#igraph_copy
    igraph_copy(&g_cp, graph);
       
	//get in-degree
	igraph_vector_t indegree;
	IGRAPH_VECTOR_INIT_FINALLY(&indegree, no_of_nodes );
	IGRAPH_CHECK(igraph_degree(graph, &indegree, igraph_vss_all(),
								   directed ? IGRAPH_IN : IGRAPH_ALL, IGRAPH_LOOPS));
    //init adjlist_cp
    IGRAPH_CHECK(igraph_adjlist_init(&g_cp, &al_cp, IGRAPH_OUT, IGRAPH_LOOPS_ONCE, IGRAPH_MULTIPLE));
      
	//init gsl stuff
	const double tol = 1.0e-6;  /* solution relative tolerance */
	//const size_t max_iter_gsl = 10000; /* maximum iterations */
	const gsl_splinalg_itersolve_type *T = gsl_splinalg_itersolve_gmres;
	gsl_splinalg_itersolve *work =
	gsl_splinalg_itersolve_alloc(T, no_of_nodes, 0);
	size_t iter = 0;
	double residual;
	int status;          
            
    gsl_spmatrix *A = gsl_spmatrix_alloc(no_of_nodes ,no_of_nodes); /* triplet format */        
    gsl_spmatrix *C;                            /* compressed format */
    gsl_vector *f = gsl_vector_alloc(no_of_nodes);        /* right hand side vector */
    gsl_vector *u = gsl_vector_alloc(no_of_nodes);        /* solution vector */               

    // end init, print
    printf("is directed: %d\n",  igraph_is_directed(graph));
    printf("vcount: %d\n", igraph_vcount(graph)) ;
    printf("ecount: %d\n", igraph_ecount(graph)) ;

    //main loop
    //IGRAPH_FINALLY(igraph_adjlist_destroy, &al);
    //IGRAPH_FINALLY(igraph_adjlist_destroy, &al_cp);
    
    for (ii= 0; ii < n_iter; ii++) {
            igraph_bool_t  cond = (ii==0)|(ii%n_update_s==0);
            
			if(cond){
			    //-----------START trophic---------------- 
                printf("start compute s, ii=%d\n",ii);
				// construct right hand side vector 
				for (i = 0; i < no_of_nodes; ++i)
					{
					 k = VECTOR(indegree)[i] ;
					 if (k>1) { 
						gsl_vector_set(f, i,   k   );
						}
					else{
						gsl_vector_set(f, i,   1.  );
						}
					}
				// construct the sparse matrix for the finite difference equation //
				igraph_eit_t eit; //edge iterator
				igraph_eit_create(graph, igraph_ess_all(IGRAPH_EDGEORDER_FROM), &eit);
				// see igraph/examples/simple/igraph_similarity.c ; https://igraph.org/c/doc/igraph-Iterators.html#edge-iterators
                // reset A
                gsl_spmatrix_set_zero(A);
                
				while (!IGRAPH_EIT_END(eit)) {
					long int eid = IGRAPH_EIT_GET(eit);
					i = IGRAPH_FROM(graph, eid);
					j = IGRAPH_TO(graph, eid);
					gsl_spmatrix_set(A, j, i , -1); 
					IGRAPH_EIT_NEXT(eit);
					//k++;
				}
				igraph_eit_destroy(&eit);
				
				for (i = 0; i < no_of_nodes; ++i)
				{
					gsl_spmatrix_set(A, i, i , gsl_vector_get(f, i ));
				 }   
				// convert to compressed column format 
				C = gsl_spmatrix_ccs(A);
				
				 // now solve the system with the GMRES iterative solver 
				{
				// initial guess u = 0 
				//gsl_vector_set_zero(u);
				// use values already in vector
				for (i = 0; i < no_of_nodes; ++i)
					{   gsl_vector_set(u, i,   VECTOR(s)[i]  );          
					}	
				// solve the system A u = f 
				do
				  {
					status = gsl_splinalg_itersolve_iterate(C, f, tol, u, work);
					// print out residual norm ||A*u - f|| 
					residual = gsl_splinalg_itersolve_normr(work);
					//fprintf(stderr, "iter %zu residual = %.12e\n", iter, residual);
					//if (status == GSL_SUCCESS)
					//  fprintf(stderr, "Converged\n");
				  }
				while (status == GSL_CONTINUE && ++iter < max_iter_gsl);

				// output solution 
				for (i = 0; i < no_of_nodes; ++i)
				  {
					VECTOR(s)[i]  = gsl_vector_get(u, i);
				  }
				}                
				//-------END trophic levels-------------
                
                // compute xij, q
                //igraph_vector_print(&s);  
			    trophic_level_difference(graph, &s, &x);
                moments(no_of_edges, &x, &mean, &std);
                printf("end compute s; residual=%f  E[xij] =%f   q=STD[xij] =%f \n",residual,mean,std);	
                
                // get distance wrt initial graph
                IGRAPH_CHECK(igraph_adjlist_init(graph, &al, IGRAPH_OUT, IGRAPH_LOOPS_ONCE, IGRAPH_MULTIPLE));        
                len = adjlist_dist( &al,&al_cp, IGRAPH_ADJ_DIRECTED);
                igraph_adjlist_destroy(&al);
                printf("compute distance: len = %d  cum_num_successful_swaps: %d \n",len,cum_num_successful_swaps);
                
                // record statistics
                igraph_vector_push_back(out_q,std);	
                igraph_vector_push_back(out_swap,cum_num_successful_swaps);	
                igraph_vector_push_back(out_dist,len);	
			}	
            
            // rewire
            num_successful_swaps = igraph_i_rewire_q(graph, &s, Temp, n_rewire,  IGRAPH_REWIRING_SIMPLE); 
            cum_num_successful_swaps += num_successful_swaps;

    }
    //cleanup igraph
	igraph_vector_destroy(&indegree);
	IGRAPH_FINALLY_CLEAN(1);
    igraph_adjlist_destroy(&al_cp);
    //IGRAPH_FINALLY_CLEAN( 2);
    igraph_destroy(&g_cp);
    
    //cleanup gsl    
    gsl_splinalg_itersolve_free(work);    
    gsl_spmatrix_free(A);
    gsl_spmatrix_free(C);
    gsl_vector_free(f);
    gsl_vector_free(u);
    
    return 0;
}


int igraph_i_rewire_q_loop_FAIL(igraph_t *g, int n_iter, int n_rewire, int n_update_s, float T, int seed ) {
    igraph_t g_cp;
    igraph_adjlist_t al,al_cp;
    igraph_vector_t outdeg, indeg, vec;
    igraph_bool_t is_simple;
    igraph_real_t mean, std;
    int len,i;
    //igraph_bool_t use_adjlist=1;
    igraph_integer_t num_successful_swaps;
    igraph_real_t value;
    igraph_vector_t s,x;
    igraph_vector_init_real(&s, 0.);
    igraph_vector_init_real(&x, 0.);
    
    long int no_of_edges = igraph_ecount(g);   
    long int no_of_nodes = igraph_vcount(g);  
    
    // Set random seed for reproducibility 
    igraph_rng_seed(igraph_rng_default(), seed);     
                                         
    // copy graph. see https://igraph.org/c/doc/igraph-Basic.html#igraph_copy
    igraph_copy(&g_cp, g);
            
    //gsl_spmatrix *A = gsl_spmatrix_alloc(no_of_nodes ,no_of_nodes); /* triplet format */        
    //gsl_spmatrix *C;                            /* compressed format */
    //gsl_vector *f = gsl_vector_alloc(no_of_nodes);        /* right hand side vector */
    //gsl_vector *u = gsl_vector_alloc(no_of_nodes);        /* solution vector */
        
                      
    /*get trophic */                
    trophic_level(g,   &s);
    //trophic_level2(g,&s,A ,C,f ,u);
    
    //igraph_vector_print(&s);                    
    trophic_level_difference(g, &s, &x);
 
    moments(no_of_edges, &x, &mean, &std);
    printf("E[xij] =%f   q=STD[xij] =%f \n",mean,std);
    
    //main loop
    //IGRAPH_FINALLY(igraph_adjlist_destroy, &al);
    //IGRAPH_FINALLY(igraph_adjlist_destroy, &al_cp);

    printf("is directed: %d\n",  igraph_is_directed(g));
    printf("vcount: %d\n", igraph_vcount(g)) ;
    printf("ecount: %d\n", igraph_ecount(g)) ;
    
    
    IGRAPH_CHECK(igraph_adjlist_init(&g_cp, &al_cp, IGRAPH_OUT, IGRAPH_LOOPS_ONCE, IGRAPH_MULTIPLE));
    for (i = 0; i < n_iter; i++) {
            // rewire
            num_successful_swaps = igraph_i_rewire_q(g, &s, T, n_rewire,  IGRAPH_REWIRING_SIMPLE); 

            // get distance wrt initial graph
            if( i%n_update_s==0){
                //populate adjlist
                IGRAPH_CHECK(igraph_adjlist_init(g, &al, IGRAPH_OUT, IGRAPH_LOOPS_ONCE, IGRAPH_MULTIPLE));        
            
                len = adjlist_dist( &al,&al_cp, IGRAPH_ADJ_DIRECTED);
                igraph_adjlist_destroy(&al);
                printf("len = %d  num_successful_swaps: %d   / %d ",len,num_successful_swaps, n_rewire);

                trophic_level(&g,   &s);
                //trophic_level2(g,&s,A ,C,f ,u);
                
                trophic_level_difference(&g, &s, &x);
                moments(no_of_edges, &x, &mean, &std);
                printf("  E[xij] =%f   q=STD[xij] =%f \n",mean,std);
            }
    }
    //cleanup
    igraph_adjlist_destroy(&al_cp);
    //IGRAPH_FINALLY_CLEAN( 2);

    //igraph_destroy(g);
    igraph_destroy(&g_cp);
    
    
    //gsl_spmatrix_free(A);
    //gsl_spmatrix_free(C);
    //gsl_vector_free(f);
    //gsl_vector_free(u);
    
    return 0;
}


/*
def perturbation_score_adj_list(al_r, al_0):
	return sum([len(al_r[i]-al_0[i]) for i in range(len(al_0))])/float(sum([len(j) for j in al_0]))
*/


/* compute distance between adjacency lists, see [Carstens 16], and related python implementation.
 *  in curveballs.py
 * 
 * https://igraph.org/c/doc/igraph-Data-structures.html#igraph-Adjlists
 * vector_difference_sorted: https://igraph.org/c/html/latest/igraph-Data-structures.html#igraph_vector_difference_sorted
 * 
 * */
int adjlist_dist(igraph_adjlist_t *al0,igraph_adjlist_t *al1, igraph_neimode_t mode ){
    
    igraph_integer_t i, len=0;
    igraph_vector_t tmp;
    
    igraph_vector_init(&tmp, 0);
    IGRAPH_FINALLY(igraph_vector_destroy, &tmp);


    /*
    switch (mode) {
        case IGRAPH_ADJ_UNDIRECTED:        
                IGRAPH_ERROR("unsupported mode", IGRAPH_ADJ_UNDIRECTED);
        case IGRAPH_ADJ_DIRECTED:        
            }
    */
    if ( al0->length != al1->length){
         IGRAPH_ERROR("adjacency list lengths differ",0);
    }
    
    igraph_adjlist_sort(al0);
    igraph_adjlist_sort(al1);
    
        for (i = 0; i < al0->length; i++) {
            igraph_vector_difference_sorted(  (igraph_vector_t *)&al0->adjs[i] ,   (igraph_vector_t *)&al1->adjs[i]  , &tmp);
            len += igraph_vector_size(&tmp);
    }
    
    igraph_vector_destroy(&tmp);
    IGRAPH_FINALLY_CLEAN(1);

    
    return len;
}


/* compute trophic level using iterative sparse solver.
 *  
 * COMMENT: consistency can be checked  since <x_{ij}> =1 by definition,
 * where <.> is the average over edges in a given network realization.
 * See [JJ17] for explanation.
 *   
 *  sparse solver: https://www.gnu.org/software/gsl/doc/html/splinalg.html
 * 
 * */
int trophic_level(const igraph_t *graph, igraph_vector_t *vector){
    
    // resize output vector
    int i,j,k;
    igraph_bool_t directed    = 1;
    long int no_of_nodes = igraph_vcount(graph);

    gsl_spmatrix *A = gsl_spmatrix_alloc(no_of_nodes ,no_of_nodes); /* triplet format */        
    gsl_spmatrix *C;                            /* compressed format */
    gsl_vector *f = gsl_vector_alloc(no_of_nodes);        /* right hand side vector */
    gsl_vector *u = gsl_vector_alloc(no_of_nodes);        /* solution vector */

    IGRAPH_CHECK(igraph_vector_resize(vector, no_of_nodes));
    
    /*get in-degree*/
    igraph_vector_t indegree;
    IGRAPH_VECTOR_INIT_FINALLY(&indegree, no_of_nodes );
    IGRAPH_CHECK(igraph_degree(graph, &indegree, igraph_vss_all(),
                                   directed ? IGRAPH_IN : IGRAPH_ALL, IGRAPH_LOOPS));
    /* construct right hand side vector */
    for (i = 0; i < no_of_nodes; ++i)
        {
         k = VECTOR(indegree)[i] ;
         if (k>1) { 
            gsl_vector_set(f, i,   k   );
            }
        else{
            gsl_vector_set(f, i,   1.  );
            }
        }

    /* construct the sparse matrix for the finite difference equation */
    igraph_eit_t eit; //edge iterator
    igraph_eit_create(graph, igraph_ess_all(IGRAPH_EDGEORDER_FROM), &eit);
    // see igraph/examples/simple/igraph_similarity.c ; https://igraph.org/c/doc/igraph-Iterators.html#edge-iterators
    while (!IGRAPH_EIT_END(eit)) {
        long int eid = IGRAPH_EIT_GET(eit);
        i = IGRAPH_FROM(graph, eid);
        j = IGRAPH_TO(graph, eid);
        gsl_spmatrix_set(A, j, i , -1); 
        IGRAPH_EIT_NEXT(eit);
        //k++;
    }
    igraph_eit_destroy(&eit);
    
    for (i = 0; i < no_of_nodes; ++i)
    {
        gsl_spmatrix_set(A, i, i , gsl_vector_get(f, i ));
     }   

    /* convert to compressed column format */
    C = gsl_spmatrix_ccs(A);
    
     /* now solve the system with the GMRES iterative solver */
    //{
    const double tol = 1.0e-6;  /* solution relative tolerance */
    const size_t max_iter_gsl = 100; /* maximum iterations */
    const gsl_splinalg_itersolve_type *T = gsl_splinalg_itersolve_gmres;
    gsl_splinalg_itersolve *work =
      gsl_splinalg_itersolve_alloc(T, no_of_nodes, 0);
    size_t iter = 0;
    double residual;
    int status;

    /* initial guess u = 0 */
    //gsl_vector_set_zero(u);
    // use values already in vector
    for (i = 0; i < no_of_nodes; ++i)
        {   gsl_vector_set(u, i,   VECTOR(*vector)[i]  );          
        }


    /* solve the system A u = f */
    do
      {
        status = gsl_splinalg_itersolve_iterate(C, f, tol, u, work);

        /* print out residual norm ||A*u - f|| */
        residual = gsl_splinalg_itersolve_normr(work);
        //fprintf(stderr, "iter %zu residual = %.12e\n", iter, residual);

        //if (status == GSL_SUCCESS)
        //  fprintf(stderr, "Converged\n");
      }
    while (status == GSL_CONTINUE && ++iter < max_iter_gsl);

    /* output solution */
    for (i = 0; i < no_of_nodes; ++i)
      {
        VECTOR(*vector)[i]  = gsl_vector_get(u, i);
      }

    gsl_splinalg_itersolve_free(work);
    //}
    //free gsl stuff 
    gsl_spmatrix_free(A);
    gsl_spmatrix_free(C);
    gsl_vector_free(f);
    gsl_vector_free(u);
    //free igraph
    igraph_vector_destroy(&indegree);
    IGRAPH_FINALLY_CLEAN(1);

  return GSL_SUCCESS; 
    
} 
 
int trophic_level2(const igraph_t *graph, igraph_vector_t *vector,gsl_spmatrix *A , gsl_spmatrix *C,gsl_vector *f ,gsl_vector *u){
    
    // resize output vector
    int i,j,k;
    igraph_bool_t directed    = 1;
    long int no_of_nodes = igraph_vcount(graph);

    //gsl_spmatrix *A = gsl_spmatrix_alloc(no_of_nodes ,no_of_nodes); /* triplet format */        
    //gsl_spmatrix *C;                            /* compressed format */
    //gsl_vector *f = gsl_vector_alloc(no_of_nodes);        /* right hand side vector */
    //gsl_vector *u = gsl_vector_alloc(no_of_nodes);        /* solution vector */

    IGRAPH_CHECK(igraph_vector_resize(vector, no_of_nodes));
    
    /*get in-degree*/
    igraph_vector_t indegree;
    IGRAPH_VECTOR_INIT_FINALLY(&indegree, no_of_nodes );
    IGRAPH_CHECK(igraph_degree(graph, &indegree, igraph_vss_all(),
                                   directed ? IGRAPH_IN : IGRAPH_ALL, IGRAPH_LOOPS));
    /* construct right hand side vector */
    for (i = 0; i < no_of_nodes; ++i)
        {
         k = VECTOR(indegree)[i] ;
         if (k>1) { 
            gsl_vector_set(f, i,   k   );
            }
        else{
            gsl_vector_set(f, i,   1.  );
            }
        }

    /* construct the sparse matrix for the finite difference equation */
    igraph_eit_t eit; //edge iterator
    igraph_eit_create(graph, igraph_ess_all(IGRAPH_EDGEORDER_FROM), &eit);
    // see igraph/examples/simple/igraph_similarity.c ; https://igraph.org/c/doc/igraph-Iterators.html#edge-iterators
    while (!IGRAPH_EIT_END(eit)) {
        long int eid = IGRAPH_EIT_GET(eit);
        i = IGRAPH_FROM(graph, eid);
        j = IGRAPH_TO(graph, eid);
        gsl_spmatrix_set(A, j, i , -1); 
        IGRAPH_EIT_NEXT(eit);
        //k++;
    }
    igraph_eit_destroy(&eit);
    
    for (i = 0; i < no_of_nodes; ++i)
    {
        gsl_spmatrix_set(A, i, i , gsl_vector_get(f, i ));
     }   

    /* convert to compressed column format */
    C = gsl_spmatrix_ccs(A);
    
     /* now solve the system with the GMRES iterative solver */
    {
    const double tol = 1.0e-6;  /* solution relative tolerance */
    const size_t max_iter = 100; /* maximum iterations */
    const gsl_splinalg_itersolve_type *T = gsl_splinalg_itersolve_gmres;
    gsl_splinalg_itersolve *work =
      gsl_splinalg_itersolve_alloc(T, no_of_nodes, 0);
    size_t iter = 0;
    double residual;
    int status;

    /* initial guess u = 0 */
    //gsl_vector_set_zero(u);
    // use values already in vector
    for (i = 0; i < no_of_nodes; ++i)
        {   gsl_vector_set(u, i,   VECTOR(*vector)[i]  );          
        }


    /* solve the system A u = f */
    do
      {
        status = gsl_splinalg_itersolve_iterate(C, f, tol, u, work);

        /* print out residual norm ||A*u - f|| */
        residual = gsl_splinalg_itersolve_normr(work);
        //fprintf(stderr, "iter %zu residual = %.12e\n", iter, residual);

        //if (status == GSL_SUCCESS)
        //  fprintf(stderr, "Converged\n");
      }
    while (status == GSL_CONTINUE && ++iter < max_iter);

    /* output solution */
    for (i = 0; i < no_of_nodes; ++i)
      {
        VECTOR(*vector)[i]  = gsl_vector_get(u, i);
      }

    gsl_splinalg_itersolve_free(work);
    }
    //free gsl stuff 
    /*
    gsl_spmatrix_free(A);
    gsl_spmatrix_free(C);
    gsl_vector_free(f);
    gsl_vector_free(u);*/
    //free igraph
    igraph_vector_destroy(&indegree);
    IGRAPH_FINALLY_CLEAN(1);

  return GSL_SUCCESS; 
    
}  
 
 /* Compute x_{ij}=s_i -s_j over edges
  * 
  * 
  * 
  * */
 
int trophic_level_difference(const igraph_t *graph, const igraph_vector_t *s, igraph_vector_t *x)
{
    int i,j,k ;
    igraph_real_t  xij;
    long int no_of_nodes = igraph_vcount(graph);
    long int no_of_edges = igraph_ecount(graph);
    //IGRAPH_CHECK(igraph_vector_resize(x, no_of_edges));
    //IGRAPH_CHECK(igraph_vector_resize(x, 0));
    igraph_vector_null(x)    ;
        
    /* iterate edges */
    igraph_eit_t eit; //edge iterator
    igraph_eit_create(graph, igraph_ess_all(IGRAPH_EDGEORDER_FROM), &eit);

    // see igraph/examples/simple/igraph_similarity.c ; https://igraph.org/c/doc/igraph-Iterators.html#edge-iterators
    k=0;
    while (!IGRAPH_EIT_END(eit)) {
        long int eid = IGRAPH_EIT_GET(eit);
        i = IGRAPH_FROM(graph, eid);
        j = IGRAPH_TO(graph, eid);
        xij = VECTOR(*s)[j] - VECTOR(*s)[i];
        VECTOR(*x)[k]  = xij;
        
        //igraph_vector_push_back(x, xij);
        IGRAPH_EIT_NEXT(eit);
        k++;
    }
    igraph_eit_destroy(&eit);
        
    }
 
 
 /*
  * compute first moments of a vector
  * 
  */
 
int moments(int n_edge, igraph_vector_t *x, igraph_real_t *mean, igraph_real_t *std)
{
    int i;
    double sum=0.;
    double tmp, var=0;
    
    
    for (i = 0; i < n_edge; i++) {
        sum += VECTOR(*x)[i] ;}
    sum /= n_edge;

    for (i = 0; i < n_edge; i++) {
        tmp = (VECTOR(*x)[i] - sum);
        var +=   tmp*tmp;}    
    var /= (n_edge-1);
    
    *mean = sum ;
    *std = sqrt(var);
} 
 
 
 // --------------------------------------------------------------------
 // FAIL: DO NOT USE FUNCTIONS BELOW
  
 
/*
 * see function igraph_i_personalized_pagerank_arpack(..)  in IGRAPH_HOME/src/centrality_other.c
 *         IGRAPH_CHECK(igraph_arpack_rnsolve(igraph_i_pagerank, &data, options, 0, &values, &vectors));
 *
 * ARPACK-based implementation of \c igraph_personalized_pagerank.
 *
 * See \c igraph_personalized_pagerank for the documentation of the parameters.
 * 
 * */
typedef struct igraph_i_pagerank_data_t {
    const igraph_t *graph;
    igraph_adjlist_t *adjlist;
    igraph_real_t damping;
    igraph_vector_t *outdegree;
    igraph_vector_t *indegree;
    igraph_vector_t *tmp;
    igraph_vector_t *reset;
} igraph_i_pagerank_data_t;


/* utility function, copied and modified after igraph_i_pagerank.
 * Is used by tropic_level_FAIL
 * 
 *  COMMENT: DO NOT USE
 * 
 * */

static int igraph_i_coherence(igraph_real_t *to, const igraph_real_t *from,
                             int n, void *extra) {

    igraph_i_pagerank_data_t *data = extra;
    igraph_adjlist_t *adjlist = data->adjlist;
    igraph_vector_t *outdegree = data->outdegree;
    igraph_vector_t *indegree = data->indegree;     // AH
    igraph_vector_t *tmp = data->tmp;
    igraph_vector_t *reset = data->reset;
    igraph_vector_int_t *neis;
    long int i, j, nlen;
    igraph_real_t sumfrom = 0.0;
    igraph_real_t fact = 1 - data->damping;

    for (i = 0; i < n; i++) {
        //sumfrom += VECTOR(*outdegree)[i] != 0 ? from[i] * fact : from[i];
        VECTOR(*tmp)[i] = from[i];
    }


    /* Here we calculate the part of the `to` vector that results from
     * moving along links (and not from teleportation) */
    for (i = 0; i < n; i++) {
        neis = igraph_adjlist_get(adjlist, i);
        nlen = igraph_vector_int_size(neis);
        to[i] = 0.0;
        if ( VECTOR(*indegree)[i] >0){
            for (j = 0; j < nlen; j++) {
                long int nei = (long int) VECTOR(*neis)[j];
                to[i] += VECTOR(*tmp)[nei];    // AH :  to[i] =  \sum s_j a_ij       ,  with a_ij =  edge from j to i, as in JJ17
            }
            to[i] *= data->damping;

             to[i] /=VECTOR(*indegree)[i];
             to[i] +=1.0;
            }
        else
        {to[i] = 1.0;
             }    
     // printf("to[i] = %f",to[i])   ;    
    }

    /* Now we add the contribution from random jumps. `reset` is a vector
     * that defines the probability of ending up in vertex i after a jump.
     * `sumfrom` is the global probability of jumping as mentioned above. */
    /* printf("sumfrom = %.6f\n", (float)sumfrom); */

    return 0;
}

/* utility function, not used below, except as a model to write igraph_i_coherence 
 * copied from HOME_IGRAPH/src/centrality/centrality_other.c
 * 
 *  COMMENT: do not use
 * 
 * */

static int igraph_i_pagerank(igraph_real_t *to, const igraph_real_t *from,
                             int n, void *extra) {

    igraph_i_pagerank_data_t *data = extra;
    igraph_adjlist_t *adjlist = data->adjlist;
    igraph_vector_t *outdegree = data->outdegree;
    igraph_vector_t *tmp = data->tmp;
    igraph_vector_t *reset = data->reset;
    igraph_vector_int_t *neis;
    long int i, j, nlen;
    igraph_real_t sumfrom = 0.0;
    igraph_real_t fact = 1 - data->damping;

    /* Calculate p(x) / outdegree(x) in advance for all the vertices.
     * Note that we may divide by zero here; this is intentional since
     * we won't use those values and we save a comparison this way.
     * At the same time, we calculate the global probability of a
     * random jump in `sumfrom`. For vertices with no outgoing edges,
     * we will surely jump from there if we are there, hence those
     * vertices contribute p(x) to the teleportation probability.
     * For vertices with some outgoing edges, we jump from there with
     * probability `fact` if we are there, hence they contribute
     * p(x)*fact */
    for (i = 0; i < n; i++) {
        sumfrom += VECTOR(*outdegree)[i] != 0 ? from[i] * fact : from[i];
        VECTOR(*tmp)[i] = from[i] / VECTOR(*outdegree)[i];
    }

    /* Here we calculate the part of the `to` vector that results from
     * moving along links (and not from teleportation) */
    for (i = 0; i < n; i++) {
        neis = igraph_adjlist_get(adjlist, i);
        nlen = igraph_vector_int_size(neis);
        to[i] = 0.0;
        for (j = 0; j < nlen; j++) {
            long int nei = (long int) VECTOR(*neis)[j];
            to[i] += VECTOR(*tmp)[nei];    // AH :  to[i] =  \sum s_j a_ij       ,  with a_ij =  edge from j to i, as in JJ17
        }
        to[i] *= data->damping;
    }

    /* Now we add the contribution from random jumps. `reset` is a vector
     * that defines the probability of ending up in vertex i after a jump.
     * `sumfrom` is the global probability of jumping as mentioned above. */
    /* printf("sumfrom = %.6f\n", (float)sumfrom); */

    if (reset) {
        /* Running personalized PageRank */
        for (i = 0; i < n; i++) {
            to[i] += sumfrom * VECTOR(*reset)[i];
        }
    } else {
        /* Traditional PageRank with uniform reset vector */
        sumfrom /= n;
        for (i = 0; i < n; i++) {
            to[i] += sumfrom;
        }
    }

    return 0;
}

 
 /*
  * tentative implementation of trophic_level building on function
  * igraph_i_personalized_pagerank_arpack()
  * in HOME_IGRAPH/src/centrality/centrality_other.c
  * 
  *  COMMENT: FAILS
  *      leading eigenvalue (in value) is 0. Output vector is strange.
  * 
  * 
  * */
 int trophic_level_FAIL(const igraph_t *graph, igraph_vector_t *vector,
                                                 igraph_real_t *value, const igraph_vs_t vids,
                                                 igraph_bool_t directed, igraph_real_t damping,
                                                 const igraph_vector_t *reset,
                                                 igraph_arpack_options_t *options) {
    igraph_matrix_t values;
    igraph_matrix_t vectors;
    igraph_neimode_t dirmode;
    igraph_vector_t outdegree;
    igraph_vector_t indegree;
    igraph_vector_t tmp;
    igraph_vector_t normalized_reset;

    long int i;
    long int no_of_nodes = igraph_vcount(graph);
    long int no_of_edges = igraph_ecount(graph);

    if (reset && igraph_vector_size(reset) != no_of_nodes) {
        IGRAPH_ERROR("Invalid length of reset vector when calculating personalized PageRank scores.", IGRAPH_EINVAL);
    }

    if (no_of_edges == 0) {
        /* Special case: graph with no edges. Result is the same as the personalization vector. */
        if (value) {
            *value = 1.0;
        }
        if (vector) {
            IGRAPH_CHECK(igraph_vector_resize(vector, no_of_nodes));
            if (reset && no_of_nodes > 0) {
                for (i=0; i < no_of_nodes; ++i) {
                    VECTOR(*vector)[i] = VECTOR(*reset)[i];
                }
                igraph_vector_scale(vector, 1.0 / igraph_vector_sum(vector));
            } else {
                igraph_vector_fill(vector, 1.0 / no_of_nodes);
            }
        }
        return IGRAPH_SUCCESS;
    }

    options->n = (int) no_of_nodes;
    options->nev = 1;
    options->ncv = 0;   /* 0 means "automatic" in igraph_arpack_rnsolve */
    options->which[0] = 'L'; options->which[1] = 'M';
    options->start = 1;       /* no random start vector */

    directed = directed && igraph_is_directed(graph);


    IGRAPH_MATRIX_INIT_FINALLY(&values, 0, 0);
    IGRAPH_MATRIX_INIT_FINALLY(&vectors, options->n, 1);

    if (directed) {
        dirmode = IGRAPH_IN;
    } else {
        dirmode = IGRAPH_ALL;
    }

    IGRAPH_VECTOR_INIT_FINALLY(&indegree, options->n);
    IGRAPH_VECTOR_INIT_FINALLY(&outdegree, options->n);
    IGRAPH_VECTOR_INIT_FINALLY(&tmp, options->n);

    RNG_BEGIN();

    if (reset) {
        /* Normalize reset vector so the sum is 1 */
        double reset_sum, reset_min;
        reset_min = igraph_vector_min(reset);
        if (reset_min < 0) {
            IGRAPH_ERROR("The reset vector must not contain negative elements.", IGRAPH_EINVAL);
        }
        if (igraph_is_nan(reset_min)) {
            IGRAPH_ERROR("The reset vector must not contain NaN values.", IGRAPH_EINVAL);
        }
        reset_sum = igraph_vector_sum(reset);
        if (reset_sum == 0) {
            IGRAPH_ERROR("The sum of the elements in the reset vector must not be zero.", IGRAPH_EINVAL);
        }

        IGRAPH_CHECK(igraph_vector_copy(&normalized_reset, reset));
        IGRAPH_FINALLY(igraph_vector_destroy, &normalized_reset);

        igraph_vector_scale(&normalized_reset, 1.0 / reset_sum);
    }

    //if (!weights) {

        igraph_adjlist_t adjlist;
        igraph_i_pagerank_data_t data;

        data.graph = graph;
        data.adjlist = &adjlist;
        data.damping = damping;
        data.outdegree = &outdegree;
        data.indegree = &indegree;
        data.tmp = &tmp;
        data.reset = reset ? &normalized_reset : NULL;

        IGRAPH_CHECK(igraph_degree(graph, &outdegree, igraph_vss_all(),
                                   directed ? IGRAPH_OUT : IGRAPH_ALL, IGRAPH_LOOPS));
        IGRAPH_CHECK(igraph_degree(graph, &indegree, igraph_vss_all(),
                                   directed ? IGRAPH_IN : IGRAPH_ALL, IGRAPH_LOOPS));
        /* Set up an appropriate starting vector. We start from the in-degrees
         * plus some small random noise to avoid convergence problems */
        for (i = 0; i < options->n; i++) {
            if (VECTOR(indegree)[i]) {
                MATRIX(vectors, i, 0) = VECTOR(indegree)[i] + RNG_UNIF(-1e-4, 1e-4);
            } else {
                MATRIX(vectors, i, 0) = 1;
            }
        }

        IGRAPH_CHECK(igraph_adjlist_init(
            graph, &adjlist, dirmode, IGRAPH_LOOPS, IGRAPH_MULTIPLE
        ));
        IGRAPH_FINALLY(igraph_adjlist_destroy, &adjlist);

        /* here coherence instead of pagerank */
        //IGRAPH_CHECK(igraph_arpack_rnsolve(igraph_i_pagerank,  &data, options, 0, &values, &vectors));
        IGRAPH_CHECK(igraph_arpack_rnsolve(igraph_i_coherence,  &data, options, 0, &values, &vectors));

        igraph_adjlist_destroy(&adjlist);
        IGRAPH_FINALLY_CLEAN(1);

    //} 

    RNG_END();

    if (reset) {
        igraph_vector_destroy(&normalized_reset);
        IGRAPH_FINALLY_CLEAN(1);
    }

    igraph_vector_destroy(&tmp);
    igraph_vector_destroy(&outdegree);
    igraph_vector_destroy(&indegree);
    IGRAPH_FINALLY_CLEAN(3);

    if (value) {
        *value = MATRIX(values, 0, 0);
    }

    if (vector) {
        long int i;
        igraph_vit_t vit;
        long int nodes_to_calc;
        igraph_real_t sum = 0;

        for (i = 0; i < no_of_nodes; i++) {
            sum += MATRIX(vectors, i, 0);
        }

        IGRAPH_CHECK(igraph_vit_create(graph, vids, &vit));
        IGRAPH_FINALLY(igraph_vit_destroy, &vit);
        nodes_to_calc = IGRAPH_VIT_SIZE(vit);

        IGRAPH_CHECK(igraph_vector_resize(vector, nodes_to_calc));
        for (IGRAPH_VIT_RESET(vit), i = 0; !IGRAPH_VIT_END(vit);
             IGRAPH_VIT_NEXT(vit), i++) {
            VECTOR(*vector)[i] = MATRIX(vectors, (long int)IGRAPH_VIT_GET(vit), 0);
            VECTOR(*vector)[i] /= sum;
        }

        igraph_vit_destroy(&vit);
        IGRAPH_FINALLY_CLEAN(1);
    }

    if (options->info) {
        IGRAPH_WARNING("Non-zero return code from ARPACK routine!");
    }

    igraph_matrix_destroy(&vectors);
    igraph_matrix_destroy(&values);
    IGRAPH_FINALLY_CLEAN(2);

    return IGRAPH_SUCCESS;
}



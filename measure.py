"""

Refs:

    [Newman18] "Networks 2nd ed." M.E.J. Newman, OUP, 2018.
"""

import numpy as np
from network_tools.triangles import Triangles

# this if for grap_tool functions below
from graph_tool.all import *
import numpy 
import scipy

import networkx as nx

from scipy.sparse.linalg import eigs

def snn(A,k,s,is_sparse=False, is_directed=False):
    """ 
    Average nearest neighbors strength (ANNS) of a weighted undirected graph.

	NB: doesn't depend on a random ensemble. Can be used to compute
	snn_avg_UWCM, if A is replaced by P.

    $s^{nn}_i=\frac{ \sum_{j \neq i} a_{ij} s_j }{k_i}$, see [SG17] (3.28)
    
    TODO: 
    * add sparse
    * unit test
    
    Parameters:
    ---------------
    A: np.ndarray, shape=(n,n)
    the adjacency matrix
    
    k: np.ndarray, shape= (n,)
    the degree vector
    
    s: np.ndarray, shape= (n,)
    the strength vector
    
    Output:
    ---------
    snn: np.ndarray, shape= (n,)
    the ANNS vector
    
    """
    if is_directed: raise ValueError('undirected graph only')
    
    if not A.ndim==2: raise ValueError('A must be 2-dimensional')
    if not s.ndim==1: raise ValueError('s must be 1-dimensional')
    if not k.ndim==1: raise ValueError('k must be 1-dimensional')
	
    #sanity check
    if np.any( A.diagonal() ): 
        raise ValueError('nonzero diagonal')
        
    n = A.shape[0]    
    if is_sparse:
        raise NotImplementedError
    else:    
        #np.fill_diagonal(A,0.) # move outside of function
        #snn = np.zeros(n)
        snn = A.dot(s)
        idx_pos = snn>0
        snn[idx_pos] = snn[idx_pos]/k[idx_pos]  
        #snn = snn/k
        #snn[np.isnan(snn)] = 0.
    return snn


def snn_inin(A,k_in,s_in,is_sparse=False):
    """ 
    Directed Average nearest neighbors strength (ANNS) of a weighted undirected graph.

    $s^{nn, in/in}_i=\frac{ \sum_{u \neq i} a_{ui} s_in_u }{k_in_u}$, see ??
    
    TODO: 
    * add sparse
    * unit test
    
    Parameters:
    ---------------
    A: np.ndarray, shape=(n,n)
    the adjacency matrix
    
    k_in: np.ndarray, shape= (n,)
    the in-degree vector
    
    s_in: np.ndarray, shape= (n,)
    the in-strength vector
    
    Output:
    ---------
    snn: np.ndarray, shape= (n,)
    the ANNS vector
    
    """

    if not A.ndim==2: raise ValueError('A must be 2-dimensional')
    if not s_in.ndim==1: raise ValueError('s must be 1-dimensional')
    if not k_in.ndim==1: raise ValueError('k must be 1-dimensional')
	
    #sanity check
    if np.any( A.diagonal() ): 
        raise ValueError('nonzero diagonal')
        
    n = A.shape[0]    
    if is_sparse:
        raise NotImplementedError
    else:    
        #np.fill_diagonal(A,0.) # move outside of function
        #snn = np.zeros(n)
        snn = A.transpose().dot(s_in)
        idx_pos = snn>0
        snn[idx_pos] = snn[idx_pos]/k_in[idx_pos]  
        #snn = snn/k
        #snn[np.isnan(snn)] = 0.
    return snn


def snn_outout(A,k_out,s_out,is_sparse=False):
    """ 
    Directed Average nearest neighbors strength (ANNS) of a weighted undirected graph.

    $s^{nn, out/out}_i=\frac{ \sum_{j \neq i} a_{ij} s_out_i }{k_out_i}$, see ??
    
    TODO: 
    * add sparse
    * unit test
    
    Parameters:
    ---------------
    A: np.ndarray, shape=(n,n)
    the adjacency matrix
    
    k_out: np.ndarray, shape= (n,)
    the in-degree vector
    
    s_out: np.ndarray, shape= (n,)
    the in-strength vector
    
    Output:
    ---------
    snn: np.ndarray, shape= (n,)
    the ANNS vector
    
    """

    if not A.ndim==2: raise ValueError('A must be 2-dimensional')
    if not s_out.ndim==1: raise ValueError('s must be 1-dimensional')
    if not k_out.ndim==1: raise ValueError('k must be 1-dimensional')
	
    #sanity check
    if np.any( A.diagonal() ): 
        raise ValueError('nonzero diagonal')
        
    n = A.shape[0]    
    if is_sparse:
        raise NotImplementedError
    else:    
        #np.fill_diagonal(A,0.) # move outside of function
        #snn = np.zeros(n)
        snn = A.dot(s_out)
        idx_pos = snn>0
        snn[idx_pos] = snn[idx_pos]/k_out[idx_pos]  
        #snn = snn/k
        #snn[np.isnan(snn)] = 0.
    return snn


def local_clustering(A,k):
    """
    local clustering from sparse adjacency matrix of undirected graph
    
    cf formula: [Newman18] p.186
    """
    # TODO: check is sparse, check k is 1d np.ndarray, do division only for nonzero
    # TODO: add unit test, cmparison to igraph: https://igraph.org/python/doc/api/igraph._igraph.GraphBase.html#transitivity_local_undirected
    triangles = Triangles()
    triangles.fit_transform(A)
    ntriangles = np.array(triangles.n_triangles_local)
    return  2*ntriangles / (k*(k-1))





# BELOW: move to "measure.py"
#copied from https://git.skewed.de/count0/netzschleuder/-/blob/master/analyze.py
#by T.Peixoto

analyses = {}

def register(name=None, title=None, scale="linear", cont=False, meta=False):
    """Decorator that registers the function to the global analyses list, with a
    given name and title."""

    global analyses

    def reg(f):
        nonlocal name
        nonlocal meta
        if name is None:
            name = f.__name__.split(".")[-1]
        analyses[name] = dict(f=f, title=title, scale=scale, meta=meta,
                              cont=cont)
        return f

    return reg

@register("num_edges", "Number of edges", "log")
def get_E(g):
    return g.num_edges()

@register("num_vertices", "Number of vertices", "log")
#@cache_result
def get_N(g):
    return g.num_vertices()

@register("is_directed", title="Directed")
#@cache_result
def is_directed(g):
    return g.is_directed()

@register("average_degree", "Average degree", "log")
#@cache_result
def get_ak(g):
    if g.is_directed():
        return g.num_edges() / g.num_vertices()
    else:
        return 2 * g.num_edges() / g.num_vertices()

@register("degree_std_dev", "Degree standard deviation", "symlog")
#@cache_result
def get_kdev(g):
    g = GraphView(g, directed=False)
    k = g.get_out_degrees(g.get_vertices())
    return k.std()

@register("is_bipartite", "Bipartite")
#@cache_result
def is_bip(g):
    return is_bipartite(g)

@register("global_clustering", "Global clustering")
#@cache_result
def get_clustering(g):
    if is_bipartite(g):
        return 0.
    return global_clustering(g)[0]

@register("degree_assortativity", "Degree assortativity")
#@cache_result
def get_assortativity(g):
    g = GraphView(g, directed=False)
    return scalar_assortativity(g, "out")[0]

@register("largest_component_fraction", "Size of largest component")
#@cache_result
def get_S(g,directed=False):
    c = label_largest_component(g, directed=False)
    return c.fa.sum() / g.num_vertices()

@register("largest_component_fraction_dir", "Size of largest component dir")
#@cache_result
def get_SCC(g,directed=False):
    c = label_largest_component(g, directed=True)
    return c.fa.sum() / g.num_vertices()

def get_nb_component(g):
    comp, hist= label_components(g,directed=False)
    return hist.shape[0]

@register("small_component_size_avg", "Average size of small component")
#@cache_result
def get_s(g):
    comp, hist= label_components(g)
    hist.sort()
    hist = hist[:-1] #remove largest component
    count = np.bincount(np.array(hist,dtype=int))
    s_avg = count.dot( np.arange( count.shape[0] )  ) / hist.shape[0]
    return s_avg

@register("edge_reciprocity", "Edge reciprocity")
#@cache_result
def get_reciprocity(g):
    if g.is_directed():
        return edge_reciprocity(g)
    return 1.



@register("transition_gap", "Second eigenvalue of transition matrix")
#@cache_result
def get_tgap(g):
    g = GraphView(g, directed=False)
    u = extract_largest_component(g)
    if u.num_vertices() != g.num_vertices():
        g = u
    if 2 >= g.num_vertices() - 1:
        return numpy.nan
    T = transition(g, operator=True)
    k = numpy.array(g.degree_property_map("out").fa, dtype="float")
    k /= numpy.linalg.norm(k)
    def matvec(x):
        return T.matvec(x) - numpy.dot(k, x) * k
    Tp = scipy.sparse.linalg.LinearOperator(T.shape, matvec=matvec)
    try:
        ew = scipy.sparse.linalg.eigs(Tp, k=1, which="LR",
                                      return_eigenvectors=False,
                                      tol=1e-6)
    except scipy.sparse.linalg.eigen.arpack.ArpackNoConvergence:
        return numpy.nan
    return float(min(ew.real))

@register("mixing_time", "Random walk mixing time", "log")
#@uses(["transition_gap"])
#@cache_result
def get_mixing(g, tgap):
    if tgap <= 0:
        return numpy.inf
    return -1/numpy.log(tgap)

@register("hashimoto_radius", "Largest eigenvalue of non-backtracking matrix", "log")
#@cache_result
def get_hgap(g):
    g = GraphView(g, directed=False)
    l = label_parallel_edges(g, mark_only=True)
    if l.fa.max() > 0:
        l.a = numpy.logical_not(l.a)
        g = GraphView(g, efilt=l)
    else:
        del l
    T = hashimoto(g, compact=True, operator=True)
    try:
        ew = scipy.sparse.linalg.eigs(T, k=1, which="LR", return_eigenvectors=False)
    except (TypeError, scipy.sparse.linalg.eigen.arpack.ArpackNoConvergence):
        return numpy.nan
    return float(ew.real[0])

@register("diameter", "(Pseudo-) diameter", "log")
#@cache_result
def get_diameter(g):
    g = GraphView(g, directed=False)
    u = extract_largest_component(g)
    if u.num_vertices() != g.num_vertices():
        g = u
    if g.num_vertices() > 10000:
        d = pseudo_diameter(g)[0]
    else:
        d = max([shortest_distance(g, source=v).a.max() for v in g.vertices()])
    return int(d)

def get_cycle_count(g,unique=False, max_count=9999):
    """
    check how many cycles in the graph, ow many edges should be removed.
    """
    c= all_circuits(g,unique=unique)
    n_c=0
    for c_ in c: 
        n_c+=1 
        if n_c==max_count: break
    return n_c

def get_largest_eigv(g):
    """
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.linalg.eigs.html#scipy.sparse.linalg.eigs    
    """
    A = adjacency(g, operator=True)
    N = g.num_vertices()
    ew1 = eigs(A, k=1, which="LR", return_eigenvectors=False) #LR: largest real part
    return np.real(ew1)

def get_spectral_gap(g):
    """   
    algebraic connectivity, spectral gap
    
    see Newman18 §6.14.2 must be small "in order for a network to have a good visualization"
    
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.linalg.eigs.html#scipy.sparse.linalg.eigs
    
    NB: this is SLOW!!
    
    some REFS to check:
    moonen https://lirias.kuleuven.be/bitstream/123456789/370371/2/12-129.pdf
    https://arxiv.org/abs/1502.03049
    https://www.sciencedirect.com/science/article/abs/pii/S0016003215302040
    https://www.researchgate.net/profile/Alexandre-Reiffers-Masson/publication/344831777_Estimating_Fiedler_value_on_large_networks_based_on_random_walk_observations/links/5f92997092851c14bcdee51e/Estimating-Fiedler-value-on-large-networks-based-on-random-walk-observations.pdf
    
    https://homes.esat.kuleuven.be/~abertran/software.html
    https://code.google.com/archive/p/optimal-synchronization-bounds/
    """
    gu = GraphView(g, directed=False)
    L = laplacian(gu, operator=True)
    ew1 = eigs(L, k=2, which="SR", return_eigenvectors=False, sigma=0.01)    #SR: smallest real part
    return np.real(ew[1])-np.real(ew1[0])
    
#-------------------------------------------------------------------------------
#copied from https://github.com/carolinamattsson/local-connectivity-structure/blob/main/tutorial.ipynb

def bipartivity_approx(nx_network,evs=1,weight=None):
    # Convert into an adjacency matrix
    sp_network = nx.to_scipy_sparse_matrix(nx_network,weight=weight)
    # Initialize the output dictionary
    bp_approx = {}
    # Compute the top eigenvalues
    bp_approx["evs_A"] = list(scipy.sparse.linalg.eigsh(sp_network.asfptype(),k=2*evs,return_eigenvectors=False,which='BE'))
    # Get the approximation of the logit bipartivity from the top eigenvalues
    bp_approx["logit_bp"] = -1*min(bp_approx["evs_A"]) - max(bp_approx["evs_A"])
    # Report the approximations
    return bp_approx

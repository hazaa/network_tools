

from typing import Optional, Union

import numpy as np
from scipy import sparse
from scipy.spatial import cKDTree

from sknetwork.embedding.base import BaseEmbedding
from sknetwork.utils.check import check_format, is_symmetric, check_square
from sknetwork.utils.format import directed2undirected




class ForceAtlas(BaseEmbedding):
    """Force Atlas layout for displaying graphs.
    
    ADAPTED FROM scikit-networks by A.Hazan
    original code: https://scikit-network.readthedocs.io/en/latest/_modules/sknetwork/embedding/force_atlas.html
    MODIFICATION : y axis is kept fixed

    * Graphs
    * Digraphs

    Parameters
    ----------
    n_components : int
        Dimension of the graph layout.
    n_iter : int
        Number of iterations to update positions.
        If ``None``, use the value of self.n_iter.
    approx_radius : float
        If a positive value is provided, only the nodes within this distance a given node are used to compute
        the repulsive force.
    lin_log : bool
        If ``True``, use lin-log mode.
    gravity_factor : float
        Gravity force scaling constant.
    repulsive_factor : float
        Repulsive force scaling constant.
    tolerance : float
        Tolerance defined in the swinging constant.
    speed : float
        Speed constant.
    speed_max : float
        Constant used to impose constrain on speed.

    Attributes
    ----------
    embedding_ : np.ndarray
        Layout in given dimension.

    Example
    -------
    >>> from sknetwork.embedding.force_atlas import ForceAtlas
    >>> from sknetwork.data import karate_club
    >>> force_atlas = ForceAtlas()
    >>> adjacency = karate_club()
    >>> embedding = force_atlas.fit_transform(adjacency)
    >>> embedding.shape
    (34, 2)

    References
    ----------
    Jacomy M., Venturini T., Heymann S., Bastian M. (2014).
    `ForceAtlas2, a Continuous Graph Layout Algorithm for Handy Network Visualization Designed for the Gephi Software.
    <https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0098679>`_
    Plos One.
    """
    def __init__(self, n_components: int = 2, n_iter: int = 50, approx_radius: float = -1, lin_log: bool = False,
                 gravity_factor: float = 0.01, repulsive_factor: float = 0.1, tolerance: float = 0.1,
                 speed: float = 0.1, speed_max: float = 10):
        super(ForceAtlas, self).__init__()
        self.n_components = n_components
        self.n_iter = n_iter
        self.approx_radius = approx_radius
        self.lin_log = lin_log
        self.gravity_factor = gravity_factor
        self.repulsive_factor = repulsive_factor
        self.tolerance = tolerance
        self.speed = speed
        self.speed_max = speed_max


    def fit(self, adjacency: Union[sparse.csr_matrix, np.ndarray], pos_init: Optional[np.ndarray] = None,
            n_iter: Optional[int] = None) -> 'ForceAtlas':
        """Compute layout.

        Parameters
        ----------
        adjacency :
            Adjacency matrix of the graph, treated as undirected.
        pos_init :
            Position to start with. Random if not provided.
        n_iter : int
            Number of iterations to update positions.
            If ``None``, use the value of self.n_iter.

        Returns
        -------
        self: :class:`ForceAtlas`
        """
        # verify the format of the adjacency matrix
        adjacency = check_format(adjacency)
        check_square(adjacency)
        if not is_symmetric(adjacency):
            adjacency = directed2undirected(adjacency)
        n = adjacency.shape[0]

        # setting of the tolerance according to the size of the graph
        if n < 5000:
            tolerance = 0.1
        elif 5000 <= n < 50000:  # pragma: no cover
            tolerance = 1
        else:  # pragma: no cover
            tolerance = 10

        if n_iter is None:
            n_iter = self.n_iter

        # initial position of the nodes of the graph
        if pos_init is None:
            position: np.ndarray = np.random.randn(n, self.n_components)
        else:
            if pos_init.shape != (n, self.n_components):
                raise ValueError('The initial position does not have valid dimensions.')
            else:
                position = pos_init
        # compute the vector with the degree of each node
        degree: np.ndarray = adjacency.dot(np.ones(adjacency.shape[1])) + 1

        # initialization of variation of position of nodes
        resultants = np.zeros(n)
        delta: np.ndarray = np.zeros((n, self.n_components))
        swing_vector: np.ndarray = np.zeros(n)
        global_speed = 1

        for iteration in range(n_iter):
            delta *= 0
            global_swing = 0
            global_traction = 0

            if self.approx_radius > 0:
                tree = cKDTree(position)
            else:
                tree = None

            for i in range(n):

                # attraction
                indices = adjacency.indices[adjacency.indptr[i]:adjacency.indptr[i + 1]]
                attraction = position[i] - position[indices]

                if self.lin_log:
                    attraction = np.sign(attraction) * np.log(1 + np.abs(10 * attraction))
                attraction = attraction.sum(axis=0)

                # repulsion
                if tree is None:
                    neighbors = np.arange(n)
                else:
                    neighbors = tree.query_ball_point(position[i], self.approx_radius)

                grad: np.ndarray = (position[i] - position[neighbors])  # shape (n_neigh, n_components)
                distance: np.ndarray = np.linalg.norm(grad, axis=1)  # shape (n_neigh,)
                distance = np.where(distance < 0.01, 0.01, distance)
                repulsion = grad * (degree[neighbors] / distance)[:, np.newaxis]

                repulsion *= self.repulsive_factor * degree[i]
                repulsion = repulsion.sum(axis=0)

                # gravity
                gravity = self.gravity_factor * degree[i] * grad
                gravity = gravity.sum(axis=0)

                # forces resultant applied on node i for traction, swing and speed computation
                force = repulsion - attraction - gravity
                resultant_new: float = np.linalg.norm(force)
                resultant_old: float = resultants[i]

                swing_node: float = np.abs(resultant_new - resultant_old)  # force variation applied on node i
                swing_vector[i] = swing_node
                global_swing += (degree[i] + 1) * swing_node

                traction: float = np.abs(resultant_new + resultant_old) / 2  # traction force applied on node i
                global_traction += (degree[i] + 1) * traction

                node_speed = self.speed * global_speed / (1 + global_speed * np.sqrt(swing_node))
                if node_speed > self.speed_max / resultant_new:  # pragma: no cover
                    node_speed = self.speed_max / resultant_new

                delta[i]: np.ndarray = node_speed * force
                resultants[i] = resultant_new
                global_speed = tolerance * global_traction / global_swing

            delta[:,1]=0 #MODIFICATION IS HERE: y is kept fixed AH
            position += delta  # calculating displacement and final position of points after iteration
            if (swing_vector < 1).all():
                break  # if the swing of all nodes is zero, then convergence is reached and we break.

        self.embedding_ = position
        return self




def trophic_layout(G, 
                   k=None, 
                   ypos=None, 
                   iterations=50, 
                   seed=None,    
                   threshold=1e-4):
    ''' 
    ----------
    NB: this function is copied from Trophic Analysis Toolbox. 
    WARNING: NOT SCALABLE (copmutes interdistance array:  nnodes*nnodes*2)
    ----------
    This function position nodes in network G using modified Fruchterman-Reingold layout. 
    The layou spreads the nodes on the x-axis taking y-possitions as given. 
    By default the function uses for y-possitions trophic levels as defined in [1], 
    but may also be passed any y-possitions user chooses to define.
    
    REQUIRED INPUTS:
    G   : networkx graph object.
         Positions will be assigned to every node in G.
    
    OPTIONAL INPUTS
    k   : integer or None (default=None). If None the distance is set to 1/sqrt(nnodes) 
        where nnodes is the number of nodes.  Increase this value to spread nodes farther apart on x-axis .
    ypos: array or None (default=None). Initial y-positions for nodes. If None, then use
        trophic levels as defined by [1]. Alternatively user can pass any desired possitions as ypos. 
    iterations : integer (default=50)
        Maximum number of iterations taken.
    seed : integer, tuple, or None (default=None). For reproducible layouts, specify seed for random number generator.
           This can be an integer; or the output seedState obtained from a previous run of trophic_layout or trophic_plot in order to reporduce
           the same layout result.
           If you run:
               pos1, seedState = trophic_layout(G)
               pos2, _ = trophic_layout(G,seed=seedState)
            then pos1==pos2
    threshold: float (default = 1e-4)
        Threshold for relative error in node position changes.
        The iteration stops if the error is below this threshold.
    
    OUTPUTS
        pos a dictionary of possitions for each node in G
        seedState : (tuple) the seedState needed to reproduce layout obtained (e.g. if you run:
                        pos1, seedState = trophic_layout(G)
                        pos2, _ = trophic_layout(G,seed=seedState)
                  then pos2==pos1
    
    '''
    
    if seed is None or isinstance(seed,int):
        np.random.seed(seed) # sets seed using default (None) or user specified (int) seed
        seedState = np.random.get_state() # saves seed state to be returned (for reproducibility of result)
    elif isinstance(seed,tuple): # allows user to pass seedState obtained from previous run to reproduce same layout
        np.random.seed(None)
        np.random.set_state(seed)
        seedState=seed
    else:
        msg = '"Seed should be None (default); integer or tuple (use seedState which is output of trophic_layout).'
        raise ValueError(msg)
        
    import networkx as nx
    
    # Check networkx graph object
    if not isinstance(G, nx.Graph):
        msg='This function takes networkx graph object'
        raise ValueError(msg)
    
    # Check network weakly connected
    G2 = G.to_undirected(reciprocal=False,as_view=True)
    if not nx.is_connected(G2):
        msg='Network must be weakly connected'
        raise ValueError(msg)
        
    A = nx.to_numpy_array(G)
    dim=2
    nnodes, _ = A.shape
    
    A=A+np.transpose(A) # symmetrise for layout algorithm

    if ypos is None:
        h=trophic_levels(G)
        pos = np.asarray(np.random.rand(nnodes, dim), dtype=A.dtype)
        pos[:,1]=h
        # random initial positions
        #pos = np.asarray(seed.rand(nnodes, dim), dtype=A.dtype)
    else:
        # pos = np.asarray(seed.rand(nnodes, dim), dtype=A.dtype)
        pos = np.asarray(np.random.rand(nnodes, dim), dtype=A.dtype)
        pos[:,1]=ypos
        # make sure positions are of same type as matrix
        pos = pos.astype(A.dtype)
        
    # optimal distance between nodes
    if k is None:
        k = np.sqrt(1.0 / nnodes)
    
    # the initial "temperature"  is about .1 of domain area
    # this is the largest step allowed in the dynamics.
    t = max(max(pos.T[0]) - min(pos.T[0]), max(pos.T[1]) - min(pos.T[1])) * 0.1
    
    # simple cooling scheme.
    # linearly step down by dt on each iteration so last iteration is size dt.
    dt = t / float(iterations + 1)
    
    for iteration in range(iterations):
        
        # matrix of difference between points
        delta = pos[:, np.newaxis, :] - pos[np.newaxis, :, :]
           # This is nnodes*nnodes*2 array giving delta_ij1=x_i-x_j delta_ij2=y_i-y_j
        
        # distance between points 
        distance = np.linalg.norm(delta, axis=-1) 
             # This is the nnodes*nnodes euclidian distance matrix with elements
             # d_ij=sqrt((x_i-x_j)^2+(y_i-y_j)^2)
                
        # enforce minimum distance of 0.01
        np.clip(distance, 0.01, None, out=distance)
        
        # displacement "force"
        displacement = np.einsum('ijk,ij->ik',
                                 delta,
                                 (k * k / distance**2 - A * distance / k))
        
        # update positions
        length = np.linalg.norm(displacement, axis=-1) # returns the Euclidean norm of each row in displacement (this norm is also called the 2-norm, or Euclidean LENGTH).
        length = np.where(length < 0.01, 0.01, length)  # enforce: where length<0.01, replace with 0.01, otherwise keep length
        delta_pos = np.einsum('ij,i->ij', displacement, t / length)
        delta_pos[:,1]=0 
        pos += delta_pos 
        
        # cool temperature
        t -= dt
        
        # check convergence
        err = np.linalg.norm(delta_pos) / nnodes
        if err < threshold:
            break
            
    pos = dict(zip(G, pos))
    return pos, seedState

int igraph_i_rewire(igraph_t *graph, igraph_integer_t n, igraph_rewiring_t mode, igraph_bool_t use_adjlist);
int igraph_i_rewire_q(igraph_t *graph, igraph_vector_t *s , igraph_real_t T, igraph_integer_t n, igraph_rewiring_t mode, igraph_bool_t use_adjlist);
int adjlist_dist(igraph_adjlist_t *al0,igraph_adjlist_t *al1, igraph_neimode_t mode );
//float trophic_level(igraph_t *graph,igraph_vector_t *s);
/*
int trophic_level (const igraph_t *graph, igraph_vector_t *vector,
                                                 igraph_real_t *value, const igraph_vs_t vids,
                                                 igraph_bool_t directed, igraph_real_t damping,
                                                 const igraph_vector_t *reset,
                                                 igraph_arpack_options_t *options) ;*/
int trophic_level(const igraph_t *graph, igraph_vector_t *vector);                                                 
int trophic_level_difference(const igraph_t *graph, const igraph_vector_t *s, igraph_vector_t *x);
int moments(int n_edge, igraph_vector_t *x, igraph_real_t *mean, igraph_real_t *std);
int igraph_vector_difference_sorted_first_nodes(igraph_t *graph);
//igraph_i_rewire_q_loop(igraph_t *g, int n_iter, int n_rewire, int n_update_s, float T, int seed );
int igraph_i_rewire_q_loop(igraph_t *graph, int n_iter, int n_rewire, int n_update_s, int max_iter_gsl, float Temp, int seed,igraph_vector_t *out_q,igraph_vector_t *out_swap,igraph_vector_t *out_dist  );
//igraph_i_rewire_q_loop2(igraph_t *g, int n_iter, int n_rewire, int n_update_s, float T, int seed );

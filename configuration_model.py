"""
Compute various theoretical properties of the configuration model

REFS:

[Newman18] MEJ Newman "Networks" 2nd ed., 2018, OUP.
"""

import numpy as np

def has_giant_component(k):
    """
    check the Molloy-Reed criterion
    <k^2>-2<k> >0
    undirected case only
    cf [Newman 18] eq.(12.24)
    """
    return np.mean(k**2)-2*np.mean(k)  >0
    
  
def small_component_size_no_giant(k):
    """
    Average size of small components WHEN THERE IS NO GIANT COMPONENT.
    Undirected case only
    $\langle s \rangle =  1+ \frac{\langle k^2 \rangle}{ 2 \langle k \rangle -\langle k^2 \rangle}$. [Newman 18] eq.(12.56)
    """
    k_avg = np.mean(k)
    k2_avg = np.mean(k**2)
    return 1+ (k2_avg)/(2*k_avg-k2_avg)  

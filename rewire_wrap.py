"""
    WRAPPER of rewire.c USING CTYPES  and PYTHON-IGRAPH-CTYPES
    https://docs.python.org/3/library/ctypes.html?highlight=ctypes
    https://github.com/igraph/python-igraph-ctypes
  

    WARNING!! : this module uses libigraph.so, a shared library compiled for git, 
    which resides in a harcoded directory, and which may differ from the
    python-loaded library loaded when importing igraph module.
    
    COMPILE SHARED LIBRARY  librewire.so:  
    $gcc -fPIC rewire.c  `pkg-config  --libs --cflags igraph` -c
    $gcc -shared -o librewire.so rewire.o -lgsl -ligraph -L/home/aurelien/local/git/extern/igraph/build/src

    LOAD MODULE
    $export LD_LIBRARY_PATH=/home/aurelien/local/git/extern/igraph/build/src

    >>>from ctypes import *
    >>>librewire = cdll.LoadLibrary("/home/aurelien/local/git/network_tools/librewire.so")
    >>>from igraph_ctypes.constructors import create_empty_graph  # NB: this one calls libigraph.so !!!!!!!!!!!!!
    >>>g = create_empty_graph(10)

    >>>librewire.igraph_vector_difference_sorted_first_nodes(igraph_p) #FAILS
    >>>librewire.igraph_vector_difference_sorted_first_nodes(g._as_parameter_()) #OK!!!!!
"""

"""
-------
TODO
* CREATE new  igraph_ctypes.graph.Graph from edgelist. cf /tests :
    g = create_empty_graph(n)
    assert g.ecount() == 0
    g.add_edges((i, (i + 1) % n) for i in range(n))
* GET EDGELIST from igraph_ctypes.graph.Graph  ; make gt object with it
* do the same with:    igraph_i_rewire_q    (Q: how to pass argument niter ?)
* checks:  q is almost constant ; distance from original graph is large ; degree seq is conserved
* finally: try with agribalyse graph

------
CRASH
import igraph as ig
g = ig.Graph.Erdos_Renyi(20,0.1)
from ctypes import POINTER
librewire.igraph_vector_difference_sorted_first_nodes(g._raw_pointer())  #CRASH
"""
import igraph as ig    

from ctypes import *

from igraph_ctypes.constructors import create_empty_graph    
from igraph_ctypes._internal.types import  igraph_integer_t,igraph_real_t
from igraph_ctypes._internal.lib import igraph_vector_push_back   
from igraph_ctypes._internal.wrappers import _Vector   
from igraph_ctypes._internal.lib import _lib

librewire = cdll.LoadLibrary("/home/aurelien/local/git/network_tools/librewire.so")
   
   
def test_rewire_q_loop_ctypes_ig():
    """
    """
    #g=ig.Graph(directed=True)
    #g.add_vertices([0,1,2,3,4])
    #g.add_edges([(0,2),(0,3), (1,3),(1,4)])
    g = ig.Graph.Barabasi(10000,2,directed=True)
    
    g_ctypes = convert_to_ctypes_ig(g)    
    #
    niter= 1000
    n_rewire=10
    n_update_s= 100
    max_iter_gsl = 1000
    T=0.0001
    seed=42
    r = rewire_q_loop_ctype_ig(g_ctypes,niter,n_rewire,n_update_s,max_iter_gsl,T, seed)
    r    
   
def test_read_vector_real():
    """
    """
    read_vector_real = librewire.read_vector_real
    read_vector_real.restype = c_double
    
    c_vector = _Vector.create(0)
    [igraph_vector_push_back(c_vector, igraph_real_t(i))   for i in range(10)]
    for i in range(10):
        print(  read_vector_real(c_vector,i)  )
        
           
   
def test_edgelist_from_ctypes_ig():
    """
    """
    g=ig.Graph(directed=True)
    g.add_vertices([0,1,2,3,4])
    edgelist_init = [(0,2),(0,3), (1,3),(1,4)]
    g.add_edges(edgelist_init)
    
    g_ctypes = convert_to_ctypes_ig(g)    
    edgelist_final = edgelist_from_ctypes_ig(g_ctypes)   
    
    assert edgelist_init==edgelist_final
   
"""def test_read_edge():
    #    very ugly but seems to work
    g=ig.Graph(directed=True)
    g.add_vertices([0,1,2,3,4])
    g.add_edges([(0,2),(0,3), (1,3),(1,4)])
    
    g_ctypes = convert_to_ctypes_ig(g)
    
    for i in range( 2*g_ctypes.ecount() ):
        a = librewire.read_edge_vector(g_ctypes._as_parameter_() , igraph_integer_t(i)) 
        print(a)
"""

def test_copy_edge():
    """
    copy_edge(igraph_t *graph, igraph_vector_t *from, igraph_vector_t *to)
    
    """   

    g=ig.Graph(directed=True)
    g.add_vertices([0,1,2,3,4])
    g.add_edges([(0,2),(0,3), (1,3),(1,4)])
    
    g_ctypes = convert_to_ctypes_ig(g)
    
    c_vector = _Vector.create(0)

    librewire.copy_edge(g_ctypes._as_parameter_(), c_vector)
    edgelist = []
    for i in range( 2*g_ctypes.ecount() ):
        e = librewire.read_vector(c_vector,i)
        print(e)

       
"""   
def test_copy_edge():
    #copy_edge(igraph_t *graph, igraph_vector_t *from, igraph_vector_t *to)
    #
    FAILS!!!

    raise NotImplementedError
    
    g=ig.Graph(directed=True)
    g.add_vertices([0,1,2,3,4])
    g.add_edges([(0,2),(0,3), (1,3),(1,4)])
    
    g_ctypes = convert_to_ctypes_ig(g)
    
    c_from = _Vector.create(0)
    c_to = _Vector.create(0)
    
    librewire.copy_edge(g_ctypes._as_parameter_(), c_from, c_to)
    edgelist = []
    for i in range( g_ctypes.ecount() ):
        from_ = _lib.igraph_vector_pop_back(c_from)
        to_ = _lib.igraph_vector_pop_back(c_to)
        edgelist.append((from_,to_))
"""
   
   
def test_rewire_q_ctypes_ig():
    """
    """
    #g=ig.Graph(directed=True)
    #g.add_vertices([0,1,2,3,4])
    #g.add_edges([(0,2),(0,3), (1,3),(1,4)])
    g = ig.Graph.Barabasi(1000,2,directed=True)
    
    g_ctypes = convert_to_ctypes_ig(g)
    #
    T=0.001
    s = g.pagerank()
    n_rewire=1000
    num_successful_swaps = rewire_q_ctypes_ig(g_ctypes,n_rewire,T,s)
    print(num_successful_swaps)
    
 
def convert_to_ctypes_ig(g):
    """
    convert an ig.Graph object, created by the official wrapper, to
    an object created by the ctypes wrapper
    """
    g_ctypes = create_empty_graph(g.vcount(), g.is_directed() )    
    g_ctypes.add_edges((e.source, e.target) for e in g.es() )
    return g_ctypes

def edgelist_from_ctypes_ig(g_ctypes):
    """
    
    from_: g_ctypes._instance._Boxed__c_instance._fields_[2][1]
    to: g_ctypes._instance._Boxed__c_instance._fields_[3][1]
    """
    c_vector = _Vector.create(0)
    librewire.copy_edge(g_ctypes._as_parameter_(), c_vector)
    
    edgelist = []
    for i in range( g_ctypes.ecount() ):
        src =   librewire.read_vector(c_vector,2*i)
        tgt =   librewire.read_vector(c_vector,2*i+1)
        edgelist.append((src,tgt))
    return edgelist
    
def edgelist_from_ctypes_ig_DEPRECATED(g_ctypes):
    """
    
    from_: g_ctypes._instance._Boxed__c_instance._fields_[2][1]
    to: g_ctypes._instance._Boxed__c_instance._fields_[3][1]
    """
    raise DeprecationWarning
    
    edgelist = []
    for i in range( g_ctypes.ecount() ):
        src = librewire.read_edge_vector(g_ctypes._as_parameter_() , igraph_integer_t(2*i)) 
        tgt = librewire.read_edge_vector(g_ctypes._as_parameter_() , igraph_integer_t(2*i+1)) 
        edgelist.append((src,tgt))
    return edgelist
    
    
def rewire_q_ctypes_ig(g_ctypes,n_rewire,T,s):
    """
    """
    c_T=igraph_real_t(T)
    c_n_rewire= igraph_integer_t(n_rewire)
    IGRAPH_REWIRING_SIMPLE = igraph_integer_t(0)
    c_s = _Vector.create(0)
    [igraph_vector_push_back(c_s, igraph_real_t(s_i))   for s_i in s]
    nswap= librewire.igraph_i_rewire_q(g_ctypes._as_parameter_(), c_s , c_T, c_n_rewire, IGRAPH_REWIRING_SIMPLE)
    c_s._destroy()
    return nswap
    
def rewire_q_loop_ctype_ig(g_ctypes,n_iter,n_rewire,n_update_s,max_iter_gsl,T, seed=0) :
    """
    peform a rewire/trophic level update loop
    """

    c_n_iter = igraph_integer_t(n_iter)
    c_n_rewire= igraph_integer_t(n_rewire)
    c_n_update_s= igraph_integer_t(n_update_s)
    c_max_iter_gsl = igraph_integer_t(max_iter_gsl)
    c_seed= igraph_integer_t(seed)
    c_T=igraph_real_t(T)
    c_q_out = igraph_real_t(0.)
    
    c_q = _Vector.create(0)
    c_swap = _Vector.create(0)
    c_dist = _Vector.create(0)
    #rewire
    librewire.igraph_i_rewire_q_loop(g_ctypes._as_parameter_(),
                                        c_n_iter, c_n_rewire, c_n_update_s,c_max_iter_gsl, c_T, c_seed,c_q,c_swap,c_dist )
    #compute vector size
    size = _lib.igraph_vector_size(c_q)
    r = {'q':[], 'nswap':[],'dist':[]}
    
    #https://docs.python.org/3/library/ctypes.html#return-types
    read_vector_real = librewire.read_vector_real
    read_vector_real.restype = c_double
        
    #copy vector content
    for i in range(size):
        #librewire.read_vector_real(c_q,i , byref(c_q_out)) #FAILS
        c_q_out = read_vector_real(c_q,i)
        r['q'].append(  c_q_out )
        r['nswap'].append( librewire.read_vector(c_swap,i) )
        r['dist'].append( librewire.read_vector(c_dist,i) )
    return r

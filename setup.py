from distutils.core import setup,Extension
#from Cython.Build import cythonize
from Cython.Distutils import build_ext
"""

$python setup.py build_ext --inplace

example: https://pastebin.com/cMw7fgBC
http://docs.cython.org/en/latest/src/quickstart/build.html
http://docs.cython.org/en/latest/src/userguide/source_files_and_compilation.html#compilation

"""

module1 = Extension('rewire_wrap',
                    define_macros = [('MAJOR_VERSION', '1'),
                                     ('MINOR_VERSION', '0')],
                    include_dirs = ['/usr/local/include/igraph'],
                    libraries = ['igraph'],
                    library_dirs = ['/home/aurelien/local/git/extern/igraph/build/src'],
                    sources = ["rewire_wrap.pyx",'rewire.c'],
                     extra_compile_args=["-fPIC"]
			)

setup(
  name = 'rewire_wrap',
  #ext_modules = cythonize("mtrand.pyx",'distributions.c'),
  #ext_modules = cythonize(["randkit.pyx",'distributions.c','randomkit.c']),
  ext_modules = [module1],
  cmdclass={'build_ext': build_ext}
)

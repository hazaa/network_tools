import  numpy as  np
import pandas as pd
import graph_tool.all as gt
import matplotlib.pyplot as plt
import networkx as nx
from functools import partial
plt.rcParams['font.size']=20

from numba import jit

from scipy.special import factorial 

from scipy.optimize import root

"""

Refs:
[NSW01] Newman, Strogatz, Watts  http://arXiv.org/abs/cond-mat/0007235v2
[Serafini 18] An iterative scheme to compute size probabilities in random graphs and branching processes
[Newman18] MEJ Newman "Networks" 2nd ed., 2018, OUP.

see epidemic "final size" pdf of epidemy
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6326237/ https://github.com/joelmiller/Invasion_PGF

sparse polynomials:
http://www.bpaslib.org/index.html

"""

"""
* GF==z-transform (in book by cohen/havlin; abelian/tauberian)
* python+GF: dobson book chapter 6: python code
 epydemic: https://simondobson.org/blog/2021/06/10/generating-functions-for-epydemic/
 examples ???
* GF+code: wilf's book about GF: Maple/Mathematica material 
* sage+GF ?
* sage+network
* serafini recursion
"""


def test_size_giant_component():
    """
    cf [Newman18] §12.6.1 p.387
    """
    def giant_component_example_Newman(p):
        # see [Newman18] eq.(12.41)
        u = p[1]/(3*p[3])
        S = 1- (p[0] + p[1]*u + p[2]*u**2 + p[3]*u**3)
        return S
    #p = np.array([0.5,0.3,0.15,0.05])
    a=0.8; p= np.array( [(1-a)*a**k for k in [0,1,2,3]] )
    d = get_avg_degree(p)
    q = get_q(p,d)
    S,success,u = size_giant_component(p)
    assert success
    #check d==z
    #expected: cf [Newman 18] eq.(12.41)
    assert np.isclose(S , giant_component_example_Newman(p))
    """
    Comment: OK, works fine
    """


def size_giant_component(p):
    """
    Compute the size of the giant component in a configuration model.
    Undirected monopartite case
    cf [Newman18] eq.(12.27), (12.30)
    """    
    S=0
    #get g0
    G0 = np.polynomial.Polynomial(p)
    dG0 =  G0.deriv()        
    z = dG0(1)   
    #get g1   
    G1 = 1./z * dG0    
    #solve u=g1(u)
    func = lambda x: G1(x)-x
    sol = root(func, 0.5)
    u = sol.x[0]
    #check
    #compute S=1-g0(u)
    S = 1-G0(u)
    return S,sol.success,u
        
def size_small_component(p,u):
    """
    Compute the size of the giant component in a configuration model.
    Undirected monopartite case
    cf [Newman18] eq.(12.53)
    """
    s=0
    #get g0
    G0 = np.polynomial.Polynomial(p)
    dG0 =  G0.deriv()        
    z = dG0(1)   
    #get g1   
    G1 = 1./z * dG0   
    dG1 =  G1.deriv()  
    #
    s = 1+ ( u**2 * dG0(1) )/(  G0(u) *(1-dG1(u)) )
    return s

@jit(nopython=True)
def sample_k(max=100):
    """
    used by gt.random_graph
    """
    accept = False
    while not accept:
        k = np.random.randint(1,max+1)
        accept = np.random.random() < 1.0/k
    return k

def test_component_size_distribution_compare():
    """
    """
    #suppose a degree distribution pk
    # (eg power law with cutoff, NSW01 eq.17)
    p = [1/i for i in range(1,100)]
    p.insert(0,0) 
    #node degree sampler
    sample_k_40 = partial(sample_k,40)    
    #call
    component_size_distrib_unipartite_undir_compare(p,sample_k_40, trunc=100,niter_fixed_point=10,
                                                                                    n_node = 100000, n_sample = 20,
                                                                                    plot=True)    
def get_avg_degree(p):
    """
    get average degree from degree distribution
    """
    n=p.shape[0]
    d = p.dot( np.arange(n))
    return d


def get_avg_degree_2d(p):
    """
    get average degree from 2D generating function
    
    $z = \frac{\partial{ \mathcal{G}} }{\partial{x}}|_{x,y=1}= \frac{\partial{ \mathcal{G}} }{\partial{y}}|_{x,y=1}
    $
    
    Parameters:
    -----------------
    p: array, shape(n,m)
    pjk the joint distribution of degree
    """
    n = p.shape[1]
    #z =np.sum( p.transpose().dot( np.arange(m)))
    z =np.sum( p.dot( np.arange(n)))
    return z


def get_p(k):
    """
    compute degree probability from degree sequence
    """
    pk=np.bincount(k)
    pk=pk/pk.sum() 
    return pk

def get_q(p,d):
    """
    get excess degree distribution from degree distribution
    
    Parameters:
    ----------------
    p: array
    degree distribution
    
    d: float:
    average degree
    """
    n = p.shape[0]
    q = p[1:]*range(1,n)/d
    return q

def test_get_2D_pdf_from_degree_sample():
    """
    """
    deg_list = np.array([[1, 2],  [1, 2],   [1, 2],  [0, 0]])
    pjk = get_2D_pdf_from_degree_sample(deg_list)

def get_2D_pdf_from_degree_sample(degree_list):
    """
    
    Parameters:
    ----------------
    degree_list: list
    [(in,out),(in,out),...]
    
    https://stackoverflow.com/questions/36863404/accumulate-constant-value-in-numpy-array
    """
    degree_list = np.array(degree_list)
    max_i= degree_list[:,0].max()
    max_j= degree_list[:,1].max()
    # count
    out_shape = (max_i+1,max_j+1) # Input param
    ids = np.ravel_multi_index(degree_list.T,out_shape)
    out =np.bincount(ids,minlength=np.prod(out_shape)).reshape(out_shape)
    #normalize
    out = out/out.sum()
    return out


def out_component_size_distribution_unipartite_directed():
    """
    FAIL
    
    use p_k^in=p_k^out  
    
    see [NSW01] Fig.6 "the distribution P_s of the number of vertices...with identically 
                        exponentially distributed in/out degree" 
    NB: this one is directed.
    """
    kappa_l = np.linspace(0.8,1,5)#[0.5,0.8,1.45]
    kmax = 1000 #
    maxn = 1000
    r_l=[]
    s_l=[]
    for kappa in kappa_l:
        # pk=exponential (cf NSW01 eq (13) )
        # because 
        p = [(1-np.exp(-1/kappa) )*np.exp(-k/kappa) for k in range(kmax)]
        #!!!!!  since in/out degree is identically distributed, solving eq 62 
        # and H1=yG1(H1) and H0 = yG0(H1) is done from p directly
        p=np.array(p) 
        n = p.shape[0]
        d = get_avg_degree(p)
        q = get_q(p,d)
        r,a = recursion_serafini_mathematica_r(q,maxn)
        s=recursion_serafini_mathematica_s(p,a,maxn)
        r_l.append(r)
        s_l.append(s)
    
    #plot
    fig,ax = plt.subplots(1,1)
    for kappa,s in zip(kappa_l, s_l):
        ax.loglog(range(0,len(s) ) ,  s, label=str(kappa))
    fig.legend()
    """
    Comment:  FAILURE !!!!!  we don't find the same critical value. (0.9 here, vs 1.44..)
    """
    # compuke r_k, s_k
    # compare r_k theor to empirical (NEVER TRIED !!!)
    # compare s_k theor to empirical
    # check s_k critical behavior when kappa increases: kappa=0.5,0.8,1.44
    #     see [NSW01]  Fig. 6 
    
def pi_s_exponential(s,a):
    """
    $\pi_s$ "the probability that a randomly chosen node belongs to a small (non-giant)
    component of size $s$" ([Newman 18] p.411)
    
    p_k= (1-a) * a^k   k=0,...   
    
    [Newman 18] eq. (12.137), page 416
    """
    return ((factorial(3*s-3))/(factorial(s-1)*factorial(2*s-1)) ) *(a**(s-1))*((1-a)**(2*s-1))


def test_exponential_degree_distribution():
    """
    """
    #check RNG for geometric
    #  f(k) = (1-p)^(k-1)*p   k=1,... 
    n_samp = 1000000
    p=0.7
    sample_geo = np.random.geometric(p,n_samp)
    fig,ax = plt.subplots(1,1)
    #count = np.bincount(sample_geo)
    kmax=count.shape[0]
    ax.plot( range(0,kmax),count/n_samp, label='count' )
    k_ = np.arange(1,kmax)
    ax.plot(k_, (1-p)**(k_-1) *p ,label='pdf theor' )
    ax.legend()
    #check RNG for exponential
    #  pk= (1-a) * a^k   k=0,...      
    n_samp = 1000000
    a=0.3
    sample_geo = np.random.geometric(1-a,n_samp)-1 #Note the "-1" here
    fig,ax = plt.subplots(1,1)
    count = np.bincount(sample_geo)
    kmax=count.shape[0]
    ax.plot( range(0,kmax),count/n_samp, label='count' )
    k_ = np.arange(0,kmax)
    ax.plot(k_, (1-a)*a**k_ ,label='pdf theor' )
    ax.legend()
    #check that node degree is exponentially distributed
    a=0.3
    n_node = 1000000  
    g = gt.random_graph(n_node, lambda: np.random.geometric(1-a)-1, verbose=True,
                                            directed=False,parallel_edges=True,self_loops=True,
                                             n_iter=1)  
    vp_deg = g.degree_property_map('total')
    k = np.array(vp_deg.a)
    count = np.bincount(k)
    fig,ax = plt.subplots(1,1)
    kmax=count.shape[0]
    ax.plot( range(0,kmax),count/n_node, label='count' )
    k_ = np.arange(0,kmax)
    ax.plot(k_, (1-a)*a**(k_) ,label='pdf theor' )
    ax.legend()    

    
def get_pi_s_fom_component_size_hist(hist,n_node):
    """
    compute the estimation of "the probability that a randomly chosen node belongs to a small (non-giant)
    component of size $s$" ([Newman 18] p.411)
    
    Parameters:
    ------------------
    hist : array or list
    hist is returned by gt.label_component(): [size_component_0, size_component_1, ...]
    
    Output:
    -----------
    
    """
    #remove largest component
    hist.sort()
    hist = hist[:-1]
    #get component size histogram
    count = np.bincount(np.array(hist,dtype=int))
    x = np.arange( count.shape[0] )
    pi_s_empirical = count*x/(n_node)   # nb of nodes in compo of size s =  nb_of_components_of_size_s * s_nodes_per_component 
    return pi_s_empirical



def component_size_distrib_unipartite_undirected_compare_EXPONENTIAL(a=0.3, smax=50,
                                                                                    n_node = 100000, n_sample = 1,
                                                                                    backend='gt',
                                                                                    plot=True,
                                                                                    debug=False):
    """
    Compare theorical vs empirical $\pi_s$ "the probability that a randomly chosen node belongs to a small (non-giant)
    component of size $s$" ([Newman 18] p.411)
    
    [Newman 18] eq. (12.137), page 416, gives an analytical expression for  $\pi_s$ in the undirected case 
    for an EXPONENTIAL random graph. 
    
    Ref: GEOMETRIC RNG:
    Newman18 pk=(1-a)a^k, k=0...
    https://numpy.org/doc/stable/reference/random/generated/numpy.random.RandomState.geometric.html#numpy.random.RandomState.geometric
    https://numpy.org/doc/stable/reference/random/generated/numpy.random.Generator.geometric.html#numpy.random.Generator.geometric
    k=1,...            f(k) = (1-p)^(k-1)*p
    
    NB: no cutoff in this one (compare to [NSW01])
    """                                                                                
    #theoretical
    pi_s = []
    for s in range(1,smax):
        pi_s.append( pi_s_exponential(s,a) )
    #plot
    fig,ax = plt.subplots(1,1)
    ax.semilogy(range(1,smax),  pi_s, label=str(a))
    fig.legend()
   
    #sample graphs
    n_node = 1000000  
    for i in range(n_sample): 
        if backend=='gt': # with gt
            g = gt.random_graph(n_node, lambda: np.random.geometric(1-a)-1, verbose=True,
                                                directed=False,parallel_edges=True,self_loops=True,
                                                n_iter=1)  
            comp, hist= gt.label_components(g)   
        elif backend=='igraph_c': # igraph C library
        # first compile component_size.c; then run:
        # $./component_size > data/component_size.txt
        # TODO: direct call from python  (sys);   pass a argument to binary
        #then in python
            hist=np.fromfile('data/component_size.txt',sep=" ",dtype='int')
            n_node = 10000000
        else: raise ValueError('unknown backend')    
        #plot
        pi_s_empirical  = get_pi_s_fom_component_size_hist(hist,n_node)
        x = np.arange( pi_s_empirical.shape[0] )
        ax.semilogy( x, pi_s_empirical)
   #COMPARE WITH SERAFINI
    maxn=50
    k_l = list(range(maxn))
    p = [(1-a)*a**k for k in k_l ]
    p=np.array(p) 
    n = p.shape[0]
    d = get_avg_degree(p)
    q = get_q(p,d)
    r,aa = recursion_serafini_mathematica_r(q,maxn)
    s=recursion_serafini_mathematica_s(p,aa,maxn)
    ax.semilogy(range(0,len(s) ) ,  s)
    """
    Comment: 
    * Newman vs sampling: OK. 1E7 nodes is necessary to get a good fit, as in [Newman 18] Fig.12.8 p.417
                    igraph c is necessary to reach this size fastly (gt is slow and uses too much memory)
    * NB $\pi_s$ is "the probability that a randomly chosen node belongs to a small (non-giant)
    component of size $s$" ([Newman 18] p.411)
    * Serafini vs Newman: OK.
    
    * ig python FAILS TO REALIZE DEGREE SEQUENCE :
        else: #with ig
            #https://igraph.org/python/api/latest/igraph._igraph.GraphBase.html#Degree_Sequence
            #https://igraph.org/python/api/latest/igraph.Graph.html#clusters
            out = np.random.geometric(1-a, n_node) 
            g_ig = ig.Graph(directed=False) 
            g_ig.Degree_Sequence(out.tolist(), method='simple')
            if g_ig.vcount()==0: raise ValueError('could not generate')
            g_ig.summary()
            vc = g_ig.clusters(mode='weak')
            vc.membership
            #g_ig.Realize_Degree_Sequence(out, in_=None, allowed_edge_types='all') 
            h= vc.size_histogram()
            # FAILS TO REALIZE DEGREE SEQUENCE

    """


def component_size_distrib_unipartite_undirected_compare(p,sample_k,trunc=100,niter_fixed_point=30,
                                                                                    n_node = 100000, n_sample = 50,
                                                                                    plot=True):
    """
    compute theoretical component size distribution of undirected network using [NSW01].
    compare to numerical simulation
    """
    # ---theoretical component_size_distribution
    """
    #compute G0,G1 
    G0 = np.polynomial.Polynomial(pk)
    dG0=  G0.deriv()        
    z1 =dG0(1)
    G1 = 1./z1 * dG0      
    H1,err_l=solve_fixed_point_xG1oH1(G1,niter=niter_fixed_point,trunc=trunc)
    #control convergence of fixed-point condition
    plt.plot(err_l) 
    #compute H_0 = xG_0(H_1((x)) 
    x = np.polynomial.Polynomial([0,1])
    H0 = x * G0(H1)
    H0 = H0.truncate(trunc)
    # plot sk
    if plot:
        fig,ax = plt.subplots(1,1)
        ax.loglog(range(0,len(H0.coef) ) ,  H0.coef)
    """  
    p=np.array(p) 
    n = p.shape[0]
    d = get_avg_degree(p)
    q = get_q(p,d)
    maxn = 30
    r,a = recursion_serafini_mathematica_r(q,maxn)
    s=recursion_serafini_mathematica_s(p,a,maxn)
    if plot:
        fig,ax = plt.subplots(1,1)
        ax.semilogy(range(0,len(s) ) ,  s)
    #---empirical
    size_l = []

    for i in range(n_sample):
        #   sample random graph from degree sequence 
        g = gt.random_graph(n_node, lambda: sample_k(),directed=False) 
        #compute and store size of components, excluding the giant one
        comp, hist= gt.label_components(g)
        #plot
        pi_s_empirical  = get_pi_s_fom_component_size_hist(hist,n_node)
        x = np.arange( pi_s_empirical.shape[0] )
        if plot:
            ax.semilogy( x, pi_s_empirical)


def test_G1_directed():
    """
    """
    x = [1/i for i in range(1,10)]
    x.insert(0,0)
    y = [1/i**2 for i in range(1,12)]
    y.insert(0,0)
    p_deg=np.outer( x,y )
    
    z = get_avg_degree_2d(p_deg)
    G1_num = get_G1_directed_numerical(p_deg,z)
    
    G1_symb,z_symb = get_G1_directed_symbolic(p_deg)

def get_G1_directed_numerical(p_deg,z):
    """
    cf [NSW01] III.A eq.(58) to eq.(62)
    """
    m = p_deg.shape[0]
    dp_dx = p_deg.transpose().dot( np.arange(m) )
    return 1/z *dp_dx

def get_G1_directed_symbolic(p_deg):
    """
    compute the generating function of the probability of "the number of out-going edges leaving
    (...) the vertex reached by following a randomly chosen edge" 
    cf [NSW01] III.A eq.(62)
    
    Parameters:
    ----------------
    p_deg: dict
    p_deg[(j,k)] is the joint probability of a node having in-degree j and out-degree k
    """
    raise NotImplementedError
    
    from sympy import symbols,Function,Sum,diff,Matrix
    
    x, y, z = symbols('x,y,z', real=True,positive=True)
    j, k = symbols('j,k', integer=True,positive=True)
    F0= Function('F0')
    G0= Function('G0')
    F1= Function('F1')
    G1= Function('G1')
    #
    p_deg_symb = Matrix(p_deg)
    G= Sum(( p_deg_symb[j,k]* x**j * y**k, (j, 0, p_deg.shape[0] ), (k, 0, p_deg.shape[1]))) # THIS FAILS !!!!!!!!!!!!!!
    z = diff(G,x).doit().subs({x:1,y:1})
    #F0 = G.subs({y:1}).doit()
    #G0 = G.subs({x:1}).doit()
    #F1 = 1/z * diff(G,y).doit().subs({y:1})
    G1 = 1/z * diff(G,x).doit().subs({x:1})
    # using Poly
    G1 = G1.as_poly()
    return G1.coeffs(),z


def test_recursion_serafini_mathematica():
    """
    p = {1/2, 1/4, 1/4};
    n = Length[p];

    then the average degree is computed 

    d = p.(Range[n] - 1);

    so we can compute the excess degree probabilities (note the indices)

    q = Table[ i  p[[i + 1]]/d , {i, n - 1}]

    and find  {1/3, 2/3}
    """
    p = np.array([0.5,0.25,0.25])
    n = p.shape[0]
    d = p.dot( np.arange(n))
    q = p[1:]*range(1,n)/d
    maxn = 5
    # get rk
    """
    which gives the result for r
    {1/3, 2/9, 4/27, 8/81, 16/243}
    """
    r,a = recursion_serafini_mathematica_r(q,maxn)
    """
    Comment : ok
    """
    # get sk
    """
    that gives for s
    {1/2, 1/12, 1/12, 2/27, 5/81}
    """
    s=recursion_serafini_mathematica_s(p,a,maxn)
    """
    Comment : NOT THE SAME RESULT
    """
    #plot
    plt.semilogy(s)

@jit(nopython=True)
def recursion_serafini_mathematica_r(q,maxn):
    """
    compute rk in [Serafini 18]

    A random graph has assigned degree probabilities p h , h=0,...
    p h is the probability that a randomly selected vertex has degree h
    Let G 0 (x) be the probability generating function of the degree distribution
    G_0(x) = \sum p_h x^h
    The values q h are known as excess degree probabilities.
    G_1(x) = \sum_{h \geq 0} q_h x^h
    
    H_0 = xG_0(H_1((x)) 
    H_1 = xG_1(H_1((x)) 
    
    H_0 = \sum_{k \geq 1} s_k x^k
    H_1 = \sum_{k \geq 1} r_k x^k

    Parameters:
    -------------------
    q: array, shape=(m,)
    coefficients of generating function of G1, the excess degree probability

    maxn : int
    maximum order of the iteration

    Original code:
    ------------------
    WARNING:  indexing in python is different from original mathematica (which starts at index 1)

    m = Length[q]
    maxn = 5;
    # initialize the iteration
    r = Table[0, {maxn}]; 
    r[[1]] = q[[1]]; 
    a = Table[0, {maxn}, {maxn}]; 
    a[[1, 1]] = r[[1]];

    #carry out the iteration as
    Do[
       r[[k]] =    Sum[   q[[h - 1 + 1]]   a[[h - 1, k - h + 1 ]]   , {h, 2, Min[k, m]}]; 
       a[[1, k - 1 + 1]] = r[[k]]; 
       Do[ 
             a[[h, k - h + 1]] =   Sum[a[[h - 1, k - h - j + 1]] r[[j + 1]]     , {j, 0, k - h}]     , {h, 2, k}]
       , {k, 2, maxn}]
    """
    # initialize the iteration
    m = q.shape[0]
    r = np.zeros(maxn+1)
    a=np.zeros((maxn+1,maxn+1)) 
    r[1]=q[0] #because r_1=q_0 in [Serafini 18]
    a[1,0]=r[1]
    for k in range(2,maxn+1):
        for h in range(2, min(k,m)+1):
             r[k] += q[h - 1 ]  * a[h - 1, k - h ] 
        a[1, k - 1 ] = r[k]
        for h in range(2, k+1):
                for j in range(0, k-h+1):
                    a[h, k - h ] +=   a[h - 1, k - h - j ] * r[j ]
    return r,a    

@jit(nopython=True)
def recursion_serafini_mathematica_s(p,a,maxn):
    """
    compute s_k in [Serafini 18]
    WARNING:  indexing in python is different from original mathematica (which starts at index 1)

    A random graph has assigned degree probabilities p h , h=0,...
    p h is the probability that a randomly selected vertex has degree h
    Let G 0 (x) be the probability generating function of the degree distribution
    G_0(x) = \sum p_h x^h
    The values q h are known as excess degree probabilities.
    G_1(x) = \sum_{h \geq 0} q_h x^h
    
    H_0 = xG_0(H_1((x)) 
    H_1 = xG_1(H_1((x)) 
    
    H_0 = \sum_{h \geq 1} s_k x^k
    H_1 = \sum_{h \geq 1} r_k x^k

    Parameters:
    -------------------
    p: array, shape=(n,)
    coefficients of generating function of G0, the degree distribution

    maxn : int
    maximum order of the iteration
    
    Original code:
    ---------------
    To find s

    we first set a vector  for s

    s = Table[0, {maxn}]

    define its first component

    s[[1]] = p[[1]]

    and the other components

    Do[s[[k]] =  Sum[   p[[h - 1 + 1]]   a[[h - 1, k - h + 1 ]]   , {h, 2,    Min[k, n]}];
           , {k, 2, maxn}]

    """
    # initialize the iteration
    n = p.shape[0]
    s = np.zeros(maxn+1)
    s[1]=p[0] # because of eq.8 
    for k in range(2,maxn+1):
        for h in range(2,min(k,n)+1):
            s[k]+=  p[h - 1 ] *  a[h - 1, k - h  ]
    return s

# -------------------------
# UTILITY FUNCTIONS

def  get_2nd_nearest_neig_cnt_gt(g):
   """
   compute number of second neighbors from gt graph
   
   NB: SLOW
   TODO: numba ?
   """
   l = []
   for v in g.iter_vertices():
       cnt = 0
       for u in g.iter_all_neighbors(v):
           for u2 in g.iter_all_neighbors(u):
              cnt+=1 
       l.append(cnt) 
   return l


def  get_2nd_nearest_neig_cnt_nx(G, nodes):
   """
   compute number of second neighbors from nx graph
   
   NB: SLOW
   TODO: numba ?
   
   https://networkx.org/documentation/stable/reference/classes/generated/networkx.Graph.neighbors.html?highlight=neighbor
   """
   l = []
   for v in nodes:
       cnt = 0
       for u in G.neighbors(v):
           for u2 in G.neighbors(u):
              cnt+=1 
       l.append(cnt) 
   return l


def deg_sample():#name='Poisson',param=20):
    """
    return an array of random numbers
    """
    #if name=="Poisson":
    param=20
    return np.random.poisson(param)

# -------------------------
# CLASSES

def test_():
    n= 100
    k_in =  [ sample_k(40) for i in range(n)]
    k_out = [ sample_k(40) for i in range(n)]
    
    UD = UnipartiteDirected()
    UD.add_degree_sequence(k_in,k_out)
    UD.compute_marginal_pk_out()
    #test
    UD.compute_G0()
    UD.compute_G1()
    UD.solve_H1()
    plt.plot( UD.H1_err )
    UD.compute_H0()
    
    fig,ax = plt.subplots(1,1)
    ax.loglog(range(0,len(UD.H0.coef) ) ,  UD.H0.coef)

class UnipartiteDirected():
    """
    after NSW01 §III
    
    Problem: numpy has 1d polynomial, but not 2D
    
    TODO: compare to sage of sympy symbolic calculus 
    """
    def __init__(self,trunc=100):
        self.degree_sequence_in =[] #degree sequence
        self.degree_sequence_out =[] #degree sequence
        self.nb_vertices=[]  # nb vertices
        self.pk_in=[] #degree distribution
        self.pk_out=[] #degree distribution
        self.G0=None
        self.dG0 =None
        self.G1=None
        self.dG1=None
        self.F0=None
        self.dF0 =None
        self.F1=None
        self.dF1=None
        self.H0 = None
        self.H1=None
        self.H1_err=None #error sequence in fixed point iteration
        #self.G0_o_G1=None
        self.z = None
        self.trunc = trunc #order of truncation
    def add_degree_sequence(self,k_in,k_out):    
        """
        add a degree sequence
        
        input:
        -------
        k :  list
        the degree sequence
        """
        #append k
        self.degree_sequence_in.append(k_in)
        self.degree_sequence_out.append(k_out)
        #update nb_vertices
        self.nb_vertices.append( len(k_in) )
    def compute_marginal_pk_out(self):    
        """
        forget k_in, just compute p_k_out
        """
        pk=np.bincount( self.degree_sequence_out[0] )
        pk=pk/pk.sum() 
        #update self.pk
        self.pk_out.append(pk)
    def compute_G1(self):
        """
        NSW01 eq.62
        G_1(y) = 1/z \frac{ dG }{ dx }|_{x=1}
        """
        df = pd.DataFrame({'k_in': self.degree_sequence_in[0] ,'k_out': self.degree_sequence_out[0] })
        #marginalize
        df_margin_out = df.groupby(['k_out']).sum()  # the trick is here
        df_margin_out = df_margin_out / df_margin_out.sum()
        df_margin_out.rename(columns={'k_in':'pk'},inplace=True)
        kmax = df_margin_out.index.max() 
        #get full range
        df_merge = pd.merge( pd.DataFrame( {'k_out':range(kmax)}) ,df_margin_out,
                            how='outer',left_index=True,right_on=['k_out']) 
        df_merge.reset_index(drop=True,inplace=True)                    
        df_merge.fillna(0,inplace=True)
        coef =  (1/self.z  * df_merge['pk']).values.tolist()  
        self.G1 = np.polynomial.Polynomial( coef )
    def compute_G0(self):
        """
        """
        self.G0 = np.polynomial.Polynomial(self.pk_out[0])
        self.dG0 =  self.G0.deriv()        
        self.z = self.dG0(1)        
    def solve_H1(self,niter=10,trunc=100):
        """
        solve H1(y) = y G1( H1(y))
        
        NSW01 p.10
        """
        H1,err = solve_fixed_point_xG1oH1(self.G1, niter=niter,trunc=self.trunc)
        self.H1 = H1
        self.H1_err = err
    def compute_H0(self):
        """
        H0(y) = y G0(H1(y))
        
        NSW01 p.10
        """
        self.H0 = np.polynomial.Polynomial([0,1]) * self.G0( self.H1)
        self.H0 =self.H0.truncate(  self.trunc)

class ArbitraryDegreeDistributionRG:
    """
         TODO: add "undirected" in the Class name (not clear)
    
          Random Graph with Arbitrary Degree Distribution (not arbitrary degree sequence)
          with standard numpy polynomial representation, which is dense.
          
          Ref: [NSW01] Newman, Strogatz, Watts  http://arXiv.org/abs/cond-mat/0007235v2
    """
    def __init__(self):
        self.degree_sequence =[] #degree sequence
        self.nb_vertices=[]  # nb vertices
        self.pk=[] #degree distribution
        self.G0=None
        self.dG0 =None
        self.G1=None
        self.dG1=None
        self.G0_o_G1=None
        self.z1 = None
        self.z2 = None
        self.l = None
        self.has_giant_component = None
    def add_degree_sequence(self,k):    
        """
        add a degree sequence
        
        input:
        -------
        k :  list
        the degree sequence
        """
        #append k
        self.degree_sequence.append(k)
        #update nb_vertices
        self.nb_vertices.append( len(k) )
        #compute pk
        pk=np.bincount(k)
        pk=pk/pk.sum() 
        #update self.pk
        self.pk.append(pk)
    def compute_generating_function(self):
        pass
    def giant_component(self):  
        """
        """  
        if self.has_giant_component is None:
            self.has_giant_component =  self.z2>self.z1   
            return self.giant_component()
        else: 
            return self.has_giant_component
    def plot_pdf_first_neighbor_count(self,ax):
        ncoef = self.G0.coef.shape[0]
        ax.plot(range(ncoef), self.G0.coef, 'r-', label="p_k")
    def plot_pdf_second_neighbor_count(self,ax):
        ncoef = self.G0_o_G1.coef.shape[0]
        ax.plot( range(ncoef),     self.G0_o_G1.coef, 'r-',  label="$p_k^{theo}$")
        
class UnipartiteArbitraryDegreeDistributionRG(ArbitraryDegreeDistributionRG):
    """   
    TODO: add "undirected" in the Class name (not clear)
     """
    def __init__(self):
        ArbitraryDegreeDistributionRG.__init__(self)
        self.d2G0 = None
    def compute_generating_function(self):
        """
        """
        if len(self.degree_sequence) != 1: 
            raise ValueError('len(self.degree_sequence) should be 1')
        if len(self.nb_vertices) != 1: 
            raise ValueError('len(self.nb_vertices) should be 1')
        if len(self.pk) != 1: 
            raise ValueError('len(self.pk) should be 1')
        
        self.G0 = np.polynomial.Polynomial(self.pk[0])
        self.dG0 =  self.G0.deriv()        
        self.d2G0 = self.dG0.deriv()
        self.z1 = self.dG0(1)
        self.z2 = self.d2G0(1)
        self.G1 = 1./self.z1 * self.dG0
        self.G0_o_G1= self.G0(self.G1)
        #TODO: truncation ???
        N = self.nb_vertices[0]
        self.l = np.log(N/self.z1)/np.log(self.z2/self.z1)+1.
   
class BipartiteArbitraryDegreeDistributionRG(ArbitraryDegreeDistributionRG):
    """  
    TODO: add "undirected" in the Class name (not clear)
      """
    def __init__(self):        
        ArbitraryDegreeDistributionRG.__init__(self)
        self.f0=None
        self.g0=None
        self.df0=None
        self.dg0=None
        self.f1=None
        self.g1=None
        #self.df1=None
        #self.dg1=None
        self.mu = None
        self.nu = None
        self.G0_o_G1=None
    def compute_generating_function(self):
        """
        """
        if len(self.degree_sequence) != 2: 
            raise ValueError('len(self.degree_sequence) should be 2')
        if len(self.nb_vertices) != 2: 
            raise ValueError('len(self.nb_vertices) should be 2')
        if len(self.pk) != 2: 
            raise ValueError('len(self.pk) should be 2')
        
        self.f0=np.polynomial.Polynomial(self.pk[0])
        self.g0=np.polynomial.Polynomial(self.pk[1])
        self.df0=self.f0.deriv()
        self.dg0=self.g0.deriv()
        
        self.mu = self.df0(1)
        self.nu = self.dg0(1)
        
        self.f1=1./self.mu * self.df0
        self.g1=1./self.nu * self.dg0
                
        self.G0 = self.f0(self.g1)
        #TODO: truncation ???
        self.dG0 =  self.G0.deriv()        
        
        self.G1 = self.f1(self.g1)
        self.z1 = self.dG0(1)
        
        self.G0_o_G1= self.G0(self.G1)    
        #TODO: truncation ???    
        self.z2 = self.G0_o_G1(1)

        #N = self.nb_vertices[0]
        #self.l = np.log(N/self.z1)/np.log(self.z2/self.z1)+1.   
   
# -------------------------
# TESTS   
   
def test_ArbitraryDegreeDistributionRG():
    """
    """
    g = gt.random_graph(5000, deg_sample, directed=False)
    a_RG = ArbitraryDegreeDistributionRG()
    k = g.degree_property_map('total')
    a_RG.add_degree_sequence(k.a.tolist())
   
def test_Unipartite():
    """
    """
    u_RG=UnipartiteArbitraryDegreeDistributionRG()
    g = gt.random_graph(5000, deg_sample, directed=False)
    k = g.degree_property_map('total')
    u_RG.add_degree_sequence(k.a.tolist())   
    u_RG.compute_generating_function() 
    fig, ax = plt.subplots(2, 1, figsize=(10, 10))
    u_RG.plot_pdf_first_neighbor_count(ax[0])
    u_RG.plot_pdf_second_neighbor_count(ax[1])
    """
    COMMENTS:
    * works well for Poisson (N=5000) with param 20
    * TODO: try other parameter
    * TODO: try powerlaw,...
    """

def test_Bipartite():
    """
    """
    # make data
    n,m,p = 100,2000,0.01
    #https://networkx.org/documentation/stable/reference/algorithms/bipartite.html?highlight=bipartite#module-networkx.algorithms.bipartite.generators
    G = nx.algorithms.bipartite.generators.random_graph(n,m,p)
    dv = G.degree(range(n)) 
    dv_dict=dict(dv) 
    deg_seq_0 = list(dv_dict.values())
    dv = G.degree(range(n,n+m)) 
    dv_dict=dict(dv) 
    deg_seq_1 = list(dv_dict.values())
    #
    b_RG=BipartiteArbitraryDegreeDistributionRG()
    b_RG.add_degree_sequence(deg_seq_0)   
    b_RG.add_degree_sequence(deg_seq_1)   
    b_RG.compute_generating_function() 
    fig, ax = plt.subplots(2, 1, figsize=(10, 10))
    b_RG.plot_pdf_first_neighbor_count(ax[0])
    ax[0].plot(range(b_RG.pk[1].shape[0]), b_RG.pk[1], 'b--', label="$p_k^{samp}$")
    # get count of second neighbors of nodes in mode '0'
    #k2 = get_2nd_nearest_neig_cnt_nx(G, range(n))
    k2 = get_2nd_nearest_neig_cnt_nx(G, range(n,n+m))
    pk2=np.bincount(k2)
    pk2=pk2/pk2.sum()
    b_RG.plot_pdf_second_neighbor_count(ax[1])
    ax[1].plot(range(pk2.shape[0]), pk2, 'b--', label="$p_k^{samp}$")
    """
    COMMENTS:
    * works wells for small sparse Poisson (random) network: n,m,p = 100,1000,0.01
    * large error in predicted pdf if: 100,2000,0.01   Q: WHY ????
    * saturates RAM if p too big.
    """

    
def test_unipartite_undirected():
    """
    without classes defined above
    """
    # generate
    #file:///home/aurelien/local/git/extern/graph-tool/doc/build/generation.html#graph_tool.generation.random_graph
    g = gt.random_graph(20000, deg_sample, directed=False)
    N = g.num_vertices()
    # compute degree distribution
    k = g.degree_property_map('total')
    k_mean = np.mean(k.a)
    pk=np.bincount(k.a)
    pk=pk/pk.sum() 
    # make generating function
    #https://numpy.org/doc/stable/reference/generated/numpy.polynomial.polynomial.Polynomial.html#numpy.polynomial.polynomial.Polynomial
    G0 = np.polynomial.Polynomial(pk) # GF for distrib of 1st nearest neigh;
    G0_deriv = G0.deriv()
    G0_deriv2 = G0_deriv.deriv()
    z1 = G0_deriv(1)
    z2 = G0_deriv2(1)
    G1 = 1/z1 * G0_deriv
    G2nn = G0(G1) # GF for distrib of 2nd nearest neigh; [NSW01] eq.(9)
    l = np.log(N/z1)/np.log(z2/z1)+1
    #check
    assert G0(1)==1              #[NSW01] eq.(3)
    assert k_mean == z1 	 #[NSW01] eq.(5)
    dist, ends = gt.pseudo_diameter(g) 
    print('l={}  diameter={}'.format(l,dist)) #[NSW01] eq.(54)
    #distrib of 1st neighbor count
    fig, ax = plt.subplots(2, 1, figsize=(10, 10))
    ax[0].plot(range(G0.coef.shape[0]), G0.coef, 'r-', label="p_k")
    ax[1].set_xlabel("nb 1st order neighbor")
    #distrib of 2nd neighbor count
    k2=get_2nd_nearest_neig_cnt_gt(g)
    pk2=np.bincount(k2)
    pk2=pk2/pk2.sum()

    ax[1].plot(range(G2nn.coef.shape[0]), G2nn.coef, 'r-', 
               label="$p_k^{theo}$")
    ax[1].plot(range(pk2.shape[0]), pk2, 'b--', label="$p_k^{samp}$")
    ax[1].set_xlabel("nb 2nd order neighbor")
    ax[1].legend()


# ######################################
# DEPRECATED OR FAILING


def solve_fixed_point_xG1oH1(G1,niter=5,trunc=100):
    """
    H_1 = xG_1(H_1((x)) 
    cf [NSW01] III.A
    
    
    COMMENT: result depends strongly on on n_iter !!
    """
    #init
    x = np.polynomial.Polynomial([0,1])
    H1 = np.polynomial.Polynomial([1])
    err_l=[]
    #loop
    for i in range(niter):
        H1new = x * G1( H1)
        #truncate
        H1new=H1new.truncate(trunc)    
        #compute some error
        if i>5:
            err = np.max(np.abs(H1new.coef-H1.coef))
            err_l.append(err)
        #replace
        H1=H1new.copy()
    return H1,err_l
    
def  test_recursion_serafini()  :
    # get qk from pk
    #qk = G1.coef
    # run recursion_serafini_H1
    #kmax = 100
    #rk,sk = recursion_serafini(pk,qk,kmax)
    pass

def out_component_size_distribution_sagemath():
    """
    https://doc.sagemath.org/html/en/reference/polynomial_rings/sage/rings/polynomial/polynomial_element.html
    sagecell.sagemath.org
    SR: symbolic ring
    
    R = QQ['x1,x2,x3,x4']; R
    
    R.<x> = PolynomialRing(QQ)
    g = -x^4 + x^2/2 - x
    g.derivative()
    
    R = QQ['x']['y']
    y = R.gen()
    x = R.base_ring().gen()
    p1 = 1 - x*y + 2*y**3
    p2 = -1/3 + y**5
    p1._mul_trunc_(p2, 5)
    
    Pol.<x> = CBF[]
    (1 + x + x^2/2 + x^3/6 + x^4/24 + x^5/120).compose_trunc(1 + x, 2)
    
    sum(k, k, 1, n).factor()
    f(x,y) = x*y + sin(x^2) + e^(-x); derivative(f, x)

    R = QQ['x']['y']
    y = R.gen()
    x = R.base_ring().gen()    
    k,j = var('k,j')
    G(x,y)= sum((1/(j*k))* x^j * y^k  for  j in  (1, 100) for k in (1, 100))
    z = G.derivative(x).subs(x=1,y=1)
    F0 = G.subs(y=1)
    G0 = G.subs(x=1)
    F1 = 1/z * G.derivative(y).subs(y=1)
    G1 = 1/z * G.derivative(x).subs(x=1)
    print(F1)
    print(G1)
    #
    niter = 5
    n_truncate = 30
    H1 = y
    for i in range(niter):
            H1 = y* G1.subs(y=H1) 
        
            
    # WITH POLY
    R.<x,y> = PolynomialRing(RR,2)
    #G= sum((1/(j*k))* x^j * y^k  for  j in  (1, 100) for k in (1, 100))
    G=0
    for j in range(1,100):
        for k in range(1,100):
            G+= 1/(j*k)* x^j * y^k  
    print(G.coefficients())
    z = G.derivative(x).subs(x=1,y=1)
    F0 = G.subs(y=1)
    G0 = G.subs(x=1)
    F1 = 1/z * G.derivative(y).subs(y=1)
    G1 = 1/z * G.derivative(x).subs(x=1)
    print(F1)
    print(G1)
    #
    niter = 5
    n_truncate = 30
    H1 = 1
    for i in range(niter):
            H1 = y* G1.subs(y=H1).truncate(y,n_truncate)
    H0 = y* G0.subs(y=H1).truncate(y,n_truncate) 
    coef = H0.coefficients()     
    list_plot(coef[::-1], scale='loglog')  
    
    COMMENT: result depends strongly on on n_iter !!
    """

def out_component_size_distribution_sympy():
    """
    compute out-component   cf [NSW01] III.A
    
    https://docs.sympy.org/latest/modules/polys/basics.html#polynomials
    
    assumptions: https://docs.sympy.org/latest/guides/assumptions.html
    
    #array: https://docs.sympy.org/latest/modules/tensor/array.html?highlight=array
    from sympy import Array 
    a1 = Array([[1, 2], [3, 4], [5, 6]])
    """
    x, y, z = symbols('x,y,z', real=True,positive=True)
    j, k = symbols('j,k', integer=True,positive=True)
    F0= Function('F0')
    G0= Function('G0')
    F1= Function('F1')
    G1= Function('G1')
    #
    G= Sum((1/(j*k))* x**j * y**k, (j, 1, 100), (k, 1, 100))
    z = diff(G,x).doit().subs({x:1,y:1})
    F0 = G.subs({y:1}).doit()
    G0 = G.subs({x:1}).doit()
    F1 = 1/z * diff(G,y).doit().subs({y:1})
    G1 = 1/z * diff(G,x).doit().subs({x:1})
    #---
    # using Poly
    G1 = G1.as_poly()
    # fixed-point eq:    H_1 = xG_1(H_1((x))    cf [NSW01] III.A
    niter = 5
    n_truncate = 30
    H1 = Poly(y,y) 
    for i in range(niter):
            H1 = Poly(y,y)* G1.compose(H1) 
            #H1=H1.trunc(n_truncate) #FAILS
            #H1.as_expr().truncate(10).as_poly() #FAILS
            #H1 = fps(H1.as_expr()).truncate(n_truncate).removeO().as_poly()  #OK BUT SLOW
            all_terms = Poly(H1, y).all_terms()
            H1=sum(y**n * term for (n,), term in all_terms if n <= n_truncate).as_poly()  #OK BUT SLOW
    #compute H_0 = xG_0(H_1((x)) 
    H0 = Poly(y,y)* G0.as_poly().compose(H1) 
    coef = H0.coeffs()
    # plot. NB: first coefficients correspond to largest powers
    plt.loglog(range(len(coef)), coef[::-1] )
    #---
    # using series FAILS
    f1.compose(f2, x).truncate(8)
    niter = 5
    n_truncate = 30
    yfps = fps(y)
    H1fps = fps(y)
    G1fps = fps(G1.as_expr())
    for i in range(niter):
            H1fps =yfps.product(G1fps.compose(H1fps,y).truncate(10) , y).truncate(10) #Poly(y,y)* G1.compose(H1) 
    
    #---
    # using expr
    # fixed-point eq:    H_1 = xG_1(H_1((x))    cf [NSW01] III.A
    niter = 5
    n_truncate = 30
    H1 = y
    for i in range(niter):
            H1 = y* G1.subs({y:H1})  #FAST
            H1=H1.expand().simplify()
            #HOWTO TRUNCATE ??
    #compute H_0 = xG_0(H_1((x)) 
    H0 = y* G0.subs({y:H1})       
    coef = H0.as_poly().coeffs() #SO SLOW
    # plot. NB: first coefficients correspond to largest powers
    plt.loglog(range(len(coef)), coef[::-1] )


def recursion_serafini(p,q,kmax):
    """
    FAILS: r_k seems to depend on r_k+1
    
    A random graph has assigned degree probabilities p h , h=0,...
    p h is the probability that a randomly selected vertex has degree h
    Let G 0 (x) be the probability generating function of the degree distribution
    G_0(x) = \sum p_h x^h
    The values q h are known as excess degree probabilities.
    G_1(x) = \sum_{h \geq 0} q_h x^h
    
    H_0 = xG_0(H_1((x)) 
    H_1 = xG_1(H_1((x)) 
    
    H_0 = \sum_{h \geq 1} s_k x^k
    H_1 = \sum_{h \geq 1} r_k x^k
    
    a_i^j  is stored as a[i,j]
    """
    raise NotImplementedError
    
    r = np.zeros(kmax)
    s = np.zeros(kmax)
    a=np.zeros((kmax+1,kmax+1)) #MOD
    a[0,1]=q[0]
    r[1]=q[0] #eq(6)
    # compute r
    for k in range(2,kmax):
        for h in range(2,k+1):
            print("k {}  h {}".format(k,h))
            r[k]+=q[h-1]*a[k-h,h-1]
        a[k-1,1]=r[k]
        for h in range(2,k):
            for j in range(0,k):
                a[k-1,h]+=a[j,h-1]*r[k-j]
        for h in range(0,k+1):
            for j in range(0,h+1):
                print("h {}  j {}".format(h,j))
                a[h,k]+=a[j,k-1]*r[h+1-j]
    # compute s
    s[1] =p[0]
    for k in range(2,kmax):
        for h in range(0,k+1):
            s[k]+=p[h-1]*a[k-h,h-1]                
    return r,s                   
